package dao;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import entities.Comment;
import executor.Executor;
import util.dto.UpdateResult;
import util.exceptions.InvalidDataException;
import util.exceptions.comments.*;
import util.exceptions.posts.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by yuriy on 28.08.17.
 */
public class CommentsDAO {
    private static final String CREATE_COMMENT = "INSERT INTO comments(post_id, author_id, value, creation_date) " +
            "VALUES(?, ?, ?, ?)";
    private static final String GET_POST_COMMENTS = "SELECT * FROM comments WHERE post_id = ? ORDER BY creation_date DESC";
    private static final String DELETE_COMMENT = "DELETE FROM comments WHERE id = ?";
    private static final String EDIT_COMMENT = "UPDATE comments SET value = ? WHERE id = ?";
    private static final String GET_COMMENT_BY_ID = "SELECT * FROM comments WHERE id = ?";

    private Executor executor;

    public CommentsDAO() {
        this.executor = new Executor();
    }

    public Comment getCommentById(Integer commentId) throws SQLException, GotNoCommentException {
        List<Object> params = new ArrayList<>();
        params.add(commentId);

        Comment comment = executor.execQuery(GET_COMMENT_BY_ID, params, resultSet -> {
            if (!resultSet.next())
                return null;
            return parseComment(resultSet);
        });
        if (comment == null)
            throw new GotNoCommentException("no comment with such id");
        return comment;
    }

    public Comment createComment(Comment comment) throws InvalidDataException, CreationCommentException {
        List<Object> params = new ArrayList<>();
        params.add(comment.getPostId());
        params.add(comment.getAuthorId());
        params.add(comment.getValue());
        params.add(comment.getCreationDate());

        try {
            UpdateResult result = executor.execUpdate2(CREATE_COMMENT, params);
            int rows = result.getRowsAffected();
            Long id = result.getGeneratedKey();
            if (rows != 1)
                throw new SQLException("smth went wrong with query");
            return getCommentById(id.intValue());
        }
        catch (MySQLIntegrityConstraintViolationException e) {
            e.printStackTrace();
            throw new InvalidDataException("one or both ids are invalid. No users or posts with such ids (unable to create comment");
        }
        catch (SQLException | GotNoCommentException e) {
            e.printStackTrace();
            throw new CreationCommentException("internal error. Unable to add comment");
        }
    }

    public Comment editComment(String value, Integer commentId) throws GotNoCommentException, EditingCommentException {
        List<Object> params = new ArrayList<>();
        params.add(value);
        params.add(commentId);

        try {
            int rows = executor.execUpdate(EDIT_COMMENT, params);
            if (rows != 1)
                throw new GotNoCommentException("no affect on database. comment wasn't deleted. (maybe no comment with such id in db?");
            return getCommentById(commentId);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new EditingCommentException("internal error. unable to update comment");
        }
    }

    public void deleteComment(Integer commentId) throws DeletingCommentException, GotNoCommentException {
        List<Object> params = new ArrayList<>();
        params.add(commentId);

        try {
            int rows = executor.execUpdate(DELETE_COMMENT, params);
            if (rows != 1)
                throw new GotNoCommentException("no affect on database. comment wasn't deleted. (maybe no comment with such id in db?");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DeletingCommentException("internal error. unable to delete comment");
        }
    }

    public List<Comment> getPostComments(Integer postId) throws GotNoCommentException, GettingCommentsException {
        List<Object> params = new ArrayList<>();
        params.add(postId);

        List<Comment> list = null;
        try {
            list = executor.execQuery(GET_POST_COMMENTS, params, resultSet -> {
                List<Comment> lst = new ArrayList<>();
                while (resultSet.next()) {
                    lst.add(parseComment(resultSet));
                }
                return lst;
            });
            if (list.isEmpty())
                throw new GotNoCommentException("this user doesnt have comments on this post");
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new GettingCommentsException("internal error. unable to get comments");
        }

    }

    private Comment parseComment(ResultSet resultSet) throws SQLException {
        return new Comment(
                resultSet.getInt("id"),
                resultSet.getInt("post_id"),
                resultSet.getInt("author_id"),
                resultSet.getString("value"),
                new Date(resultSet.getTimestamp("creation_date").getTime())
        );
    }
}
