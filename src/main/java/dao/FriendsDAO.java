package dao;

import entities.User;
import executor.Executor;
import util.dto.FriendshipRelation;
import util.enums.FriendsRequests;
import util.enums.FriendshipStatus;
import util.exceptions.friends.*;
import util.exceptions.users.NoUserException;
import util.exceptions.users.NoUserFriendRequestsException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuriy on 19.08.17.
 */
public class FriendsDAO {
    private static final String SEND_REQUEST = "INSERT INTO friends(friend1_id, friend2_id) VALUES(?, ?)";
    private static final String GET_FRIENDS = "SELECT users.* FROM users " +
            "JOIN (SELECT * FROM friends WHERE (friend1_id = ? OR friend2_id = ?) " +
                "AND accept = 'true') as cus " +
            "ON ((users.id = cus.friend1_id AND NOT cus.friend1_id = ?) " +
                "OR (users.id = cus.friend2_id AND NOT cus.friend2_id = ?))";
    private static final String GET_REQUESTS_TO_USER = "SELECT users.* FROM users " +
            "JOIN (SELECT * FROM friends WHERE (friend1_id = ? OR friend2_id = ?) AND accept = 'false') as cus " +
            "ON (users.id = cus.friend1_id AND cus.friend2_id = ?)";
    private static final String GET_REQUESTS_FROM_USER = "SELECT users.* FROM users " +
            "JOIN (SELECT * FROM friends WHERE (friend1_id = ? OR friend2_id = ?) AND accept = 'false') as cus " +
            "ON (users.id = cus.friend2_id AND NOT cus.friend2_id = ?)";
    private static final String GET_FRIENDSHIP_STATUS = "SELECT * FROM friends WHERE " +
            "((friend1_id = ? AND friend2_id = ?) OR (friend1_id = ? AND friend2_id = ?))";
    private static final String DELETE_FRIENDS = "DELETE FROM friends WHERE friend1_id = ? AND friend2_id = ? " +
            "OR friend1_id = ? AND friend2_id = ?";
    private static final String ACCEPT_FRIENDSHIP = "UPDATE friends SET accept = 'true' " +
            "WHERE friend1_id = ? && friend2_id = ?";




    private Executor executor;

    public FriendsDAO() {
        this.executor = new Executor();
    }

    //Порядок важен! 1 - тот кто делает заявку, 2 - кто получает (и будет в правом столбце)
    public void sendFriendRequest(Integer userId, Integer newFriendId) throws FriendRequestException, DuplicateFriendsPairException {
        List<Integer> params = new ArrayList<>();
        params.add(userId);
        params.add(newFriendId);

        try {
            int rows = executor.execUpdate(SEND_REQUEST, params);
            if (rows != 1)
                throw new SQLException("smth went wrong with query");

        } catch (SQLIntegrityConstraintViolationException e) {
            e.printStackTrace();
            throw new DuplicateFriendsPairException("this pair of friends already exists");
        }
        catch (SQLException e) {
            e.printStackTrace();
            throw new FriendRequestException("unable to add new pair of friends");
        }


    }

    //OK
    public List<User> getUserFriendsById(Integer userId)
            throws NoUserException, GettingFriendsException, GotNoFriendsException {

        List<Integer> params = new ArrayList<>();
        for (int i = 0; i < 4; i++) params.add(userId);

        try {
            List<User> list =  executor.execQuery(GET_FRIENDS, params, resultSet -> {
                List<User> lst = new ArrayList<>();
                while (resultSet.next()) {
                    lst.add(parseUser(resultSet));
                }
                return lst;
            });
            if (list.isEmpty())
                throw new GotNoFriendsException("got no friends from this user");
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new GettingFriendsException("internal error. Unable to get user's friendlist");
        }
    }

    //OK
    public List<User> getUserFriendRequestsById(Integer userId, FriendsRequests requestType)
            throws NoUserException, GettingFriendRequestsListException, NoUserFriendRequestsException {

        String query = (requestType == FriendsRequests.REQUESTS_FROM_USER)
                ? GET_REQUESTS_FROM_USER
                : GET_REQUESTS_TO_USER;
        List<Integer> params = new ArrayList<>();
        for (int i = 0; i < 3; i++) params.add(userId);

        try {
            List<User> list = executor.execQuery(query, params, resultSet -> {
                List<User> lst = new ArrayList<>();
                while (resultSet.next()) {
                    lst.add(parseUser(resultSet));
                }
                return lst;
            });
            if (list.isEmpty())
                throw new NoUserFriendRequestsException("got no request for this user");
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new GettingFriendRequestsListException("internal error. Unable to get user's requestList");
        }
    }

    //1 - хочет узнать какой он друг по отншению ко 2
    public FriendshipStatus getUserFriendshipStatus(Integer requestingUserId, Integer requestedUserId) throws GettingFriendshipStatusException {
        List<Integer> params = new ArrayList<>();
        params.add(requestingUserId);
        params.add(requestedUserId);
        params.add(requestedUserId);
        params.add(requestingUserId);

        try {
            FriendshipRelation relation = executor.execQuery(GET_FRIENDSHIP_STATUS, params, resultSet -> {
                if (!resultSet.next()) {
                    return new FriendshipRelation();
                }
                FriendshipRelation rel = new FriendshipRelation(
                        resultSet.getInt("friend1_id"),
                        resultSet.getInt("friend2_id"),
                        resultSet.getString("accept")
                );
                return rel;
            });
            return parseStatus(relation, requestingUserId);
        }
        catch (SQLException/* | NoUserException*/ e) {
            e.printStackTrace();
            throw new GettingFriendshipStatusException("internal error. Unable to get friendship status");
        }
    }

    //порядок не важен
    /*public void deleteFriendship(Integer requestingUserId, Integer userToFinishWithId)*/
    public void deleteFriendship(Integer user1Id, Integer user2Id)
            throws FriendshipDeleteException, NoFriendsDeletedException {

        List<Integer> params = new ArrayList<>();
        params.add(user1Id);
        params.add(user2Id);
        params.add(user2Id);
        params.add(user1Id);

        try {
             int rows = executor.execUpdate(DELETE_FRIENDS, params);
             if (rows != 1)
                 throw new NoFriendsDeletedException("no affect on database. nobody was deleted");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new FriendshipDeleteException("smth went wrong. Unable to delete bond");
        }
    }

    //2 - всегда тот юзер, который принимает заявку (всегда в правой колонке)
    public void acceptFriend(Integer requesterUserId, Integer accepterUserId)
            throws FriendAcceptException {

        List<Integer> params = new ArrayList<>();
        params.add(requesterUserId);
        params.add(accepterUserId);

        try {
            int rows = executor.execUpdate(ACCEPT_FRIENDSHIP, params);
            if (rows != 1)
                throw new FriendAcceptException("no affect on database. nobody was accepted");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new FriendAcceptException("unable to accept friendship");
        }
    }

    private User parseUser(ResultSet resultSet) throws SQLException {
        return new User(
                resultSet.getInt("id"),
                resultSet.getString("email"),
                resultSet.getString("password"),
                resultSet.getString("first_name"),
                resultSet.getString("second_name"),
                resultSet.getString("birth_date"),
                resultSet.getString("country"),
                resultSet.getString("phone_num"),
                resultSet.getString("gender"),
                resultSet.getString("about"),
                resultSet.getString("sports"),
                resultSet.getString("weight"),
                resultSet.getString("height"),
                resultSet.getString("img")
        );
    }

    private FriendshipStatus parseStatus(FriendshipRelation relation, Integer requestingUserId) throws SQLException {
        if (relation.isEmpty()) return FriendshipStatus.NOT_FRIEND;

        if (relation.getAccept().equals("true"))
            return FriendshipStatus.FRIEND;
        else {
            if (relation.getUserId1().equals(requestingUserId))
                return FriendshipStatus.REQUETED_BY_USER;
            else
                return FriendshipStatus.REQUESTED_TO_USER;
        }
    }
}
