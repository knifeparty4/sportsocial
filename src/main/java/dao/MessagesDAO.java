package dao;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import entities.Message;
import entities.Post;
import executor.Executor;
import util.dto.UpdateResult;
import util.exceptions.InvalidDataException;
import util.exceptions.messages.*;
import util.exceptions.posts.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessagesDAO {
    private static final String CREATE_MESSAGE = "INSERT INTO messages(sender_id, receiver_id, value, sent_date) " +
            "VALUES(?, ?, ?, ?)";
    private static final String GET_USER_MESSAGES = "SELECT * FROM messages WHERE (sender_id = ? AND receiver_id = ?)" +
            "OR (receiver_id = ? AND sender_id = ?) ORDER BY sent_date DESC";
    private static final String DELETE_MESSAGE = "DELETE FROM messages WHERE id = ?";
    private static final String EDIT_POST = "UPDATE posts SET value = ? WHERE id = ?";
    private static final String GET_MESSAGE_BY_ID = "SELECT * FROM messages WHERE id = ?";

    private Executor executor;

    public MessagesDAO() {
        this.executor = new Executor();
    }

    public Message getMessageById(Integer msgId) throws GotNoMessageException {
        List<Object> params = new ArrayList<>();
        params.add(msgId);

        Message msg = null;
        try {
            msg = executor.execQuery(GET_MESSAGE_BY_ID, params, resultSet -> {
                if (!resultSet.next())
                    return null;
                return parseMessage(resultSet);
            });
        } catch (SQLException e) {
            e.printStackTrace();
            throw new GotNoMessageException("internal error while getting message");
        }
        if (msg == null)
            throw new GotNoMessageException("no message with such id");
        return msg;
    }

    public Message createMessage(Message msg) throws CreationMessageException {
        List<Object> params = new ArrayList<>();
        params.add(msg.getSenderId());
        params.add(msg.getReceiverId());
        params.add(msg.getValue());
        params.add(msg.getSentDate());

        try {
            UpdateResult result = executor.execUpdate2(CREATE_MESSAGE, params);
            int rows = result.getRowsAffected();
            Long id = result.getGeneratedKey();
            if (rows != 1)
                throw new SQLException("smth went wrong with query while creating message");
            return getMessageById(id.intValue());
        } catch (SQLException | GotNoMessageException e) {
            e.printStackTrace();
            throw new CreationMessageException("internal error. Unable to create message");
        }

    }


    public void deleteMessage(Integer msgId, Integer userFromId) {

    }

    public void deleteMessage(Integer msgId) throws DeletingMessageException, GotNoMessageException {
        List<Object> params = new ArrayList<>();
        params.add(msgId);

        try {
            int rows = executor.execUpdate(DELETE_MESSAGE, params);
            if (rows != 1)
                throw new GotNoMessageException("no affect on database. message wasn't deleted");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DeletingMessageException("internal error. unable to delete message");
        }
    }


    public List<Message> getUserMessages(Integer userId, Integer otherUserId)
            throws GettingMessagesException, GotNoMessagesException {
        List<Object> params = new ArrayList<>();
        params.add(userId);
        params.add(otherUserId);
        params.add(userId);
        params.add(otherUserId);

        try {
            List<Message> list = executor.execQuery(GET_USER_MESSAGES, params, resultSet -> {
                List<Message> lst = new ArrayList<>();

                while (resultSet.next()) {
                    lst.add(parseMessage(resultSet));
                }
                return lst;
            });
            if (list.isEmpty())
                throw new GotNoMessagesException("this users dont have messages");
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new GettingMessagesException("internal error. unable to get messages of this users");
        }
    }

    private Message parseMessage(ResultSet resultSet) throws SQLException {
        return new Message(
                resultSet.getInt("id"),
                resultSet.getInt("sender_id"),
                resultSet.getInt("receiver_id"),
                resultSet.getString("value"),
                new Date(resultSet.getTimestamp("sent_date").getTime()),
                resultSet.getString("checked"),
                resultSet.getString("deleted_sender"),
                resultSet.getString("deleted_receiver")
        );
    }


}
