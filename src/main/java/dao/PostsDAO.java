package dao;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import entities.Post;
import executor.Executor;
import util.DBConnector;
import util.dto.UpdateResult;
import util.exceptions.InvalidDataException;
import util.exceptions.posts.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by yuriy on 22.08.17.
 */
public class PostsDAO {
    private static final String CREATE_POST = "INSERT INTO posts(author_id, receiver_id, value, creation_date) " +
            "VALUES(?, ?, ?, ?)";
    private static final String GET_USER_POSTS = "SELECT * FROM posts WHERE receiver_id = ? ORDER BY creation_date DESC";
    private static final String DELETE_POST = "DELETE FROM posts WHERE id = ?";
    private static final String EDIT_POST = "UPDATE posts SET value = ? WHERE id = ?";
    private static final String GET_POST_BY_ID = "SELECT * FROM posts WHERE id = ?";

    private Executor executor;

    public PostsDAO() {
        this.executor = new Executor();
    }

    public Post getPostById(Integer postId) throws SQLException, GotNoPostException {
        List<Object> params = new ArrayList<>();
        params.add(postId);

        Post post = executor.execQuery(GET_POST_BY_ID, params, resultSet -> {
            if (!resultSet.next())
                return null;
            return parsePost(resultSet);
        });
        if (post == null)
            throw new GotNoPostException("no post with such id");
        return post;
    }

    public Post createPost(Post newPost) throws InvalidDataException, CreationPostException {
        List<Object> params = new ArrayList<>();
        params.add(newPost.getAuthorId());
        params.add(newPost.getReceiverId());
        params.add(newPost.getValue());
        params.add(newPost.getCreationDate());

        try {
            UpdateResult result = executor.execUpdate2(CREATE_POST, params);
            int rows = result.getRowsAffected();
            Long id = result.getGeneratedKey();
            if (rows != 1)
                throw new SQLException("smth went wrong with query");
            return getPostById(id.intValue());
        } catch (MySQLIntegrityConstraintViolationException e) {
            e.printStackTrace();
            throw new InvalidDataException("one or both ids are invalid. No users with such ids");
        } catch (SQLException | GotNoPostException e) {
            e.printStackTrace();
            throw new CreationPostException("internal error. Unable to add post");
        }
        /*try {
            int rows = executor.execUpdate(CREATE_POST, params);
            if (rows != 1)
                throw new SQLException("smth went wrong with query");
            return get
        }
        catch (MySQLIntegrityConstraintViolationException e) {
            e.printStackTrace();
            throw new InvalidDataException("one or both ids are invalid. No users with such ids");
        }
        catch (SQLException e) {
            e.printStackTrace();
            throw new CreationPostException("internal error. Unable to add post");
        }*/
    }

    public Post editPost(String value, Integer postId) throws GotNoPostException, EditingPostException {
        List<Object> params = new ArrayList<>();
        params.add(value);
        params.add(postId);

        try {
            int rows = executor.execUpdate(EDIT_POST, params);
            if (rows != 1)
                throw new GotNoPostException("no affect on database. post wasn't updated. (maybe no post with this id in db?");
            return getPostById(postId);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new EditingPostException("internal error. unable to update post");
        }
    }

    public void deletePost(Integer postId) throws DeletingPostException, GotNoPostException {
        List<Object> params = new ArrayList<>();
        params.add(postId);

        try {
            int rows = executor.execUpdate(DELETE_POST, params);
            if (rows != 1)
                throw new GotNoPostException("no affect on database. post wasn't deleted. (maybe no post with this id in db?");
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DeletingPostException("internal error. unable to delete post");
        }

    }

    public List<Post> getUserPosts(Integer userReceiverId) throws NoPostsException, GettingPostException {
        List<Object> params = new ArrayList<>();
        params.add(userReceiverId);

        List<Post> list = null;
        try {
            list = executor.execQuery(GET_USER_POSTS, params, resultSet -> {
                List<Post> lst = new ArrayList<>();

                while (resultSet.next()) {
                    lst.add(parsePost(resultSet));
                }
                return lst;
            });
            if (list.isEmpty())
                throw new NoPostsException("this user doesnt have posts on his page");
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new GettingPostException("internal error. unable to get posts");
        }

    }

    private Post parsePost(ResultSet resultSet) throws SQLException {
        return new Post(
                resultSet.getInt("id"),
                resultSet.getInt("author_id"),
                resultSet.getInt("receiver_id"),
                resultSet.getString("value"),
                new Date(resultSet.getTimestamp("creation_date").getTime())
        );
    }
}
