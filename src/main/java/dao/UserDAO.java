package dao;

import entities.User;
import executor.Executor;
import util.dto.Name;
import util.exceptions.users.NoUserException;
import util.exceptions.users.UserCreateException;
import util.exceptions.users.UserDeleteException;
import util.exceptions.users.UserUpdateException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class UserDAO {
    private static final String GET_USER_BY_ID = "SELECT * FROM users WHERE id = ?";
    private static final String GET_USER_BY_EMAIL = "SELECT * FROM users WHERE email = ?";
    private static final String CREATE_USER = "INSERT INTO users(email, password, first_name, " +
            "second_name, birth_date, country, phone_num, gender, about, sports, weight, height, img) " +
            "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String DELETE_USER_BY_ID = "DELETE FROM users WHERE id = ?";
    private static final String DELETE_USER_BY_EMAIL = "DELETE FROM users WHERE email = ?";
    private static final String UPDATE_USER = "UPDATE users SET email = ?, password = ?," +
            "first_name = ?, second_name = ?, birth_date = ?, country = ?, phone_num = ?, gender = ?, " +
            "about = ?, sports = ?, weight = ?, height = ?, img = ? WHERE id = ?";
    private static final String GET_USERS_BY_FIRST_NAME = "SELECT * FROM users WHERE first_name = ?";
    private static final String GET_USERS_BY_SECOND_NAME = "SELECT * FROM users WHERE second_name = ?";
    private static final String GET_USERS_BY_FIRST_AND_SECOND_NAME = "SELECT * FROM users WHERE first_name = ? " +
            "AND second_name = ?";
    private static final String GET_ALL_USERS = "SELECT * FROM users";

    private Executor executor;

    public UserDAO() {
        this.executor = new Executor();
    }

    public User getUserById(Integer id) throws NoUserException {
        List<Integer> params = new ArrayList<>();
        params.add(id);

        try {
            User user = executor.execQuery(GET_USER_BY_ID, params, resultSet -> {
                if (!resultSet.next())
                    return null;
                return parseUser(resultSet);
            });
            if (user == null)
                throw new NoUserException("got no user");
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new NoUserException("error occured. can't get user");
        }
        /*try {
            return executor.execQuery(GET_USER_BY_ID, params, resultSet -> {
                if (!resultSet.next()) {
                    throw new NoUserException("no user found");
                }
                return new User(
                        resultSet.getInt("id"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getString("first_name"),
                        resultSet.getString("second_name"),
                        resultSet.getString("birth_date"),
                        resultSet.getString("country"),
                        resultSet.getString("phone_num"),
                        resultSet.getString("gender")
                );
            });
        } catch (SQLException e) {
            e.printStackTrace();
            throw new NoUserException("error occured. can't get user");
        }*/
    }

    public User getUserByEmail(String email) throws NoUserException {
        List<String> params = new ArrayList<>();
        params.add(email);

        try {
            User user = executor.execQuery(GET_USER_BY_EMAIL, params, resultSet -> {
                if (!resultSet.next())
                    return null;
                return parseUser(resultSet);
            });
            if (user == null)
                throw new NoUserException("no user found");
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new NoUserException("error occured. can't get user");
        }
        /*try {
            return executor.execQuery(GET_USER_BY_EMAIL, params, resultSet -> {
                if (!resultSet.next())
                    throw new NoUserException("no user found");
                return new User(
                        resultSet.getInt("id"),
                        resultSet.getString("email"),
                        resultSet.getString("password"),
                        resultSet.getString("first_name"),
                        resultSet.getString("second_name"),
                        resultSet.getString("birth_date"),
                        resultSet.getString("country"),
                        resultSet.getString("phone_num"),
                        resultSet.getString("gender")
                );
            });
        } catch (SQLException e) {
            e.printStackTrace();
            throw new NoUserException("error occured. can't get user");
        }*/
    }

    public User createUser(User newUser) throws UserCreateException{
       List<Object> params = new ArrayList<>();
       params.add(newUser.getEmail());
       params.add(newUser.getPassword());
       params.add(newUser.getFirst_name());
       params.add(newUser.getSecond_name());
       params.add(newUser.getBirth_date());
       params.add(newUser.getCountry());
       params.add(newUser.getPhone_num());
       params.add(newUser.getGender());
       params.add(newUser.getAbout());
       params.add(newUser.getSports());
       params.add(newUser.getWeight());
       params.add(newUser.getHeight());
       params.add(newUser.getImg());
       try {
           executor.execUpdate(CREATE_USER, params);
           return getUserByEmail(newUser.getEmail());
       }
       catch (SQLException | NoUserException e) {
           e.printStackTrace();
           throw new UserCreateException("error occured. User wasn't created");
       }
    }

    public User deleteUserById(Integer id) throws UserDeleteException {
        List<Object> params = new ArrayList<>();
        params.add(id);
        try {
            User userToDelete = getUserById(id);
            executor.execUpdate(DELETE_USER_BY_ID, params);
            return userToDelete;
        }
        catch (NoUserException | SQLException e) {
            e.printStackTrace();
            throw new UserDeleteException("error occured. User wasn't deleted");
        }
    }

    public User deleteUserByEmail(String email) throws UserDeleteException {
        List<Object> params = new ArrayList<>();
        params.add(email);
        try {
            User userToDelete = getUserByEmail(email);
            executor.execUpdate(DELETE_USER_BY_EMAIL, params);
            return userToDelete;
        }
        catch (NoUserException | SQLException e) {
            e.printStackTrace();
            throw new UserDeleteException("error occured. User wasn't deleted");
        }
    }

    public User updateUser(User userToUpdate, Integer id) throws UserUpdateException {
        List<Object> params = new ArrayList<>();
        params.add(userToUpdate.getEmail());
        params.add(userToUpdate.getPassword());
        params.add(userToUpdate.getFirst_name());
        params.add(userToUpdate.getSecond_name());
        params.add(userToUpdate.getBirth_date());
        params.add(userToUpdate.getCountry());
        params.add(userToUpdate.getPhone_num());
        params.add(userToUpdate.getGender());
        params.add(userToUpdate.getAbout());
        params.add(userToUpdate.getSports());
        params.add(userToUpdate.getWeight());
        params.add(userToUpdate.getHeight());
        params.add(userToUpdate.getImg());
        params.add(id);

        try {
            executor.execUpdate(UPDATE_USER, params);
            return getUserById(id);
        } catch (SQLException | NoUserException e) {
            e.printStackTrace();
            throw new UserUpdateException("error occured. User wasn't updated");
        }


    }

    public List<User> getUsersByName(Name fullName) throws NoUserException {
        List<Object> params = new ArrayList<>();
        String query = "";

        if (fullName.getSecond_name().equals("Noname") && fullName.getFirst_name().equals("Noname")) {
            query = GET_ALL_USERS;
        }
        else if (fullName.getSecond_name().equals("Noname")) {
            params.add(fullName.getFirst_name());
            query = GET_USERS_BY_FIRST_NAME;
        }
        else if (fullName.getFirst_name().equals("Noname")) {
            query = GET_USERS_BY_SECOND_NAME;
            params.add((fullName.getSecond_name()));
        }
        else if (!fullName.getSecond_name().equals("Noname") && !fullName.getFirst_name().equals("Noname")) {
            params.add(fullName.getFirst_name());
            params.add(fullName.getSecond_name());
            query = GET_USERS_BY_FIRST_AND_SECOND_NAME;
        }

        try {
            return executor.execQuery(query, params, resultSet -> {
                List<User> list = new ArrayList<>();
                while(resultSet.next()) {
                    User user = parseUser(resultSet);
                    list.add(user);
                }
                return list;
            });
        } catch (SQLException e) {
            e.printStackTrace();
            throw new NoUserException("no users found");
        }
    }

    private User parseUser(ResultSet resultSet) throws SQLException {
        return new User(
                resultSet.getInt("id"),
                resultSet.getString("email"),
                resultSet.getString("password"),
                resultSet.getString("first_name"),
                resultSet.getString("second_name"),
                resultSet.getString("birth_date"),
                resultSet.getString("country"),
                resultSet.getString("phone_num"),
                resultSet.getString("gender"),
                resultSet.getString("about"),
                resultSet.getString("sports"),
                resultSet.getString("weight"),
                resultSet.getString("height"),
                resultSet.getString("img")
        );
    }

    /*public List<User> getAllUsers() {
        List<Object> params = new ArrayList<>();
        return executor.execUpdate()

    }*/
}
