package entities;

import java.util.Date;

/**
 * Created by yuriy on 28.08.17.
 */
public class Comment {
    private Integer id;
    private Integer postId;
    private Integer authorId;
    private String value;
    private Date creationDate;

    public Comment(Integer id, Integer postId, Integer authorId, String value, Date creationDate) {
        this.id = id;
        this.postId = postId;
        this.authorId = authorId;
        this.value = value;
        this.creationDate = creationDate;
    }

    public Comment(Integer postId, Integer authorId, String value, Date creationDate) {
        this.postId = postId;
        this.authorId = authorId;
        this.value = value;
        this.creationDate = creationDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comment comment = (Comment) o;

        if (id != null ? !id.equals(comment.id) : comment.id != null) return false;
        if (!postId.equals(comment.postId)) return false;
        if (!authorId.equals(comment.authorId)) return false;
        if (!value.equals(comment.value)) return false;
        return creationDate.equals(comment.creationDate);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + postId.hashCode();
        result = 31 * result + authorId.hashCode();
        result = 31 * result + value.hashCode();
        result = 31 * result + creationDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", postId=" + postId +
                ", authorId=" + authorId +
                ", value='" + value + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}