package entities;

import java.util.Date;

/**
 * Created by yuriy on 02.09.17.
 */
public class Message {
    private Integer id;
    private Integer senderId;
    private Integer receiverId;
    private String value;
    private Date sentDate;
    private String checked;
    private String deletedReceiver;
    private String deletedSender;

    public Message(Integer id,
                   Integer senderId,
                   Integer receiverId,
                   String value,
                   Date sentDate,
                   String checked,
                   String deletedReceiver,
                   String deletedSender) {
        this.id = id;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.value = value;
        this.sentDate = sentDate;
        this.checked = checked;
        this.deletedReceiver = deletedReceiver;
        this.deletedSender = deletedSender;
    }

    public Message(Integer senderId,
                   Integer receiverId,
                   String value,
                   Date sentDate,
                   String checked,
                   String deletedReceiver,
                   String deletedSender) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.value = value;
        this.sentDate = sentDate;
        this.checked = checked;
        this.deletedReceiver = deletedReceiver;
        this.deletedSender = deletedSender;
    }

    public Message(Integer id,
                   Integer senderId,
                   Integer receiverId,
                   String value,
                   Date sentDate) {
        this.id = id;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.value = value;
        this.sentDate = sentDate;
    }

    public Message(Integer senderId,
                   Integer receiverId,
                   String value,
                   Date sentDate) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.value = value;
        this.sentDate = sentDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public Integer getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Integer receiverId) {
        this.receiverId = receiverId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public String getDeletedReceiver() {
        return deletedReceiver;
    }

    public void setDeletedReceiver(String deletedReceiver) {
        this.deletedReceiver = deletedReceiver;
    }

    public String getDeletedSender() {
        return deletedSender;
    }

    public void setDeletedSender(String deletedSender) {
        this.deletedSender = deletedSender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (id != null ? !id.equals(message.id) : message.id != null) return false;
        if (!senderId.equals(message.senderId)) return false;
        if (!receiverId.equals(message.receiverId)) return false;
        if (!value.equals(message.value)) return false;
        if (!sentDate.equals(message.sentDate)) return false;
        if (checked != null ? !checked.equals(message.checked) : message.checked != null) return false;
        if (deletedReceiver != null ? !deletedReceiver.equals(message.deletedReceiver) : message.deletedReceiver != null)
            return false;
        return deletedSender != null ? deletedSender.equals(message.deletedSender) : message.deletedSender == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + senderId.hashCode();
        result = 31 * result + receiverId.hashCode();
        result = 31 * result + value.hashCode();
        result = 31 * result + sentDate.hashCode();
        result = 31 * result + (checked != null ? checked.hashCode() : 0);
        result = 31 * result + (deletedReceiver != null ? deletedReceiver.hashCode() : 0);
        result = 31 * result + (deletedSender != null ? deletedSender.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", senderId=" + senderId +
                ", receiverId=" + receiverId +
                ", value='" + value + '\'' +
                ", sentDate=" + sentDate +
                ", checked='" + checked + '\'' +
                ", deletedReceiver='" + deletedReceiver + '\'' +
                ", deletedSender='" + deletedSender + '\'' +
                '}';
    }
}
