package entities;

import java.util.*;

/**
 * Created by yuriy on 22.08.17.
 */
public class Post {
    private Integer id;
    private Integer authorId;
    private Integer receiverId;
    private String value;
    private Date creationDate;
    private Map<Comment, User> comments = new LinkedHashMap<>();

    public Post(Integer id, Integer authorId, Integer receiverId, String value, Date creationDate) {
        this.id = id;
        this.authorId = authorId;
        this.receiverId = receiverId;
        this.value = value;
        this.creationDate = creationDate;
    }

    public Post(Integer authorId, Integer receiverId, String value, Date creationDate) {
        this.authorId = authorId;
        this.receiverId = receiverId;
        this.value = value;
        this.creationDate = creationDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public Integer getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Integer receiverId) {
        this.receiverId = receiverId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Map<Comment, User> getComments() {
        return comments;
    }

    public void setComments(Map<Comment, User> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Post post = (Post) o;

        if (id != null ? !id.equals(post.id) : post.id != null) return false;
        if (!authorId.equals(post.authorId)) return false;
        if (!receiverId.equals(post.receiverId)) return false;
        if (!value.equals(post.value)) return false;
        return creationDate.equals(post.creationDate);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + authorId.hashCode();
        result = 31 * result + receiverId.hashCode();
        result = 31 * result + value.hashCode();
        result = 31 * result + creationDate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", authorId=" + authorId +
                ", receiverId=" + receiverId +
                ", value='" + value + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
