package entities;

import java.util.Objects;

public class User {
    private Integer id;
    private String email;
    private String password;
    private String first_name;
    private String second_name;
    private String birth_date;
    private String country;
    private String phone_num;
    private String gender;
    private String about;
    private String sports;
    private String weight;
    private String height;
    private String img;

    public User(Integer id,
                String email,
                String password,
                String first_name,
                String second_name,
                String birth_date,
                String country,
                String phone_num,
                String gender) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.first_name = first_name;
        this.second_name = second_name;
        this.birth_date = birth_date;
        this.country = country;
        this.phone_num = phone_num;
        this.gender = gender;
    }

    public User(String email,
                String password,
                String first_name,
                String second_name,
                String birth_date,
                String country,
                String phone_num,
                String gender) {
        this.email = email;
        this.password = password;
        this.first_name = first_name;
        this.second_name = second_name;
        this.birth_date = birth_date;
        this.country = country;
        this.phone_num = phone_num;
        this.gender = gender;
    }

    public User(String email,
                String password,
                String first_name,
                String second_name,
                String birth_date,
                String country,
                String phone_num,
                String gender,
                String about,
                String sports,
                String weight,
                String height,
                String img) {
        this.email = email;
        this.password = password;
        this.first_name = first_name;
        this.second_name = second_name;
        this.birth_date = birth_date;
        this.country = country;
        this.phone_num = phone_num;
        this.gender = gender;
        this.about = about;
        this.sports = sports;
        this.weight = weight;
        this.height = height;
        this.img = img;
    }

    public User(Integer id,
                String email,
                String password,
                String first_name,
                String second_name,
                String birth_date,
                String country,
                String phone_num,
                String gender,
                String about,
                String sports,
                String weight,
                String height,
                String img) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.first_name = first_name;
        this.second_name = second_name;
        this.birth_date = birth_date;
        this.country = country;
        this.phone_num = phone_num;
        this.gender = gender;
        this.about = about;
        this.sports = sports;
        this.weight = weight;
        this.height = height;
        this.img = img;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(String phone_num) {
        this.phone_num = phone_num;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", first_name='" + first_name + '\'' +
                ", second_name='" + second_name + '\'' +
                ", birth_date='" + birth_date + '\'' +
                ", country='" + country + '\'' +
                ", phone_num='" + phone_num + '\'' +
                ", gender='" + gender + '\'' +
                ", about='" + about + '\'' +
                ", sports='" + sports + '\'' +
                ", weight='" + weight + '\'' +
                ", height='" + height + '\'' +
                ", img='" + img + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        if (!email.equals(user.email)) return false;
        if (!password.equals(user.password)) return false;
        if (!first_name.equals(user.first_name)) return false;
        if (!second_name.equals(user.second_name)) return false;
        if (!birth_date.equals(user.birth_date)) return false;
        if (!country.equals(user.country)) return false;
        if (!phone_num.equals(user.phone_num)) return false;
        if (!gender.equals(user.gender)) return false;
        if (about != null ? !about.equals(user.about) : user.about != null) return false;
        if (sports != null ? !sports.equals(user.sports) : user.sports != null) return false;
        if (weight != null ? !weight.equals(user.weight) : user.weight != null) return false;
        if (height != null ? !height.equals(user.height) : user.height != null) return false;
        return img != null ? img.equals(user.img) : user.img == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + email.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + first_name.hashCode();
        result = 31 * result + second_name.hashCode();
        result = 31 * result + birth_date.hashCode();
        result = 31 * result + country.hashCode();
        result = 31 * result + phone_num.hashCode();
        result = 31 * result + gender.hashCode();
        result = 31 * result + (about != null ? about.hashCode() : 0);
        result = 31 * result + (sports != null ? sports.hashCode() : 0);
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        result = 31 * result + (height != null ? height.hashCode() : 0);
        result = 31 * result + (img != null ? img.hashCode() : 0);
        return result;
    }
}
