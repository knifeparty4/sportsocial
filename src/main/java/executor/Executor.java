package executor;

import util.ConnectionPool;
import util.dto.UpdateResult;

import java.sql.*;
import java.util.*;

/**
 * Created by yuriy on 11.08.17.
 */
public class Executor {

    //TODO:внедрить
    public UpdateResult execUpdate2(String update, List<?> params) throws SQLException{
        Connection connection = ConnectionPool.getInstance().getConnection();
        PreparedStatement stmt = connection.prepareStatement(update, Statement.RETURN_GENERATED_KEYS);
        paramsSetter(stmt, params);
        UpdateResult updateResult = new UpdateResult();
        updateResult.setRowsAffected(stmt.executeUpdate());
        ResultSet generatedKeys = stmt.getGeneratedKeys();
        if (generatedKeys.next())
            updateResult.setGeneratedKey(generatedKeys.getLong(1));
        stmt.close();
        ConnectionPool.getInstance().putConnection(connection);

        return updateResult;
    }

    public Integer execUpdate(String update, List<?> params) throws SQLException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        PreparedStatement stmt = connection.prepareStatement(update);
        paramsSetter(stmt, params);
        int result = stmt.executeUpdate();
        stmt.close();
        ConnectionPool.getInstance().putConnection(connection);

        return result;
    }

    public <T> T execQuery(String query, List<?> params, ResultHandler<T> handler) throws SQLException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        PreparedStatement stmt = connection.prepareStatement(query);
        paramsSetter(stmt, params);
        stmt.execute();
        ResultSet result = stmt.getResultSet();
        T value = handler.handle(result);
        result.close();
        stmt.close();
        ConnectionPool.getInstance().putConnection(connection);

        return value;
    }

    private void paramsSetter(PreparedStatement stmt, List<?> params) throws SQLException {
        if (params.isEmpty())
            return;

        for (int i = 0; i < params.size(); i++) {
            if (params.get(i) instanceof Integer)
                stmt.setInt(i + 1, (Integer) params.get(i));
            else if (params.get(i) instanceof String)
                stmt.setString(i + 1, (String) params.get(i));
            else if (params.get(i) instanceof java.util.Date)
                /*stmt.setDate(i + 1, (Date) params.get(i));*/
                stmt.setObject(i + 1, new Timestamp(((java.util.Date) params.get(i)).getTime()));
        }
    }
}
