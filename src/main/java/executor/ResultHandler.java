package executor;

import util.exceptions.users.NoUserException;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by yuriy on 11.08.17.
 */
public interface ResultHandler<T> {
    T handle(ResultSet resultSet) throws SQLException;
}
