package service;

import dao.CommentsDAO;
import entities.Comment;
import util.DBConnector;
import util.Validator;
import util.exceptions.InvalidDataException;
import util.exceptions.comments.*;
import util.exceptions.posts.EditingPostException;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by yuriy on 28.08.17.
 */
public class CommentsService {
    private CommentsDAO commentsDAO = new CommentsDAO();

    public Comment getCommentById(Integer commentId)
            throws InvalidDataException, SQLException, GotNoCommentException {
        if (!Validator.validId(commentId)) {
            throw new InvalidDataException("invalid comment id");
        }
        return commentsDAO.getCommentById(commentId);
    }

    public Comment createComment(Comment comment)
            throws InvalidDataException, CreationCommentException {
        if (!Validator.validId(comment.getAuthorId()) || !Validator.validId(comment.getPostId())) {
            throw new InvalidDataException("invalid comment authorId or postId to create");
        }
        return commentsDAO.createComment(comment);
    }

    public Comment editComment(String value, Integer commentId)
            throws InvalidDataException, GotNoCommentException, EditingCommentException {
        if (!Validator.validId(commentId) || !Validator.validStringValues(value)) {
            throw new InvalidDataException("invalid comment value or id to update");
        }
        return commentsDAO.editComment(value, commentId);
    }

    public void deleteComment(Integer commentId)
            throws InvalidDataException, GotNoCommentException, DeletingCommentException {
        if (!Validator.validId(commentId)) {
            throw new InvalidDataException("invalid comment if to delete");
        }
        commentsDAO.deleteComment(commentId);
    }

    public List<Comment> getPostComments(Integer postId)
            throws InvalidDataException, GettingCommentsException, GotNoCommentException {
        if (!Validator.validId(postId)) {
            throw new InvalidDataException("invalid postId");
        }
        return commentsDAO.getPostComments(postId);
    }



}
