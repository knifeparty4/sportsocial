package service;

import dao.FriendsDAO;
import entities.User;
import util.DBConnector;
import util.Validator;
import util.enums.FriendsRequests;
import util.enums.FriendshipStatus;
import util.exceptions.*;
import util.exceptions.friends.*;
import util.exceptions.users.NoUserException;
import util.exceptions.users.NoUserFriendRequestsException;

import java.util.List;

/**
 * Created by yuriy on 19.08.17.
 */
//TODO: сам себя в друзья???
public class FriendsService {
    private FriendsDAO friendsDAO = new FriendsDAO();


    public List<User> getFriendsByUserId(Integer userId)
            throws InvalidDataException, NoUserException, GettingFriendsException, GotNoFriendsException {

        if (!Validator.validId(userId)) {
            throw new InvalidDataException("invalid id to get friends/friend requests");
        }
        return friendsDAO.getUserFriendsById(userId);
    }

    public List<User> getUserFriendRequestsById(Integer userId, FriendsRequests requestType)
            throws InvalidDataException, NoUserException, GettingFriendRequestsListException, NoUserFriendRequestsException {

        if (!Validator.validId(userId) || requestType == null) {
            throw new InvalidDataException("invalid id to get friends/friend requests");
        }
        return friendsDAO.getUserFriendRequestsById(userId, requestType);
    }

    public void acceptOrDeclineFriend(Integer requesterUserId, Integer accepterUserId, FriendshipStatus desirableStatus)
            throws InvalidDataException, FriendAcceptException, FriendshipDeleteException, NoFriendsDeletedException {

        if (!Validator.validId(requesterUserId) && !Validator.validId(accepterUserId) || desirableStatus == null) {
            throw new InvalidDataException("invalid id1 or id accept or decline frindship");
        }

        if (desirableStatus == FriendshipStatus.FRIEND) {
            try {
                FriendshipStatus status = friendsDAO.getUserFriendshipStatus(requesterUserId, accepterUserId);
                if (status == FriendshipStatus.REQUETED_BY_USER)
                    friendsDAO.acceptFriend(requesterUserId, accepterUserId);
                else throw new GettingFriendshipStatusException("got no such request to friends");
            } catch (GettingFriendshipStatusException e) {
                e.printStackTrace();
                throw new InvalidDataException("got no such pair (requester1 reciever2) pair");
            }

        }
        else if (desirableStatus == FriendshipStatus.NOT_FRIEND) //decline
            friendsDAO.deleteFriendship(accepterUserId, requesterUserId);

    }


    public void deleteFriendship(Integer requestingUserId, Integer userToFinishWithId)
            throws InvalidDataException, FriendshipDeleteException, NoFriendsDeletedException {

        if (!Validator.validId(requestingUserId) || !Validator.validId(userToFinishWithId)) {
            throw new InvalidDataException("invalid id1 or id to finish friendship");
        }
        friendsDAO.deleteFriendship(requestingUserId, userToFinishWithId);
    }


    public FriendshipStatus getUserFriendshipStatus(Integer requestingUserId, Integer requestedUserId)
            throws InvalidDataException, GettingFriendshipStatusException {

        if (!Validator.validId(requestingUserId) || !Validator.validId(requestedUserId)) {
            throw new InvalidDataException("invalid id1 or id to request friendship status");
        }

        return friendsDAO.getUserFriendshipStatus(requestingUserId, requestedUserId);
    }


    public void sendFriendRequest(Integer userId, Integer newFriendId)
            throws InvalidDataException, FriendRequestException, DuplicateFriendsPairException {

        if (!Validator.validId(userId) || !Validator.validId(newFriendId) || userId.equals(newFriendId)) {
            throw new InvalidDataException("invalid id of requested user or requesting user");
        }
        try {
            new UserService().getUserById(newFriendId);
            if (getUserFriendshipStatus(newFriendId, userId) == FriendshipStatus.NOT_FRIEND)
                friendsDAO.sendFriendRequest(userId, newFriendId);
            else throw new GettingFriendshipStatusException();
        }
        catch (NoUserException e) {
            e.printStackTrace();
            throw new FriendRequestException("no user to start friendship");
        }
        catch (GettingFriendshipStatusException e) {
            e.printStackTrace();
            throw new FriendRequestException("this pair already exists");
        }
    }
}
