package service;

import dao.MessagesDAO;
import entities.Message;
import util.DBConnector;
import util.Validator;
import util.exceptions.InvalidDataException;
import util.exceptions.messages.CreationMessageException;
import util.exceptions.messages.GettingMessagesException;
import util.exceptions.messages.GotNoMessageException;
import util.exceptions.messages.GotNoMessagesException;

import java.util.List;

/**
 * Created by yuriy on 02.09.17.
 */
public class MessageService {
    private MessagesDAO messagesDAO = new MessagesDAO();

    public Message getMessageById(Integer msgId) throws InvalidDataException, GotNoMessageException {
        if (!Validator.validId(msgId))
            throw new InvalidDataException("invalid msgId to get");

        return messagesDAO.getMessageById(msgId);
    }

    public void createMessage(Message msg) throws InvalidDataException, CreationMessageException {
        if (!Validator.validMessage(msg)) {
            throw new InvalidDataException("invalid message fields to create");
        }

        messagesDAO.createMessage(msg);

    }

    public void deleteMessage(Integer msgId, Integer userFromId) throws InvalidDataException {
        if (!Validator.validId(msgId) && !Validator.validId(userFromId)) {
            throw new InvalidDataException("invalid msgId or userFromId to delete msg from userFromId");
        }

        messagesDAO.deleteMessage(msgId, userFromId);
    }

    /*public List<Message> getUserMessages(Integer userId) {

    }*/

    public List<Message> getUserMessages(Integer userId, Integer otherUserId)
            throws InvalidDataException, GettingMessagesException, GotNoMessagesException {
        if (!Validator.validId(userId) || !Validator.validId(otherUserId)) {
            throw new InvalidDataException("invalid userId or otherUserId to get messages");
        }

        return messagesDAO.getUserMessages(userId, otherUserId);
    }
}
