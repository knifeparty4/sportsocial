package service;

import dao.CommentsDAO;
import dao.PostsDAO;
import entities.Comment;
import entities.Post;
import entities.User;
import javafx.geometry.Pos;
import util.DBConnector;
import util.Validator;
import util.enums.FriendshipStatus;
import util.exceptions.InvalidDataException;
import util.exceptions.comments.GettingCommentsException;
import util.exceptions.comments.GotNoCommentException;
import util.exceptions.friends.GettingFriendshipStatusException;
import util.exceptions.posts.*;
import util.exceptions.users.NoUserException;

import java.sql.SQLException;
import java.util.*;

/**
 * Created by yuriy on 22.08.17.
 */
public class PostsService {
    private PostsDAO postsDAO = new PostsDAO();

    public Post getPostById(Integer postId) throws InvalidDataException, GotNoPostException {
        if (!Validator.validId(postId)) {
            throw new InvalidDataException();
        }
        try {
            return postsDAO.getPostById(postId);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new GotNoPostException("sql error");
        }
    }
    public void createPost(Post newPost)
            throws CreationPostException, InvalidDataException {

        if (!Validator.validId(newPost.getAuthorId()) || !Validator.validId(newPost.getReceiverId()) ||
                !Validator.validStringValues(newPost.getValue()) || !Validator.validDate(newPost.getCreationDate())) {
            throw new InvalidDataException("invalid params to create post");
        }
        try {
            FriendshipStatus status = new FriendsService().getUserFriendshipStatus(newPost.getAuthorId(), newPost.getReceiverId());
            if (status == FriendshipStatus.FRIEND || newPost.getAuthorId().equals(newPost.getReceiverId()))
                postsDAO.createPost(newPost);
            else throw new InvalidDataException();
        } catch (InvalidDataException | GettingFriendshipStatusException e) {
            e.printStackTrace();
            throw new InvalidDataException("receiver and author have to be friends");
        }
    }

    public Post editPost(String value, Integer postId)
            throws InvalidDataException, GotNoPostException, EditingPostException {
        if (!Validator.validId(postId) || !Validator.validStringValues(value)) {
            throw new InvalidDataException("invalid postId or postValue to edit");
        }

        return postsDAO.editPost(value, postId);
    }

    public void deletePost(Integer postId)
            throws InvalidDataException, GotNoPostException, DeletingPostException {

        if (!Validator.validId(postId)) {
            throw new InvalidDataException("invalid post id");
        }
        postsDAO.deletePost(postId);
    }

    public List<Post> getUserPosts(Integer userId)
            throws NoPostsException, GettingPostException, InvalidDataException {

        if (!Validator.validId(userId)) {
            throw new InvalidDataException("invalid userid");
        }
        List<Post> posts = postsDAO.getUserPosts(userId);
        for (Post post: posts) {
            try {
                List<Comment> commentList = new CommentsService().getPostComments(post.getId());
                Map<Comment, User> commentsMap = new LinkedHashMap<>();
                UserService userService = new UserService();
                for (Comment comment: commentList) {
                    commentsMap.put(comment, userService.getUserById(comment.getAuthorId()));
                }
                post.setComments(commentsMap);
                /*post.setComments(new CommentsService().getPostComments(post.getId()));*/
            } catch (NoUserException | GotNoCommentException | GettingCommentsException e) {
                //TODO:!!!!
                ///e.printStackTrace();
            }
        }
        return posts;
    }
}
