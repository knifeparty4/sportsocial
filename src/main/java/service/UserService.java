package service;

import dao.UserDAO;
import entities.User;
import util.DBConnector;
import util.dto.Name;
import util.Validator;
import util.exceptions.*;
import util.exceptions.users.NoUserException;
import util.exceptions.users.UserCreateException;
import util.exceptions.users.UserDeleteException;
import util.exceptions.users.UserUpdateException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by yuriy on 12.08.17.
 */
public class UserService {
    private UserDAO userDAO = new UserDAO();

    public User getUserById(Integer id) throws InvalidDataException, NoUserException {
        if (!Validator.validId(id)) {
            throw new InvalidDataException("id must be > 0");
        }

        return userDAO.getUserById(id);
    }

    public User getUserByEmail(String email) throws InvalidDataException, NoUserException {
        if (!Validator.validEmail(email)) {
            throw new InvalidDataException("invalid email");
        }

        return userDAO.getUserByEmail(email);
    }

    public User createUser(User newUser, BufferedImage image) throws InvalidDataException, UserCreateException {
        if (!Validator.validUser(newUser)) {
            throw new InvalidDataException("invalid user to create");
        }
        if (!Validator.validHeightWeight(newUser.getHeight())) {
            newUser.setHeight("");
        }
        if (!Validator.validHeightWeight(newUser.getWeight())) {
            newUser.setWeight("");
        }
        if (image == null)
            newUser.setImg("default.png");
        else {
            try {
                ImageIO.write(
                        image,
                        "png",
                        new File("/home/yuriy/webImages/" + newUser.getPhone_num() + ".png")
                );
                newUser.setImg(newUser.getPhone_num() + ".png");
            } catch (IOException e) {
                e.printStackTrace();
                newUser.setImg("default.png");
            }
        }

        return userDAO.createUser(newUser);
    }

    public User deleteUserById(Integer id) throws UserDeleteException, InvalidDataException {
        if (!Validator.validId(id)) {
            throw new InvalidDataException("invalid id to delete user");
        }

        return userDAO.deleteUserById(id);
    }

    public User deleteUserByEmail(String email) throws UserDeleteException, InvalidDataException {
        if (!Validator.validEmail(email)) {
            throw new InvalidDataException("invalid id to delete user");
        }

        return userDAO.deleteUserByEmail(email);
    }

    public User updateUser(User userToUpdate, Integer id, BufferedImage image) throws InvalidDataException, UserUpdateException {
        if (!Validator.validUser(userToUpdate) || !Validator.validId(id)) {
            throw new InvalidDataException("invalid user field(s)");
        }
        if (!Validator.validHeightWeight(userToUpdate.getHeight())) {
            userToUpdate.setHeight("");
        }
        if (!Validator.validHeightWeight(userToUpdate.getWeight())) {
            userToUpdate.setWeight("");
        }
        if (image == null)
            userToUpdate.setImg("default.png");
        else {
            try {
                ImageIO.write(
                        image,
                        "png",
                        new File("/home/yuriy/webImages/" + userToUpdate.getImg())
                );
                /*userToUpdate.setImg();
                userToUpdate.setImg(userToUpdate.getPhone_num() + ".png");*/
            } catch (IOException e) {
                e.printStackTrace();
                userToUpdate.setImg("default.png");
            }
        }

        return userDAO.updateUser(userToUpdate, id);
    }

    public List<User> getUsersByName(Name fullName) throws InvalidDataException, NoUserException {
        if (!Validator.validNames(fullName.getFirst_name(), fullName.getSecond_name())) {
            throw new InvalidDataException("invalid names to get users");
        }

        return userDAO.getUsersByName(fullName);
    }


}
