package servlets.comments;

import entities.Comment;
import entities.Post;
import entities.User;
import service.CommentsService;
import service.PostsService;
import util.exceptions.InvalidDataException;
import util.exceptions.comments.CreationCommentException;
import util.exceptions.posts.CreationPostException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

/**
 * Created by yuriy on 30.08.17.
 */
@WebServlet(name = "createCommentServlet", urlPatterns = "/createComment")
public class CreateCommentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        User sessionUser = (User)(session.getAttribute("userObject"));
        CommentsService commentsService = new CommentsService();

        int authorId = sessionUser.getId();
        Date creationDate = new Date();
        String commentValue = req.getParameter("commentValue");
        int receiverId = -1;
        int postId = -1;
        try {
            receiverId = Integer.parseInt(req.getParameter("receiverId"));
            postId = Integer.parseInt(req.getParameter("postId"));
            commentsService.createComment(new Comment(postId, authorId, commentValue, creationDate));
            session.setAttribute("commentCreationSuccess", true);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        } catch (CreationCommentException | InvalidDataException e) {
            e.printStackTrace();
            session.setAttribute("commentCreationSuccess", false);
        }

        if (authorId == receiverId)
            resp.sendRedirect("/myProfile");
        else resp.sendRedirect("/otherUser?id=" + receiverId);

        //TODO:проверить: работает ли это вообще?
        session.setAttribute("commentCreationSuccess", null);
    }
}
