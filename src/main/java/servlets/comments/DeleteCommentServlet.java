package servlets.comments;

import entities.User;
import service.CommentsService;
import service.PostsService;
import util.exceptions.InvalidDataException;
import util.exceptions.comments.DeletingCommentException;
import util.exceptions.comments.GotNoCommentException;
import util.exceptions.posts.DeletingPostException;
import util.exceptions.posts.GotNoPostException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by yuriy on 30.08.17.
 */
@WebServlet(name = "deleteCommentServlet", urlPatterns = "/deleteComment")
public class DeleteCommentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        User sessionUser = (User)(session.getAttribute("userObject"));
        CommentsService commentsService = new CommentsService();

        int receiverId = -1;
        int commentId = -1;
        try {
            receiverId = Integer.parseInt(req.getParameter("receiverId"));
            commentId = Integer.parseInt(req.getParameter("commentId"));
            commentsService.deleteComment(commentId);
            req.setAttribute("deleteCommentSuccess", true);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        } catch (DeletingCommentException | InvalidDataException | GotNoCommentException e) {
            e.printStackTrace();
            req.setAttribute("deleteCommentSuccess", false);
        }

        if (receiverId == sessionUser.getId())
            req.getRequestDispatcher("/myProfile").forward(req, resp);
        else req.getRequestDispatcher("/otherUser?id=" + receiverId).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
