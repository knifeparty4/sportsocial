package servlets.comments;

import entities.User;
import service.CommentsService;
import service.PostsService;
import util.exceptions.InvalidDataException;
import util.exceptions.comments.EditingCommentException;
import util.exceptions.comments.GotNoCommentException;
import util.exceptions.posts.EditingPostException;
import util.exceptions.posts.GotNoPostException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by yuriy on 30.08.17.
 */
@WebServlet(name = "editCommentServlet", urlPatterns = "/editComment")
public class EditCommentServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        int receiverId = -1;
        User sessionUser = (User)(session.getAttribute("userObject"));
        CommentsService commentsService = new CommentsService();

        try {
            //TODO: нужна проверочка, что пользователь в сессии имеет право на удаление
            String value = req.getParameter("editCommentValue");
            receiverId = Integer.parseInt(req.getParameter("receiverId"));
            int commentId = Integer.parseInt(req.getParameter("commentId"));

            commentsService.editComment(value, commentId);
            session.setAttribute("editingCommentSuccess", true);
            session.setAttribute("noComment", false);

        } catch (InvalidDataException | EditingCommentException | NumberFormatException e) {
            e.printStackTrace();
            session.setAttribute("editingCommentSuccess", false);
            session.setAttribute("noComment", false);
        } catch (GotNoCommentException e) {
            e.printStackTrace();
            session.setAttribute("noComment", true);
            session.setAttribute("editingCommentSuccess", false);
        }
        if (receiverId == sessionUser.getId())
            resp.sendRedirect("/myProfile");
        else resp.sendRedirect("/otherUser?id=" + receiverId);

        //TODO:!!!!!!
        session.setAttribute("noPost", null);
        session.setAttribute("editingPostSuccess", null);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
