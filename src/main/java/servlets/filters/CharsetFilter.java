package servlets.filters;

import entities.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by yuriy on 14.08.17.
 */
@WebFilter(filterName = "charsetFilter"/*, urlPatterns = "*//*"*/)
public class CharsetFilter implements javax.servlet.Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        HttpServletResponse resp = (HttpServletResponse)response;
        resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        chain.doFilter(request, response);
        resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    }

    @Override
    public void destroy() {

    }
}
