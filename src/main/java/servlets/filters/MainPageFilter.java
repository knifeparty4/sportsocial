package servlets.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by yuriy on 24.08.17.
 */
/*
@WebFilter(filterName = "charsetFilter", urlPatterns = "/MyProfile.jsp")
*/
public class MainPageFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    //TODO: закрыть доступ к jsp напрямую и вообще в строке
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        /*HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.sendError(HttpServletResponse.SC_FORBIDDEN);*/

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
