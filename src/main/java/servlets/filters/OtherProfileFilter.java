package servlets.filters;

import entities.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by yuriy on 04.09.17.
 */
/*
@WebFilter(filterName = "otherProfileFilter", urlPatterns = "/otherUser")
*/
public class OtherProfileFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse resp = (HttpServletResponse)response;
        HttpServletRequest req = (HttpServletRequest)request;
        HttpSession session = req.getSession();
        User sessionUser = (User) session.getAttribute("userObject");
        try {
            Integer id = Integer.parseInt(request.getParameter("id"));
            if (id.equals(sessionUser.getId())) {
                resp.sendRedirect("/myProfile");
                return;
            }
        }
        catch (NumberFormatException e) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        chain.doFilter(request, response);
        req = (HttpServletRequest)request;
        session = req.getSession();
        session.setAttribute("successMessageSent", null);
    }

    @Override
    public void destroy() {

    }
}
