package servlets.friendship;

import entities.User;
import service.FriendsService;
import util.enums.FriendshipStatus;
import util.exceptions.InvalidDataException;
import util.exceptions.friends.FriendAcceptException;
import util.exceptions.friends.FriendshipDeleteException;
import util.exceptions.friends.NoFriendsDeletedException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by yuriy on 21.08.17.
 */
@WebServlet(name = "acceptFriendshipServlet", urlPatterns = "/acceptFriendship")
public class AcceptFriendshipServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*HttpSession session = req.getSession();
        Integer userId = ((User)session.getAttribute("userObject")).getId();
        Integer userFromPageId = -1;
        try {
            userFromPageId = Integer.parseInt(req.getParameter("id"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        FriendsService friendsService = new FriendsService();*/

        FriendsService friendsService = new FriendsService();
        Integer userFromPageId = -1;
        try {
            HttpSession session = req.getSession();
            Integer userId = ((User)session.getAttribute("userObject")).getId();
            userFromPageId = Integer.parseInt(req.getParameter("id"));
            friendsService.acceptOrDeclineFriend(userFromPageId, userId, FriendshipStatus.FRIEND);
            req.setAttribute("noSuchFriendshipRequest", false);
            req.setAttribute("successAccept", true);
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        catch (InvalidDataException e) { //нет такой пары (заявки), ошибка параметров
            e.printStackTrace();
            req.setAttribute("noSuchFriendshipRequest", true);
            req.setAttribute("successAccept", false);
        }
        catch (FriendAcceptException | NoFriendsDeletedException | FriendshipDeleteException e) { //внутренняя ошибка
            e.printStackTrace();
            req.setAttribute("noSuchFriendshipRequest", false);
            req.setAttribute("successAccept", false);
        }

        req.getRequestDispatcher("/otherUser?id=" + userFromPageId).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
