package servlets.friendship;

import entities.User;
import service.FriendsService;
import util.exceptions.InvalidDataException;
import util.exceptions.friends.FriendshipDeleteException;
import util.exceptions.friends.NoFriendsDeletedException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by yuriy on 22.08.17.
 */
@WebServlet(name = "deleteFriendshipServlet", urlPatterns = "/deleteFriendship")
public class DeleteFriendshipRelationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*HttpSession session = req.getSession();
        Integer userId = ((User)session.getAttribute("userObject")).getId();
        Integer userFromPageId = -1;
        try {
            userFromPageId = Integer.parseInt(req.getParameter("id"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        FriendsService friendsService = new FriendsService();*/

        FriendsService friendsService = new FriendsService();
        Integer userFromPageId = -1;

        try {
            HttpSession session = req.getSession();
            Integer userId = ((User)session.getAttribute("userObject")).getId();
            userFromPageId = Integer.parseInt(req.getParameter("id"));
            friendsService.deleteFriendship(userId, userFromPageId);
            req.setAttribute("noFriendshipFound", false);
            req.setAttribute("successFriendDelete", true);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        } catch (InvalidDataException | FriendshipDeleteException e) {
            e.printStackTrace();
            req.setAttribute("noFriendshipFound", false);
            req.setAttribute("successFriendDelete", false);
        } catch (NoFriendsDeletedException e) {
            e.printStackTrace();
            req.setAttribute("noFriendshipFound", true);
            req.setAttribute("successFriendDelete", false);
        }

        req.getRequestDispatcher("/otherUser?id=" + userFromPageId).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
