package servlets.friendship;

import entities.User;
import service.FriendsService;
import util.exceptions.InvalidDataException;
import util.exceptions.friends.DuplicateFriendsPairException;
import util.exceptions.friends.FriendRequestException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by yuriy on 21.08.17.
 */
@WebServlet(name = "sendFriendshipRequest", urlPatterns = "/sendFriendship")
public class SendFriendshipRequest extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*HttpSession session = req.getSession();
        Integer userId = ((User)session.getAttribute("userObject")).getId();
        Integer userFromPageId = -1;
        try {
            userFromPageId = Integer.parseInt(req.getParameter("id"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        FriendsService friendsService = new FriendsService();*/

        FriendsService friendsService = new FriendsService();
        Integer userFromPageId = -1;

        try {
            HttpSession session = req.getSession();
            Integer userId = ((User)session.getAttribute("userObject")).getId();
            userFromPageId = Integer.parseInt(req.getParameter("id"));
            friendsService.sendFriendRequest(userId, userFromPageId);
            req.setAttribute("requestSendSuccess", true);
            req.setAttribute("duplicateFriends", false);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        } catch (InvalidDataException | FriendRequestException e) {
            e.printStackTrace();
            req.setAttribute("requestSendSuccess", false);
            req.setAttribute("duplicateFriends", false);
        } catch (DuplicateFriendsPairException e) {
            e.printStackTrace();
            req.setAttribute("requestSendSuccess", false);
            req.setAttribute("duplicateFriends", true);
        }

        req.getRequestDispatcher("/otherUser?id=" + userFromPageId).forward(req, resp);
        /*resp.sendRedirect("/otherUser?id=" + userFromPageId);*/
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
