package servlets.friendship;

import entities.User;
import service.FriendsService;
import util.enums.FriendsRequests;
import util.exceptions.InvalidDataException;
import util.exceptions.friends.GettingFriendRequestsListException;
import util.exceptions.friends.GettingFriendsException;
import util.exceptions.friends.GotNoFriendsException;
import util.exceptions.users.NoUserException;
import util.exceptions.users.NoUserFriendRequestsException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created by yuriy on 21.08.17.
 */
@WebServlet(name = "showFriendsAndRequestServlet", urlPatterns = "/showFriends")
public class ShowFriendsAndRequestsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        User sessionUser = (User) session.getAttribute("userObject");
        FriendsService friendsService = new FriendsService();

        try {
            List<User> friendList = friendsService.getFriendsByUserId(sessionUser.getId());
            req.setAttribute("successGettingFriendList", true);
            req.setAttribute("friendList", friendList);
            req.setAttribute("emptyFriendList", false);

        } catch (InvalidDataException | NoUserException | GettingFriendsException e) {
            e.printStackTrace();
            req.setAttribute("successGettingFriendList", false);
            req.setAttribute("emptyFriendList", false);
        } catch (GotNoFriendsException e) {
            e.printStackTrace();
            req.setAttribute("emptyFriendList", true);
            req.setAttribute("successGettingFriendList", true);
        }

        try {
            List<User> requestsToUser = friendsService.getUserFriendRequestsById(
                    sessionUser.getId(),
                    FriendsRequests.REQUESTS_TO_USER
            );
            req.setAttribute("successGettingRequestsToList", true);
            req.setAttribute("requestsToUserList", requestsToUser);
            req.setAttribute("emptyRequestsToList", false);

        } catch (InvalidDataException | NoUserException | GettingFriendRequestsListException e) {
            e.printStackTrace();
            req.setAttribute("successGettingRequestsToList", false);
            req.setAttribute("emptyRequestsToList", false);
        } catch (NoUserFriendRequestsException e) {
            e.printStackTrace();
            req.setAttribute("emptyRequestsToList", true);
            req.setAttribute("successGettingRequestsToList", true);
        }

        try {
            List<User> requestsFromUser = friendsService.getUserFriendRequestsById(
                    sessionUser.getId(),
                    FriendsRequests.REQUESTS_FROM_USER
            );
            req.setAttribute("successGettingRequestsFromList", true);
            req.setAttribute("requestsFromUserList", requestsFromUser);
            req.setAttribute("emptyRequestsFromList", false);


        } catch (InvalidDataException | NoUserException | GettingFriendRequestsListException e) {
            e.printStackTrace();
            req.setAttribute("successGettingRequestsFromList", false);
            req.setAttribute("emptyRequestsFromList", false);
        } catch (NoUserFriendRequestsException e) {
            e.printStackTrace();
            req.setAttribute("emptyRequestsFromList", true);
            req.setAttribute("successGettingRequestsFromList", true);
        }

        req.getRequestDispatcher("/FriendList.jsp").forward(req, resp);
        //TODO: если в адресную строку вставлять то 405
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
