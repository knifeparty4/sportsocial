package servlets.friendship;

import entities.User;
import service.FriendsService;
import service.UserService;
import util.enums.FriendsRequests;
import util.exceptions.InvalidDataException;
import util.exceptions.friends.GettingFriendRequestsListException;
import util.exceptions.friends.GettingFriendsException;
import util.exceptions.friends.GotNoFriendsException;
import util.exceptions.users.NoUserException;
import util.exceptions.users.NoUserFriendRequestsException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Created by yuriy on 22.08.17.
 */
@WebServlet(name = "showOtherUsersFriends", urlPatterns = "/showUserFriends")
public class ShowOtherUserFriendsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*Integer userFromPageId = -1;
        try {
            userFromPageId = Integer.parseInt(req.getParameter("id"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        UserService userService = new UserService();
        FriendsService friendsService = new FriendsService();*/

        Integer userFromPageId = -1;
        UserService userService = new UserService();
        try {
            userFromPageId = Integer.parseInt(req.getParameter("id"));
            FriendsService friendsService = new FriendsService();
            User otherUser = userService.getUserById(userFromPageId);
            List<User> friendList = friendsService.getFriendsByUserId(otherUser.getId());
            req.setAttribute("userFriendsToShow", otherUser);
            req.setAttribute("successGettingFriendList", true);
            req.setAttribute("friendList", friendList);
            req.setAttribute("emptyFriendList", false);

        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        } catch (InvalidDataException | NoUserException | GettingFriendsException e) {
            e.printStackTrace();
            req.setAttribute("successGettingFriendList", false);
            req.setAttribute("emptyFriendList", false);
        } catch (GotNoFriendsException e) {
            e.printStackTrace();
            req.setAttribute("emptyFriendList", true);
            req.setAttribute("successGettingFriendList", true);
        }

        req.getRequestDispatcher("/OtherUserFriendList.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
