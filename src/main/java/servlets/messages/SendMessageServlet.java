package servlets.messages;

import entities.Message;
import service.MessageService;
import util.exceptions.InvalidDataException;
import util.exceptions.messages.CreationMessageException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

/**
 * Created by yuriy on 04.09.17.
 */
@WebServlet(name = "sendMessageServlet", urlPatterns = "/sendMessage")
public class SendMessageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        MessageService messageService = new MessageService();

        String value = req.getParameter("messageValue");
        Integer receiverId = -1;
        try {
            Integer senderId = Integer.parseInt(req.getParameter("senderId"));
            receiverId = Integer.parseInt(req.getParameter("receiverId"));
            messageService.createMessage(new Message(senderId, receiverId, value, new Date()));
            session.setAttribute("successMessageSent", true);
        } catch (InvalidDataException | CreationMessageException | NumberFormatException e) {
            e.printStackTrace();
            session.setAttribute("successMessageSent", false);
        }

        resp.sendRedirect("/otherUser?id=" + receiverId);

        //TODO: не работает!!!!! (исправлено фильтром)
        /*session.setAttribute("successMessageSent", null);*/




    }
}
