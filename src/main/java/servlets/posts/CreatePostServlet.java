package servlets.posts;

import entities.Post;
import entities.User;
import service.PostsService;
import util.exceptions.InvalidDataException;
import util.exceptions.posts.CreationPostException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

/**
 * Created by yuriy on 23.08.17.
 */
@WebServlet(name = "createPostServlet", urlPatterns = "/createPost")
public class CreatePostServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        /*Integer userId = -1;
        try {
            userId = Integer.parseInt(req.getParameter("userFromId"));
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        HttpSession session = req.getSession();
        User sessionUser = (User)(session.getAttribute("userObject"));
        PostsService postsService = new PostsService();

        int authorId = sessionUser.getId();
        int receiverId = userId;
        Date creationDate = new Date();
        String postValue = req.getParameter("value");

        try {
            postsService.createPost(new Post(authorId, receiverId, postValue, creationDate));
            req.setAttribute("postCreationSuccess", true);
        } catch (CreationPostException | InvalidDataException e) {
            e.printStackTrace();
            req.setAttribute("postCreationSuccess", false);

        }
        req.getRequestDispatcher("/otherUser?id=" + userId).forward(req, resp);*/
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        User sessionUser = (User)(session.getAttribute("userObject"));
        PostsService postsService = new PostsService();

        int authorId = sessionUser.getId();
        Date creationDate = new Date();
        String postValue = req.getParameter("value");
        int receiverId = -1;
        try {
            receiverId = Integer.parseInt(req.getParameter("receiverId"));
            postsService.createPost(new Post(authorId, receiverId, postValue, creationDate));
            session.setAttribute("postCreationSuccess", true);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        } catch (CreationPostException | InvalidDataException e) {
            e.printStackTrace();
            session.setAttribute("postCreationSuccess", false);
        }

        if (authorId == receiverId)
            resp.sendRedirect("/myProfile");
        else resp.sendRedirect("/otherUser?id=" + receiverId);

        //TODO:проверить: работает ли это вообще?
        session.setAttribute("postCreationSuccess", null);
        /*/
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        User sessionUser = (User)(session.getAttribute("userObject"));
        PostsService postsService = new PostsService();

        int receiverId = -1;
        if (req.getParameter("userFromId") != null) {
            Integer userId = -1;
            try {
                userId = Integer.parseInt(req.getParameter("userFromId"));
            } catch (NumberFormatException e) {
                e.printStackTrace();
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            receiverId = userId;
        }
        else receiverId = sessionUser.getId();

        int authorId = sessionUser.getId();
//      int receiverId = sessionUser.getId();
        Date creationDate = new Date();
        String postValue = req.getParameter("value");

        try {
            postsService.createPost(new Post(authorId, receiverId, postValue, creationDate));
            req.setAttribute("postCreationSuccess", true);
        } catch (CreationPostException | InvalidDataException e) {
            e.printStackTrace();
            req.setAttribute("postCreationSuccess", false);
        }

        if (authorId == receiverId)
            req.getRequestDispatcher("/myProfile").forward(req ,resp);

        else resp.sendRedirect("/otherUser?id=" + receiverId);*/


    }
}
