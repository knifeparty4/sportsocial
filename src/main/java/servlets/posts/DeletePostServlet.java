package servlets.posts;

import entities.Post;
import entities.User;
import service.PostsService;
import util.exceptions.InvalidDataException;
import util.exceptions.posts.DeletingPostException;
import util.exceptions.posts.GotNoPostException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by yuriy on 24.08.17.
 */
@WebServlet(name = "deletePostServlet", urlPatterns = "/deletePost")
public class DeletePostServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        User sessionUser = (User)(session.getAttribute("userObject"));
        PostsService postsService = new PostsService();

        int postId = -1;
        Integer receiverId = -1;

        try {
            receiverId = Integer.parseInt(req.getParameter("receiverId"));
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        try {
            postId = Integer.parseInt(req.getParameter("postId"));
            postsService.deletePost(postId);
            req.setAttribute("deletePostSuccess", true);
            //TODO: бросать не 404 а просто страницу(ниже)
        } catch (DeletingPostException | InvalidDataException | GotNoPostException | NumberFormatException e) {
            e.printStackTrace();
            req.setAttribute("deletePostSuccess", false);
        }

        if (receiverId.equals(sessionUser.getId()))
            req.getRequestDispatcher("/myProfile").forward(req, resp);
        else req.getRequestDispatcher("/otherUser?id=" + receiverId).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*Integer postId = -1;
        try {
            postId = Integer.parseInt(req.getParameter("postId"));
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            //TODO: очень сомнительно что нужно слать 404 (можно просто на страницу с ошибкой в модале)
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        PostsService postsService = new PostsService();
        try {
            postsService.deletePost(postId);
            req.setAttribute("deletePostSuccess", true);
        } catch (DeletingPostException | InvalidDataException | GotNoPostException e) {
            e.printStackTrace();
            req.setAttribute("deletePostSuccess", false);

        }
        *//*System.out.println(req.getContextPath());
        System.out.println(req.getPathInfo());
        System.out.println(req.getServletPath());*//*

        req.getRequestDispatcher("/myProfile").forward(req, resp);*/
    }
}
