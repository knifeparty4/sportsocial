package servlets.posts;

import entities.User;
import service.PostsService;
import util.exceptions.InvalidDataException;
import util.exceptions.posts.EditingPostException;
import util.exceptions.posts.GotNoPostException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by yuriy on 24.08.17.
 */
@WebServlet(name = "editPostServlet", urlPatterns = "/editPost")
public class EditPostServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*Integer postId = -1;
        String value = req.getParameter("value");
        try {
            postId = Integer.parseInt(req.getParameter("postId"));
        }
        catch (NumberFormatException e) {
            e.printStackTrace();

            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        PostsService postsService = new PostsService();*/
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        int receiverId = -1;
        User sessionUser = (User)(session.getAttribute("userObject"));

        try {
            //TODO: нужна проверочка, что пользователь в сессии имеет право на удаление
            int postId = -1;
            String value = req.getParameter("editedValue");
            postId = Integer.parseInt(req.getParameter("postId"));
            receiverId = Integer.parseInt(req.getParameter("receiverId"));
            PostsService postsService = new PostsService();
            postsService.editPost(value, postId);
            session.setAttribute("editingPostSuccess", true);
            session.setAttribute("noPost", false);

        } catch (InvalidDataException | EditingPostException | NumberFormatException e) {
            e.printStackTrace();
            session.setAttribute("editingPostSuccess", false);
            session.setAttribute("noPost", false);
        } catch (GotNoPostException e) {
            e.printStackTrace();
            session.setAttribute("noPost", true);
            session.setAttribute("editingPostSuccess", false);
        }
        if (receiverId == sessionUser.getId())
            resp.sendRedirect("/myProfile");
        else resp.sendRedirect("/otherUser?id=" + receiverId);

        //TODO:!!!!!!
        session.setAttribute("noPost", null);
        session.setAttribute("editingPostSuccess", null);
        /*
        req.getRequestDispatcher("/otherUser?id=" + req.getParameter("userId")).forward(req, resp);*/

        /*HttpSession session = req.getSession();
        try {
            Integer postId = -1;
            String value = req.getParameter("editedValue");
            postId = Integer.parseInt(req.getParameter("postId"));
            PostsService postsService = new PostsService();
            postsService.editPost(value, postId);
            session.setAttribute("editingPostSuccess", true);
            session.setAttribute("noPost", false);

        } catch (InvalidDataException | EditingPostException | NumberFormatException e) {
            e.printStackTrace();
            session.setAttribute("editingPostSuccess", false);
            session.setAttribute("noPost", false);
        } catch (GotNoPostException e) {
            e.printStackTrace();
            session.setAttribute("noPost", true);
            session.setAttribute("editingPostSuccess", false);
        }*/
       /* req.getRequestDispatcher("/myPosts").forward(req, resp);*/
    }
}
