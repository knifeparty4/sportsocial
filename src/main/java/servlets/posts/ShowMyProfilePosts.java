package servlets.posts;

import entities.Post;
import entities.User;
import service.PostsService;
import service.UserService;
import util.exceptions.InvalidDataException;
import util.exceptions.posts.GettingPostException;
import util.exceptions.posts.NoPostsException;
import util.exceptions.users.NoUserException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Created by yuriy on 23.08.17.
 */
@WebServlet(name = "showMyProfilePostsServlet", urlPatterns = "/myPosts")
public class ShowMyProfilePosts extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        User sessionUser = (User)(session.getAttribute("userObject"));
        PostsService postsService = new PostsService();

        try {
            session.setAttribute("gettingUsersFromPostsWithErr", false);
            List<Post> postList = postsService.getUserPosts(sessionUser.getId());
            System.out.println(Arrays.toString(postList.toArray()));
            Map<Post, User> postMap = parsePosts(postList, req);
            session.setAttribute("postMap", postMap);
            session.setAttribute("gotNoPosts", false);
            session.setAttribute("gettingPostsSuccess", true);
        } catch (NoPostsException e) {
            e.printStackTrace();
            session.setAttribute("gotNoPosts", true);
            session.setAttribute("gettingPostsSuccess", true);
        } catch (GettingPostException | InvalidDataException e) {
            e.printStackTrace();
            session.setAttribute("gotNoPosts", false);
            session.setAttribute("gettingPostsSuccess", false);
        }
        resp.sendRedirect("/myProfile");
    }

    //TODO: сделать через запрос
    private Map<Post, User> parsePosts(List<Post> postList, HttpServletRequest req) {
        UserService userService = new UserService();
        Map<Post, User> postMap = new LinkedHashMap<>();
        for (Post post: postList) {
            try {
                User authorUser = userService.getUserById(post.getAuthorId());
                postMap.put(post, authorUser);
            } catch (InvalidDataException | NoUserException e) {
                e.printStackTrace();
                req.setAttribute("gettingUsersFromPostsWithErr", true);
            }
        }
        return postMap;
    }
}
