package servlets.user;

import entities.User;
import service.UserService;
import util.exceptions.InvalidDataException;
import util.exceptions.users.UserUpdateException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by yuriy on 15.08.17.
 */
@WebServlet(name = "editUserProfileServlet", urlPatterns = "/editUserProfile")
@MultipartConfig
public class EditUserProfileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        Part filePart = req.getPart("inputPic"); // Retrieves <input type="file" name="file">
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        InputStream fileContent = filePart.getInputStream();
        BufferedImage image = ImageIO.read(fileContent);
        /*String phone = req.getParameter("inputPhone");*/
        /*ImageIO.write(
                image,
                "png",
                new File("/home/yuriy/webImages/" + phone + ".png")
        );*/

        //TODO: строковые константы
        User userToUpdate = new User(
                req.getParameter("inputEmail"),
                req.getParameter("inputPassword"),
                req.getParameter("inputFirstName"),
                req.getParameter("inputSecondName"),
                req.getParameter("inputDate"),
                req.getParameter("countrySelected"),
                req.getParameter("inputPhone"),
                req.getParameter("optionsGender"),
                req.getParameter("inputAbout"),
                req.getParameter("inputSports"),
                req.getParameter("inputWeight"),
                req.getParameter("inputHeight"),
                fileName
        );

        HttpSession session = req.getSession();
        User sessionUser = (User)session.getAttribute("userObject");

        try {
            User updatedUser = new UserService().updateUser(userToUpdate, sessionUser.getId(), image);
            session.setAttribute("userObject", updatedUser);
            req.setAttribute("updateSuccess", true);
        } catch (InvalidDataException | UserUpdateException e) {
            req.setAttribute("updateSuccess", false);
            e.printStackTrace();
        }
        req.getRequestDispatcher("/EditUserProfile.jsp").forward(req, resp);
    }
}
