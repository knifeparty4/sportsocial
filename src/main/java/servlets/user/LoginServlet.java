package servlets.user;

import entities.User;
import service.UserService;
import util.exceptions.InvalidDataException;
import util.exceptions.users.NoUserException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by yuriy on 12.08.17.
 */
@WebServlet(name = "loginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String email = req.getParameter("inputEmail");
        String password = req.getParameter("inputPassword");
        HttpSession session = req.getSession();

        try {
            User user = new UserService().getUserByEmail(email);

            if (password.equals(user.getPassword())) {
                session.setAttribute("userObject", user);
                session.setAttribute("loginSuccess", true);
                /*resp.sendRedirect("/MyProfile.jsp");*/
                /*req.getRequestDispatcher("/myProfile").forward(req, resp);*/
                resp.sendRedirect("/myProfile");
            }
            else {
                throw new InvalidDataException();
            }
        }
        catch (InvalidDataException | NoUserException e) {
            session.setAttribute("loginSuccess", false);
            e.printStackTrace();
            req.getRequestDispatcher("/Main.jsp").forward(req, resp);
        }

    }
}
