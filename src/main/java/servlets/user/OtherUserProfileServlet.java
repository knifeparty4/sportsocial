package servlets.user;

import entities.Message;
import entities.Post;
import entities.User;
import service.FriendsService;
import service.MessageService;
import service.PostsService;
import service.UserService;
import util.enums.FriendshipStatus;
import util.exceptions.InvalidDataException;
import util.exceptions.friends.GettingFriendshipStatusException;
import util.exceptions.messages.GettingMessagesException;
import util.exceptions.messages.GotNoMessagesException;
import util.exceptions.posts.GettingPostException;
import util.exceptions.posts.NoPostsException;
import util.exceptions.users.NoUserException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Created by yuriy on 18.08.17.
 */
@WebServlet(name = "otherUserProfileServlet", urlPatterns = "/otherUser")
public class OtherUserProfileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //TODO: разночить по методам (общее, посты, сообщения)
        HttpSession session = req.getSession();
        User userToShow = null;
        User sessionUser = ((User) session.getAttribute("userObject"));

        //дружба и сам юзер
        try {
            Integer userId = Integer.parseInt(req.getParameter("id"));
            userToShow = new UserService().getUserById(userId);
            req.setAttribute("userToShow", userToShow);
            FriendshipStatus status = new FriendsService()
                    .getUserFriendshipStatus(sessionUser.getId(), userToShow.getId());
            req.setAttribute("friendshipStatus", status);

        } catch (InvalidDataException | NoUserException | NumberFormatException e) {
            e.printStackTrace();
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        } catch (GettingFriendshipStatusException e) {
            e.printStackTrace();
            req.setAttribute("friendshipStatus", null);
        }

        //посты
        try {
            PostsService postsService = new PostsService();
            List<Post> list = postsService.getUserPosts(userToShow.getId());
            Map<Post, User> postMap = parsePosts(list, req);
            req.setAttribute("postMap", postMap);
            req.setAttribute("gotNoPosts", false);
            req.setAttribute("gettingPostsSuccess", true);
        }
        catch (GettingPostException | InvalidDataException e) {
            e.printStackTrace();
            req.setAttribute("gotNoPosts", false);
            req.setAttribute("gettingPostsSuccess", false);
        } catch (NoPostsException e) {
            e.printStackTrace();
            req.setAttribute("gotNoPosts", true);
            req.setAttribute("gettingPostsSuccess", true);
        }

        //сообщения
        MessageService messageService = new MessageService();
        try {
            List<Message> msgList = messageService.getUserMessages(sessionUser.getId(), userToShow.getId());
            req.setAttribute("successMessages", true);
            req.setAttribute("noMessages", false);
            req.setAttribute("messages", msgList);
        } catch (InvalidDataException | GettingMessagesException e) {
            e.printStackTrace();
            req.setAttribute("successMessages", false);
            req.setAttribute("noMessages", false);
        } catch (GotNoMessagesException e) {
            e.printStackTrace();
            req.setAttribute("successMessages", true);
            req.setAttribute("noMessages", true);
        }


        req.getRequestDispatcher("/OtherUserProfile.jsp").forward(req, resp);
    }

    //TODO: если сам себя нашео в поиске
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    //TODO: вынести в другое мест??
    private Map<Post, User> parsePosts(List<Post> postList, HttpServletRequest req) {
        UserService userService = new UserService();
        Map<Post, User> postMap = new LinkedHashMap<>();
        for (Post post: postList) {
            try {
                User authorUser = userService.getUserById(post.getAuthorId());
                postMap.put(post, authorUser);
            } catch (InvalidDataException | NoUserException e) {
                e.printStackTrace();
                req.setAttribute("gettingUsersFromPostsWithErr", true);
            }
        }
        return postMap;
    }
}
