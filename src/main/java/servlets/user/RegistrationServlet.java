package servlets.user;

import entities.User;
import service.UserService;
import util.exceptions.InvalidDataException;
import util.exceptions.users.UserCreateException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

/**
 * Created by yuriy on 14.08.17.
 */
@WebServlet(name = "registrationServlet", urlPatterns = "/registration")
@MultipartConfig
public class RegistrationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        Part filePart = req.getPart("inputPic"); // Retrieves <input type="file" name="file">
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        InputStream fileContent = filePart.getInputStream();
        BufferedImage image = ImageIO.read(fileContent);
        /*String phone = req.getParameter("inputPhone");
        ImageIO.write(
                image,
                "png",
                new File("/home/yuriy/IdeaProjects/sportsocial/src/main/webapp/resources/pics/" + phone + ".png")
        );*/

        User requestedUser = new User(
                req.getParameter("inputEmail"),
                req.getParameter("inputPassword"),
                req.getParameter("inputFirstName"),
                req.getParameter("inputSecondName"),
                req.getParameter("inputDate"),
                req.getParameter("countrySelected"),
                req.getParameter("inputPhone"),
                req.getParameter("optionsGender"),
                req.getParameter("inputAbout"),
                req.getParameter("inputSports"),
                req.getParameter("inputWeight"),
                req.getParameter("inputHeight"),
                "default.png"
        );
        HttpSession session = req.getSession();

        //TODO: при неудачной реге форвардить, а внутри заполнять значениями полей (отправленных при неудачной реге)
        try {
            User newUser = new UserService().createUser(requestedUser, image);
            session.setAttribute("userObject", newUser);
            session.setAttribute("registrationSuccess", true);
            resp.sendRedirect("/myProfile");
            /*req.getRequestDispatcher("/EditUserProfile.jsp").forward(req, resp);*/
        } catch (InvalidDataException | UserCreateException e) {
            e.printStackTrace();
            session.setAttribute("registrationSuccess", false);
            req.getRequestDispatcher("/Registration.jsp").forward(req, resp);
        }

        /*try {
            User newUser = new UserService().createUser(requestedUser);
            if (newUser != null) {
                session.setAttribute("userObject", newUser);
                session.setAttribute("registrationSuccess", true);
                req.getRequestDispatcher("/Profile.jsp").forward(req, resp);
            }
            else {
                throw new InvalidDataException();
            }
        } catch (InvalidDataException e) {
            e.printStackTrace();
            session.setAttribute("registrationSuccess", false);
            req.getRequestDispatcher("/Registration.jsp").forward(req, resp);
        } catch (NoUserException noUserExeption) {
            noUserExeption.printStackTrace();
        } catch (UserCreateException e) {
            e.printStackTrace();
        }*/

    }
}
