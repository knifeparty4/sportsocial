package servlets.user;

import entities.User;
import service.UserService;
import util.dto.Name;
import util.exceptions.InvalidDataException;
import util.exceptions.users.NoUserException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by yuriy on 17.08.17.
 */
@WebServlet(name = "searchUsersServlet", urlPatterns = "/searchUsers")
public class SearchUsersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstPar = req.getParameter("inputFirstName");
        String secondPar = req.getParameter("inputSecondName");
        String firstName = (!firstPar.isEmpty()) ? firstPar : "Noname";
        String secondName = (!secondPar.isEmpty()) ? secondPar : "Noname";

        Name fullName = new Name(firstName, secondName);
        /*HttpSession session = req.getSession();*/
        try {
            List<User> userList = new UserService().getUsersByName(fullName);
            req.setAttribute("userList", userList);
            req.setAttribute("gotUsersSuccess", true);
        } catch (InvalidDataException e) {
            e.printStackTrace();
            req.setAttribute("gotUsersSuccess", false);
            req.setAttribute("invalidParsToCompleteSearch", true);
        } catch (NoUserException e) {
            e.printStackTrace();
            req.setAttribute("gotUsersSuccess", false);
        }
        finally {
            req.getRequestDispatcher("/SearchUsers.jsp").forward(req, resp);

            /*//
            session.setAttribute("gotUsersSuccess", null);
            session.setAttribute("invalidParsToCompleteSearch", null);
            session.setAttribute("gotUsersSuccess", null);*/
        }
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
