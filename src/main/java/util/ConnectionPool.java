package util;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by yuriy on 05.09.17.
 */
public class ConnectionPool {
    private static BlockingQueue<Connection> pool = new ArrayBlockingQueue<>(10);
    private static ConnectionPool poolObject = null;

    private ConnectionPool() {
        try {
            DriverManager.registerDriver((Driver) Class.forName("com.mysql.jdbc.Driver").newInstance());
            for (int i = 0; i < 10; i++) {
                pool.add(getMysqlConnection());
            }
        } catch (IllegalAccessException | InstantiationException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    //TODO: повыносить, доделать
    public static synchronized ConnectionPool getInstance() {
        if (poolObject == null) {
            poolObject = new ConnectionPool();
        }
        return poolObject;
    }

    private Connection getMysqlConnection()
            throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
        StringBuilder url = new StringBuilder();
        url.
                append("jdbc:mysql://").        //db type
                append("localhost:").           //host name
                append("3306/").                //port
                append("sports_db?").          //db name
                append("useUnicode=true&").
                append("characterEncoding=utf-8&").
                append("user=root&").          //login
                append("password=ololosha4");       //password
        return DriverManager.getConnection(url.toString());
    }

    public Connection getConnection() {
        try {
            return pool.poll(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void putConnection(Connection con) {
        pool.add(con);
    }

}
