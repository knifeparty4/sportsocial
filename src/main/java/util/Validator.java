package util;

import entities.Message;
import entities.User;

import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by yuriy on 12.08.17.
 */
public class Validator {

    /*public static void main(String[] args) {
        System.out.println(Validator.validMessage(new Message(
                1, 2, "dgsg", new Date(), "true", "true", "true"
                )));
    }*/

//TODO: еще страна и дата рождения и пол!!

    public static boolean validId(Integer id) {
        return (id != null && id > 0);
    }
    public static boolean validDate(Date date) {
        return (date != null);
    }

    public static boolean validEmail(String email) {
        if (email == null)
            return false;
        Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        return pattern.matcher(email).find();
    }

    public static boolean validNames(String...names) {
        Pattern pattern = Pattern.compile("^[a-zа-яА-Я ,.'-]+$", Pattern.CASE_INSENSITIVE);
        for (String name : names) {
            if (name == null || !pattern.matcher(name).find())
                return false;
        }
        return true;
    }

    public static boolean validPhone(String phone_num) {
        if (phone_num == null)
            return false;
        Pattern pattern = Pattern.compile("^\\+?[0-9]{3}-?[0-9]{6,12}$", Pattern.CASE_INSENSITIVE);
        return pattern.matcher(phone_num).find();
    }

    public static boolean validPassword(String password) {
        if (password == null)
            return false;
        Pattern pattern = Pattern.compile("^.{8,}", Pattern.CASE_INSENSITIVE);
        return pattern.matcher(password).find();
    }

    public static boolean validUser(User user) {
        //TODO: подумать над этим
        /*if (user.getId() == null || user.getId() <= 0)
            return false;*/
        if (!Validator.validEmail(user.getEmail()))
            return false;
        if (!Validator.validPassword(user.getPassword()))
            return false;
        if (!Validator.validNames(user.getFirst_name(), user.getSecond_name()))
            return false;
        if (!Validator.validPhone(user.getPhone_num()))
            return false;
        if (!Validator.validStringValues(user.getBirth_date(), user.getCountry()))
            return false;

        return true;
    }

    public static boolean validStringValues(String...args) {
        for (String s : args) {
            if (s == null || s.isEmpty())
                return false;
        }
        return true;
    }

    /*public static boolean validNullStringValues(String...args) {
        for (String s : args) {
            if (s == null)
                return false;
        }
        return true;
    }*/

    public static boolean validHeightWeight(String value) {
        try {
            Integer.parseInt(value);
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public static boolean validBoolString(String...values) {
        for (String value: values) {
            if (!value.equals("true") && !value.equals("false"))
                return false;
        }
        return true;
    }

    //TODO: такой же для поста и комментария!!!!
    public static boolean validMessage(Message msg) {
        return (Validator.validId(msg.getReceiverId()) &&
                Validator.validId(msg.getSenderId()) &&
                Validator.validStringValues(msg.getValue()) &&
                Validator.validDate(msg.getSentDate())); /*&&*/
        //TODO: !!!!!!
                /*Validator.validBoolString(msg.getDeletedSender(), msg.getDeletedReceiver(), msg.getChecked())*/
    }

    public static boolean validFriendsId(Integer user1Id, Integer user2Id) {
        return (Validator.validId(user1Id) && Validator.validId(user2Id) && !user1Id.equals(user2Id));

    }
}
