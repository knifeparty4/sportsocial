package util.dto;

/**
 * Created by yuriy on 20.08.17.
 */
public class FriendshipRelation {
    private Integer userId1;
    private Integer userId2;
    private String accept;

    public FriendshipRelation(Integer userId1, Integer userId2, String accept) {
        this.userId1 = userId1;
        this.userId2 = userId2;
        this.accept = accept;
    }

    public FriendshipRelation() {
    }

    public Integer getUserId1() {
        return userId1;
    }

    public Integer getUserId2() {
        return userId2;
    }

    public String getAccept() {
        return accept;
    }

    public boolean isEmpty() {
        return (userId1 == null && userId2 == null && accept == null);
    }
}
