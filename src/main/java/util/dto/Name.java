package util.dto;

/**
 * Created by yuriy on 17.08.17.
 */
public class Name {
    private String first_name;
    private String second_name;

    public Name(String first_name, String second_name) {
        this.first_name = first_name;
        this.second_name = second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSecond_name() {
        return second_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Name name = (Name) o;

        if (first_name != null ? !first_name.equals(name.first_name) : name.first_name != null) return false;
        return second_name != null ? second_name.equals(name.second_name) : name.second_name == null;
    }

    @Override
    public int hashCode() {
        int result = first_name != null ? first_name.hashCode() : 0;
        result = 31 * result + (second_name != null ? second_name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Name{" +
                "first_name='" + first_name + '\'' +
                ", second_name='" + second_name + '\'' +
                '}';
    }
}
