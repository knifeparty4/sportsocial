package util.dto;

/**
 * Created by yuriy on 23.08.17.
 */
public class UpdateResult {
    private Integer rowsAffected;
    private Long generatedKey;

    public UpdateResult(Integer rowsAffected, Long generatedKey) {
        this.rowsAffected = rowsAffected;
        this.generatedKey = generatedKey;
    }

    public UpdateResult() {
    }

    public Integer getRowsAffected() {
        return rowsAffected;
    }

    public void setRowsAffected(Integer rowsAffected) {
        this.rowsAffected = rowsAffected;
    }

    public Long getGeneratedKey() {
        return generatedKey;
    }

    public void setGeneratedKey(Long generatedKey) {
        this.generatedKey = generatedKey;
    }
}
