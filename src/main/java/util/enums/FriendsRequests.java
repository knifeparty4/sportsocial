package util.enums;

/**
 * Created by yuriy on 19.08.17.
 */
public enum FriendsRequests {
    REQUESTS_TO_USER,
    REQUESTS_FROM_USER
}
