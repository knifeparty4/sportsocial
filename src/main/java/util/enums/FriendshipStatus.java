package util.enums;

/**
 * Created by yuriy on 19.08.17.
 */
public enum FriendshipStatus {
    FRIEND,
    NOT_FRIEND,
    REQUESTED_TO_USER,
    REQUETED_BY_USER
}
