package util.exceptions;

/**
 * Created by yuriy on 12.08.17.
 */
public class InvalidDataException extends Exception {
    public InvalidDataException() {
    }

    public InvalidDataException(String message) {
        super(message);
    }
}
