package util.exceptions.comments;

/**
 * Created by yuriy on 28.08.17.
 */
public class CreationCommentException extends Exception {
    public CreationCommentException() {
        super();
    }

    public CreationCommentException(String message) {
        super(message);
    }
}
