package util.exceptions.comments;

/**
 * Created by yuriy on 28.08.17.
 */
public class DeletingCommentException extends Exception {
    public DeletingCommentException() {
        super();
    }

    public DeletingCommentException(String message) {
        super(message);
    }
}
