package util.exceptions.comments;

/**
 * Created by yuriy on 30.08.17.
 */
public class EditingCommentException extends Exception {
    public EditingCommentException() {
        super();
    }

    public EditingCommentException(String message) {
        super(message);
    }
}
