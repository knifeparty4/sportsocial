package util.exceptions.comments;

/**
 * Created by yuriy on 28.08.17.
 */
public class GettingCommentsException extends Exception {
    public GettingCommentsException() {
        super();
    }

    public GettingCommentsException(String message) {
        super(message);
    }
}
