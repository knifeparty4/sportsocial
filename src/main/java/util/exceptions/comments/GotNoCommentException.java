package util.exceptions.comments;

/**
 * Created by yuriy on 28.08.17.
 */
public class GotNoCommentException extends Exception {
    public GotNoCommentException() {
        super();
    }

    public GotNoCommentException(String message) {
        super(message);
    }
}
