package util.exceptions.friends;

/**
 * Created by yuriy on 20.08.17.
 */
public class DuplicateFriendsPairException extends Exception {
    public DuplicateFriendsPairException() {
        super();
    }

    public DuplicateFriendsPairException(String message) {
        super(message);
    }
}
