package util.exceptions.friends;

/**
 * Created by yuriy on 20.08.17.
 */
public class FriendAcceptException extends Exception {
    public FriendAcceptException() {
        super();
    }

    public FriendAcceptException(String message) {
        super(message);
    }
}
