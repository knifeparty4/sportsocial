package util.exceptions.friends;

/**
 * Created by yuriy on 19.08.17.
 */
public class FriendRequestException extends Exception {
    public FriendRequestException() {
        super();
    }

    public FriendRequestException(String message) {
        super(message);
    }
}
