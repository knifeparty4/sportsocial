package util.exceptions.friends;

/**
 * Created by yuriy on 20.08.17.
 */
public class FriendshipDeleteException extends Exception {
    public FriendshipDeleteException() {
        super();
    }

    public FriendshipDeleteException(String message) {
        super(message);
    }
}
