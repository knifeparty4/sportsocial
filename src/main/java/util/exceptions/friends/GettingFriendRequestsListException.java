package util.exceptions.friends;

/**
 * Created by yuriy on 19.08.17.
 */
public class GettingFriendRequestsListException extends Exception {
    public GettingFriendRequestsListException() {
        super();
    }

    public GettingFriendRequestsListException(String message) {
        super(message);
    }
}
