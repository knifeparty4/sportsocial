package util.exceptions.friends;

/**
 * Created by yuriy on 19.08.17.
 */
public class GettingFriendsException extends Exception {
    public GettingFriendsException() {
        super();
    }

    public GettingFriendsException(String message) {
        super(message);
    }
}
