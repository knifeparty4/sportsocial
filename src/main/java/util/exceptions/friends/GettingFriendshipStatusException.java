package util.exceptions.friends;

/**
 * Created by yuriy on 20.08.17.
 */
public class GettingFriendshipStatusException extends Exception {
    public GettingFriendshipStatusException() {
        super();
    }

    public GettingFriendshipStatusException(String message) {
        super(message);
    }
}
