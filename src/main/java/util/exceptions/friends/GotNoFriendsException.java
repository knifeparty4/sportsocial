package util.exceptions.friends;

/**
 * Created by yuriy on 20.08.17.
 */
public class GotNoFriendsException extends Exception {
    public GotNoFriendsException() {
        super();
    }

    public GotNoFriendsException(String message) {
        super(message);
    }
}
