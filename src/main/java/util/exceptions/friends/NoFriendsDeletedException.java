package util.exceptions.friends;

/**
 * Created by yuriy on 20.08.17.
 */
public class NoFriendsDeletedException extends Exception {
    public NoFriendsDeletedException() {
        super();
    }

    public NoFriendsDeletedException(String message) {
        super(message);
    }
}
