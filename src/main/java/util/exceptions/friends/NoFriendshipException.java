package util.exceptions.friends;

/**
 * Created by yuriy on 20.08.17.
 */
public class NoFriendshipException extends Exception {
    public NoFriendshipException() {
        super();
    }

    public NoFriendshipException(String message) {
        super(message);
    }
}
