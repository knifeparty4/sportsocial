package util.exceptions.messages;

/**
 * Created by yuriy on 02.09.17.
 */
public class CreationMessageException extends Exception {
    public CreationMessageException() {
        super();
    }

    public CreationMessageException(String message) {
        super(message);
    }
}
