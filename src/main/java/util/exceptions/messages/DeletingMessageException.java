package util.exceptions.messages;

/**
 * Created by yuriy on 04.09.17.
 */
public class DeletingMessageException extends Exception {
    public DeletingMessageException() {
        super();
    }

    public DeletingMessageException(String message) {
        super(message);
    }
}
