package util.exceptions.messages;

/**
 * Created by yuriy on 02.09.17.
 */
public class GettingMessagesException extends Exception {
    public GettingMessagesException() {
        super();
    }

    public GettingMessagesException(String message) {
        super(message);
    }
}
