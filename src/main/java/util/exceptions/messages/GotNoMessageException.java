package util.exceptions.messages;

/**
 * Created by yuriy on 02.09.17.
 */
public class GotNoMessageException extends Exception {
    public GotNoMessageException() {
        super();
    }

    public GotNoMessageException(String message) {
        super(message);
    }
}
