package util.exceptions.messages;

/**
 * Created by yuriy on 02.09.17.
 */
public class GotNoMessagesException extends Exception {
    public GotNoMessagesException() {
        super();
    }

    public GotNoMessagesException(String message) {
        super(message);
    }
}
