package util.exceptions.posts;

/**
 * Created by yuriy on 23.08.17.
 */
public class CreationPostException extends Exception {
    public CreationPostException() {
        super();
    }

    public CreationPostException(String message) {
        super(message);
    }
}
