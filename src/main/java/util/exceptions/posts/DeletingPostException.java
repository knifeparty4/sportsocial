package util.exceptions.posts;

/**
 * Created by yuriy on 23.08.17.
 */
public class DeletingPostException extends Exception {
    public DeletingPostException() {
        super();
    }

    public DeletingPostException(String message) {
        super(message);
    }
}
