package util.exceptions.posts;

/**
 * Created by yuriy on 24.08.17.
 */
public class EditingPostException extends Exception {
    public EditingPostException() {
        super();
    }

    public EditingPostException(String message) {
        super(message);
    }
}
