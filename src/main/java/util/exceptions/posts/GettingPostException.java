package util.exceptions.posts;

/**
 * Created by yuriy on 23.08.17.
 */
public class GettingPostException extends Exception {
    public GettingPostException() {
        super();
    }

    public GettingPostException(String message) {
        super(message);
    }
}
