package util.exceptions.posts;

/**
 * Created by yuriy on 23.08.17.
 */
public class GotNoPostException extends Exception {
    public GotNoPostException() {
        super();
    }

    public GotNoPostException(String message) {
        super(message);
    }
}
