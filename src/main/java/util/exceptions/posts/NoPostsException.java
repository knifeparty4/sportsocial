package util.exceptions.posts;

/**
 * Created by yuriy on 23.08.17.
 */
public class NoPostsException extends Exception {
    public NoPostsException() {
        super();
    }

    public NoPostsException(String message) {
        super(message);
    }
}
