package util.exceptions.users;

/**
 * Created by yuriy on 15.08.17.
 */
public class NoUserException extends Exception{
    public NoUserException() {
    }

    public NoUserException(String message) {
        super(message);
    }
}
