package util.exceptions.users;

/**
 * Created by yuriy on 20.08.17.
 */
public class NoUserFriendRequestsException extends Exception {
    public NoUserFriendRequestsException() {
        super();
    }

    public NoUserFriendRequestsException(String message) {
        super(message);
    }
}
