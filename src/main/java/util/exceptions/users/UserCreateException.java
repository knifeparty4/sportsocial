package util.exceptions.users;

/**
 * Created by yuriy on 15.08.17.
 */
public class UserCreateException extends Exception {
    public UserCreateException() {
        super();
    }

    public UserCreateException(String message) {
        super(message);
    }
}
