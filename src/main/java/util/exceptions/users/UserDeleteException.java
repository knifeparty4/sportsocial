package util.exceptions.users;

/**
 * Created by yuriy on 15.08.17.
 */
public class UserDeleteException extends Exception {
    public UserDeleteException() {
        super();
    }

    public UserDeleteException(String message) {
        super(message);
    }
}
