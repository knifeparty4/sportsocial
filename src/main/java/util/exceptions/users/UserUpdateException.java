package util.exceptions.users;

/**
 * Created by yuriy on 16.08.17.
 */
public class UserUpdateException extends Exception {
    public UserUpdateException() {
        super();
    }

    public UserUpdateException(String message) {
        super(message);
    }

}
