<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: yuriy
  Date: 15.08.17
  Time: 19:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="genderMale" value="Мужской" scope="request"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <%
        response.setHeader( "Pragma", "no-cache" );
        response.setHeader( "Cache-Control", "no-cache" );
        response.setDateHeader( "Expires", 0 );
    %>

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Ваш профиль</title>

    <!-- Bootstrap -->
    <%--<link href="resources/css/bootstrap.min.css" rel="stylesheet">--%>
    <link href="resources/css/style.css" rel="stylesheet">
    <link href="resources/css/custom.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a style="font-family: Lobster" class="navbar-brand" href="/myProfile">SportSocial</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/Main.jsp#about">О проекте</a></li>
                <li><a href="/Main.jsp#tellUs">Контакты</a></li>
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>-->
            </ul>
            <!--<form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>-->
            <ul class="nav navbar-nav navbar-right">
                <form class="navbar-form navbar-left" action="/searchUsers" method="get">
                    <div class="form-group" >
                        <input style="width: 80px;" type="text" class="form-control" name="inputFirstName" placeholder="Имя">
                    </div>
                    <div class="form-group">
                        <input style="width: 130px;" type="text" class="form-control" name="inputSecondName" placeholder="Фамилия">
                    </div>
                    <button type="submit" class="btn btn-default">Искать</button>
                </form>


                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                        <%--<img style="background-color: white; border: none" src="/resources/pics/default.png"--%>
                        <%--<img style="object-fit: cover;" src="/resources/pics/${userObject.getPhone_num()}.png"--%>
                            <%--<img style="object-fit: cover;" src="/images/${userObject.getPhone_num()}.png"--%>
                            <img id="img1" style="object-fit: cover;" src="/images/${userObject.getImg()}"
                                 class="media-object img-circle" title="John Doe" alt="John Doe" width="30px" height="30px"
                                 onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                        <%--<img src="/resources/pics/file2.png"
                             class="img-responsive img-circle img-thumbnail" title="John Doe" alt="John Doe" width="30px" height="30px">--%>
                    </span>
                    <span class="user-name">${userObject.getFirst_name()} ${userObject.getSecond_name()}</span>
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/showFriends">Мои Друзья</a></li>
                        <li><a href="/myProfile">Мой Профиль</a></li>
                        <li><a href="#">Мои Сообщения</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/EditUserProfile.jsp">Редактировать профиль</a></li>
                        <li><a href="/logout">Выйти</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container" style="padding-top: 50px">
    <h1 class="page-header" align="center">Ваш профиль <small>Основное</small></h1>

    <%--<div align="center" class="col-md-12 col-sm-8 col-xs-12">
        <div class="text-center">
            <img src="http://static.squarespace.com/static/523ce201e4b0cd883dbb8bbf/t/53cf1da8e4b09630bab0c5c3/1405035267633/profile-icon.png"
                 class="avatar img-circle img-thumbnail" alt="avatar" width="150" height="150"
            >
            <h6 align="center">Измените ваше фото с помощью кнопки ниже</h6>
            <form action="/uploadImage" method="post" enctype="multipart/form-data">
                <input type="file" class="text-center center-block well well-sm" name="file">
                <input type="submit" />
            </form>
        </div>
    </div>--%>

    <c:if test="${updateSuccess != null}">
        <c:if test="${updateSuccess}">
            <div class="alert alert-success alert-dismissable" align="center">
                <a class="panel-close close" data-dismiss="alert">×</a>
                <i class="fa fa-coffee"></i>
                <p>Обновление профиля успешно!</p>
            </div>
        </c:if>
        <c:if test="${!updateSuccess}">
            <div class="alert alert-danger alert-dismissable" align="center">
                <a class="panel-close close" data-dismiss="alert">×</a>
                <i class="fa fa-coffee"></i>
                <p>Во время обновления возникла ошибка! Проверьте правильность ввода полей!</p>
            </div>
        </c:if>

        <%--<div class="alert alert-info alert-dismissable" align="center">
            <a class="panel-close close" data-dismiss="alert">×</a>
            <i class="fa fa-coffee"></i>
            <c:if test="${updateSuccess}">
                <p>Обновление профиля успешно!</p>
            </c:if>
            <c:if test="${!updateSuccess}">
                <p>Во время обновления возникла ошибка!</p>
            </c:if>
        </div>--%>
    </c:if>
    <div align="center" class="col-md-12 col-sm-8 col-xs-12">
        <div class="text-center">
            <img id="blah2" style="object-fit: cover; width: 170px; height: 170px" src="/images/${userObject.getImg()}"
                 class="avatar img-circle img-thumbnail" <%--width="170" height="170"--%>
                 onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
            <h6 align="center">Измените ваше фото с помощью кнопки ниже</h6>
        </div>
    </div>
    <div class="col-md-8 col-sm-6 col-xs-12 col-md-offset-2">

        <form id="regForm" class="form-horizontal" role="form" action="/editUserProfile" method="post" enctype="multipart/form-data">
            <div class="form-group" align="center">
                <div class="col-sm-10 col-md-12" >
                    <input accept="image/png" onchange="showPath(this)" style="display: none" type="file" id="fileInput" name="inputPic" class="text-center center-block well well-sm"><i ></i>
                    <button type="button" class="btn btn-sm" onclick="chooseFile();">Выберите фото</button>
                    <!--<p id="path"></p>-->
                    <!--<img class="col-md-5" style="display: none" id="blah" src="#" />-->
                    <script>
                        function chooseFile() {
                            $("#fileInput").click();
                        }
                        function showPath(input) {
                            /*document.getElementById("path").innerHTML = "Файл загружен"*/
                            if (input.files && input.files[0]) {
                                var reader = new FileReader();
                                reader.onload = function (e) {
                                    $('#blah2')
                                        .attr('src', e.target.result)
                                        .width(170)
                                        .height(170);

                                };
                                reader.readAsDataURL(input.files[0]);
                                document.getElementById("blah").style.display="block"
                            }
                        }
                    </script>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail" class="col-md-3 col-sm-2 control-label">Email</label>
                <div class="col-md-8 col-sm-10">
                    <input type="email" class="form-control" name="inputEmail" id="inputEmail"
                           value="${userObject.getEmail()}" placeholder="Email" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-md-3 col-sm-2 control-label">Пароль</label>
                <div class="col-md-8 col-sm-10" align="left">
                    <input type="password" class="form-control" name="inputPassword" id="inputPassword"
                           value="${userObject.getPassword()}" placeholder="Пароль" required>
                    <span id="helpBlock" class="help-block">Пароль должен быть не мнее 8 символов</span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputFirstName" class="col-md-3 col-sm-2 control-label">Имя</label>
                <div class="col-md-8 col-sm-10">
                    <input type="text" class="form-control" name="inputFirstName" id="inputFirstName"
                           value="${userObject.getFirst_name()}" placeholder="Ваше имя" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputSecondName" class="col-md-3 col-sm-2 control-label">Фамилия</label>
                <div class="col-md-8 col-sm-10">
                    <input type="text" class="form-control" name="inputSecondName" id="inputSecondName"
                           value="${userObject.getSecond_name()}" placeholder="Ваша фамилия" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputDate" class="col-md-3 col-sm-2 control-label">Дата рождения</label>
                <div class="col-md-8 col-sm-10">
                    <input type="date" class="form-control" name="inputDate" id="inputDate"
                           value="${userObject.getBirth_date()}" placeholder="Дата вашего рождения" required>
                </div>
            </div>
            <!--<div class="form-group">
                <label for="inputCountry" class="col-sm-2 control-label">Страна</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="inputCountry" id="inputCountry" placeholder="Страна, где вы находитесь" required>
                </div>
            </div>-->
            <div class="form-group">
                <label for="inputPhone" class="col-md-3 col-sm-2 control-label">Страна</label>
                <div class="col-md-8 col-sm-10">
                    <select class="form-control" name="countrySelected">
                        <option>Россия</option>
                        <option>Беларусь</option>
                        <option>Украина</option>
                        <option>Казахстан</option>
                        <option selected>${userObject.getCountry()}</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPhone" class="col-md-3 col-sm-2 control-label">Номер телефона</label>
                <div class="col-md-8 col-sm-10">
                    <input type="number" class="form-control" name="inputPhone" id="inputPhone"
                           value="${userObject.getPhone_num()}" placeholder="Ваш номер телефона" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 col-sm-2 control-label">Пол</label>
                <div class="col-md-8 col-md-10">
                    <c:if test="${userObject.getGender() eq genderMale}">
                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsGender" id="optionsRadios1" value="Мужской" checked required>
                                Мужской
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsGender" id="optionsRadios2" value="Женский" required>
                                Женский
                            </label>
                        </div>
                    </c:if>
                    <c:if test="${!(userObject.getGender() eq genderMale)}">
                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsGender" id="optionsRadios3" value="Мужской" required>
                                Мужской
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="optionsGender" id="optionsRadios4" value="Женский" checked required>
                                Женский
                            </label>
                        </div>
                    </c:if>
                </div>
            </div>
            <h3>Дополнительные поля (необязательные для заполнения)</h3>
            <div class="form-group" align="center">
                <label for="inputAbout" class="col-sm-2 col-md-3 control-label">О себе</label>
                <div class="col-sm-10 col-md-8">
                    <textarea form="regForm" name="inputAbout" id="inputAbout" class="form-control"
                              rows="3" placeholder="Вкратце расскажите о себе: характер, интересы помимо спортивных">${userObject.getAbout()}</textarea>
                    <%--<input type="text" class="form-control" name="inputAbout" id="inputAbout"
                           placeholder="Вкратце расскажите о себе: характер, интересы помимо спортивных"required>--%>
                </div>
            </div>
            <div class="form-group" align="center">
                <label for="inputSports" class="col-sm-2 col-md-3 control-label">Ваши любимые виды спорта</label>
                <div class="col-sm-10 col-md-8">
                    <input value="${userObject.getSports()}" type="text" class="form-control" name="inputSports" id="inputSports"
                           placeholder="Виды спорта, перечисленные через ','" >
                </div>
            </div>
            <div class="form-group" align="left">
                <label for="inputWeight" class="col-sm-2 col-md-3 control-label">Вес</label>
                <div class="col-sm-10 col-md-8">
                    <input value="${userObject.getWeight()}" type="number" class="form-control" name="inputWeight" id="inputWeight"
                           placeholder="Ваш вес (примерный, если не помните точное значение)" >
                    <span id="helpBlockW" class="help-block">Значение в сантиметрах</span>
                </div>
            </div>
            <div class="form-group" align="left">
                <label for="inputHeight" class="col-sm-2 col-md-3 control-label">Рост</label>
                <div class="col-sm-10 col-md-8">
                    <input value="${userObject.getHeight()}" type="number" class="form-control" name="inputHeight" id="inputHeight"
                           placeholder="Ваш рост (примерный, если не помните точное значение)" >
                    <span id="helpBlockH" class="help-block">Значение в килограммах</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-8">
                    <input class="btn btn-primary" value="Сохранить" type="submit">
                    <span></span>
                    <input class="btn btn-default" value="Отмена" type="reset">
                </div>
            </div>
        </form>
    </div>
</div>
<div style="padding-top: 20px"></div>

<!--<div class="container">
    <h1 class="page-header">Ваш профиль <small>Основное</small></h1>
    <div class="row">
        &lt;!&ndash; left column &ndash;&gt;
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="text-center">
                <img src="http://lorempixel.com/200/200/people/9/" class="avatar img-circle img-thumbnail" alt="avatar">
                <h6>Upload a different photo...</h6>
                <input type="file" class="text-center center-block well well-sm">
            </div>
        </div>
        &lt;!&ndash; edit form column &ndash;&gt;
        <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
            <div class="alert alert-info alert-dismissable">
                <a class="panel-close close" data-dismiss="alert">×</a>
                <i class="fa fa-coffee"></i>
                This is an <strong>.alert</strong>. Use this to show important messages to the user.
            </div>
            <h3>Personal info</h3>
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-lg-3 control-label">First name:</label>
                    <div class="col-lg-8">
                        <input class="form-control" value="Jane" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Last name:</label>
                    <div class="col-lg-8">
                        <input class="form-control" value="Bishop" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Company:</label>
                    <div class="col-lg-8">
                        <input class="form-control" value="" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Email:</label>
                    <div class="col-lg-8">
                        <input class="form-control" value="janesemail@gmail.com" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Time Zone:</label>
                    <div class="col-lg-8">
                        <div class="ui-select">
                            <select id="user_time_zone" class="form-control">
                                <option value="Hawaii">(GMT-10:00) Hawaii</option>
                                <option value="Alaska">(GMT-09:00) Alaska</option>
                                <option value="Pacific Time (US & Canada)">(GMT-08:00) Pacific Time (US & Canada)</option>
                                <option value="Arizona">(GMT-07:00) Arizona</option>
                                <option value="Mountain Time (US & Canada)">(GMT-07:00) Mountain Time (US & Canada)</option>
                                <option value="Central Time (US & Canada)" selected="selected">(GMT-06:00) Central Time (US & Canada)</option>
                                <option value="Eastern Time (US & Canada)">(GMT-05:00) Eastern Time (US & Canada)</option>
                                <option value="Indiana (East)">(GMT-05:00) Indiana (East)</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Username:</label>
                    <div class="col-md-8">
                        <input class="form-control" value="janeuser" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Password:</label>
                    <div class="col-md-8">
                        <input class="form-control" value="11111122333" type="password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Confirm password:</label>
                    <div class="col-md-8">
                        <input class="form-control" value="11111122333" type="password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-8">
                        <input class="btn btn-primary" value="Save Changes" type="button">
                        <span></span>
                        <input class="btn btn-default" value="Cancel" type="reset">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="resources/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $("#login").modal('show');
</script>
</body>
</html>
