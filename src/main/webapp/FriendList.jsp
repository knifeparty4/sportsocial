<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: yuriy
  Date: 18.08.17
  Time: 0:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Результаты поиска</title>

    <!-- Bootstrap -->
    <%--<link href="resources/css/bootstrap.min.css" rel="stylesheet">--%>
    <link href="resources/css/style.css" rel="stylesheet">
    <link href="resources/css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a style="font-family: Lobster" class="navbar-brand" href="/myProfile">SportSocial</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/Main.jsp#about">О проекте</a></li>
                <li><a href="/Main.jsp#tellUs">Контакты</a></li>
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>-->
            </ul>
            <!--<form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>-->
            <ul class="nav navbar-nav navbar-right">
                <form class="navbar-form navbar-left" action="/searchUsers" method="get">
                    <div class="form-group" >
                        <input style="width: 80px;" type="text" class="form-control" name="inputFirstName" placeholder="Имя">
                    </div>
                    <div class="form-group">
                        <input style="width: 130px;" type="text" class="form-control" name="inputSecondName" placeholder="Фамилия">
                    </div>
                    <button type="submit" class="btn btn-default">Искать</button>
                </form>

                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                        <%--<img style="background-color: white; border: none" src="/resources/pics/default.png"--%>
                        <%--<img style="object-fit: cover;" src="/resources/pics/${userObject.getPhone_num()}.png"--%>
                            <img style="object-fit: cover;" src="/images/${userObject.getImg()}"
                                 class="media-object img-circle" title="John Doe" alt="John Doe" width="30px" height="30px"
                                 onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                        <%--<img src="/resources/pics/file2.png"
                             class="img-responsive img-circle img-thumbnail" title="John Doe" alt="John Doe" width="30px" height="30px">--%>
                    </span>
                    <span class="user-name">${userObject.getFirst_name()} ${userObject.getSecond_name()}</span>
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/showFriends">Мои Друзья</a></li>
                        <li><a href="/myProfile">Мой Профиль</a></li>
                        <li><a href="#">Мои Сообщения</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/EditUserProfile.jsp">Редактировать профиль</a></li>
                        <li><a href="/logout">Выйти</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container" style="padding-top: 50px;" >
    <div class="row">

        <c:if test="${emptyRequestsToList != null && !emptyRequestsToList}">
            <h1 class="page-header" style="margin-bottom: 0px;" align="center">
                Входящие заявки <small>Эти люди хотят дружить с вами</small>
            </h1>
            <c:if test="${successGettingRequestsToList != null && !successGettingRequestsToList}">
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert">×</a>
                    <i class="fa fa-coffee"></i>
                    <p>Во время поиска заявок к вам в друзья произошла ошибка! Повторите свой запрос позднее.</p>
                </div>
            </c:if>
        </c:if>
        <c:if test="${successGettingRequestsToList != null && successGettingRequestsToList
                      && requestsToUserList != null && !emptyRequestsToList}">
            <div class="container col-md-offset-1">
                <c:forEach var="requestTo" items="${requestsToUserList}">
                    <div class="span3 well col-md-3" style="margin: 5px;">
                        <center>
                            <a href="/otherUser?id=${requestTo.getId()}">
                                <img src="/images/${requestTo.getImg()}"
                                     name="aboutme" style="object-fit: cover; width: 140px; height: 140px"
                                     class="avatar img-circle img-thumbnail"
                                     onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                            </a>
                            <a href="/otherUser?id=${requestTo.getId()}">
                                <h3>${requestTo.getFirst_name()} ${requestTo.getSecond_name()}</h3>
                            </a>

                            <p> <i class="glyphicon glyphicon-envelope"></i> ${requestTo.getEmail()}
                                <br/><i class="glyphicon glyphicon-globe"></i> ${requestTo.getCountry()}
                                <br/> <i class="glyphicon glyphicon-gift"></i> ${requestTo.getBirth_date()}
                                <br/> <i class="glyphicon glyphicon-user"></i> ${requestTo.getGender()}
                            </p>
                        </center>
                    </div>
                </c:forEach>
            </div>
        </c:if>

        <h1 class="page-header" style="margin-bottom: 0px;" align="center">
            Друзья<small> Вы дружите с этими людьми</small>
        </h1>
        <c:if test="${emptyFriendList != null && emptyFriendList}">
            <div class="alert alert-info alert-dismissable" align="center">
                <a class="panel-close close" data-dismiss="alert">×</a>
                <i class="fa fa-coffee"></i>
                <p>У вас пока нет друзей :( Найдите их с помощью полей поиска в панели наверху!</p>
            </div>
        </c:if>
        <c:if test="${successGettingFriendList != null && !successGettingFriendList}">
            <div class="alert alert-danger alert-dismissable" align="center">
                <a class="panel-close close" data-dismiss="alert">×</a>
                <i class="fa fa-coffee"></i>
                <p>Во время поиска ваших друзей произошла ошибка! Повторите свой запрос позднее.</p>
            </div>
        </c:if>
        <c:if test="${successGettingFriendList != null && successGettingFriendList
                      && friendList != null && !emptyFriendList}">
            <div class="container col-md-offset-1">
                <c:forEach var="friend" items="${friendList}">
                    <div class="span3 well col-md-3" style="margin: 5px;">
                        <center>
                            <a href="/otherUser?id=${friend.getId()}">
                                <img src="/images/${friend.getImg()}"
                                     name="aboutme" style="object-fit: cover; width: 140px; height: 140px"
                                     class="avatar img-circle img-thumbnail"
                                     onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                            </a>
                            <a href="/otherUser?id=${friend.getId()}">
                                <h3>${friend.getFirst_name()} ${friend.getSecond_name()}</h3>
                            </a>

                            <p> <i class="glyphicon glyphicon-envelope"></i> ${friend.getEmail()}
                                <br/><i class="glyphicon glyphicon-globe"></i> ${friend.getCountry()}
                                <br/> <i class="glyphicon glyphicon-gift"></i> ${friend.getBirth_date()}
                                <br/> <i class="glyphicon glyphicon-user"></i> ${friend.getGender()}
                            </p>
                        </center>
                    </div>
                </c:forEach>
            </div>
        </c:if>

        <c:if test="${emptyRequestsFromList != null && !emptyRequestsFromList}">
            <h1 class="page-header" style="margin-bottom: 0px;" align="center">
                Исходящие заявки <small>Вы отправили этим людям запрос в друзья</small>
            </h1>

            <c:if test="${successGettingRequestsFromList != null && !successGettingRequestsFromList}">
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert">×</a>
                    <i class="fa fa-coffee"></i>
                    <p>Во время поиска ваших заявок в друзья другим пользователям
                        произошла ошибка! Повторите свой запрос позднее.</p>
                </div>
            </c:if>
        </c:if>
        <c:if test="${successGettingRequestsFromList != null && successGettingRequestsFromList
                      && requestsFromUserList != null && !emptyRequestsFromList}">
            <div class="container col-md-offset-1">
                <c:forEach var="requestFrom" items="${requestsFromUserList}">
                    <div class="span3 well col-md-3" style="margin: 5px;">
                        <center>
                            <a href="/otherUser?id=${requestFrom.getId()}">
                                <img src="/images/${requestFrom.getImg()}"
                                     name="aboutme" style="object-fit: cover; width: 140px; height: 140px"
                                     class="avatar img-circle img-thumbnail"
                                     onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                            </a>
                            <a href="/otherUser?id=${requestFrom.getId()}">
                                <h3>${requestFrom.getFirst_name()} ${requestFrom.getSecond_name()}</h3>
                            </a>

                            <p> <i class="glyphicon glyphicon-envelope"></i> ${requestFrom.getEmail()}
                                <br/><i class="glyphicon glyphicon-globe"></i> ${requestFrom.getCountry()}
                                <br/> <i class="glyphicon glyphicon-gift"></i> ${requestFrom.getBirth_date()}
                                <br/> <i class="glyphicon glyphicon-user"></i> ${requestFrom.getGender()}
                            </p>
                        </center>
                    </div>
                </c:forEach>
            </div>
        </c:if>
    </div>
</div>
<div style="padding-top: 50px"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="resources/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $("#login").modal('show');
</script>
</body>
</html>
