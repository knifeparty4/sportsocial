<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="entities.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>



<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SportSocial - главная страница</title>

    <!-- Bootstrap -->
    <%--<link href="resources/css/bootstrap.min.css" rel="stylesheet">--%>
    <link href="resources/css/style.css" rel="stylesheet">
    <link href="resources/css/custom.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a style="font-family: Lobster" class="navbar-brand" href="/Main.jsp">SportSocial</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="#about">О проекте</a></li>
                <li><a href="#tellUs">Контакты</a></li>

                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>-->
            </ul>
            <!--<form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>-->
            <ul class="nav navbar-nav navbar-right">
                <c:if test="${loginSuccess != null && loginSuccess}">
                    <form class="navbar-form navbar-left" action="/searchUsers" method="get">
                        <div class="form-group" >
                            <input style="width: 80px;" type="text" class="form-control" name="inputFirstName" placeholder="Имя">
                        </div>
                        <div class="form-group">
                            <input style="width: 130px;" type="text" class="form-control" name="inputSecondName" placeholder="Фамилия">
                        </div>
                        <button type="submit" class="btn btn-default">Искать</button>
                    </form>

                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                        <%--<img style="background-color: white; border: none" src="/resources/pics/default.png"--%>
                        <%--<img style="object-fit: cover;" src="/resources/pics/${userObject.getPhone_num()}.png"--%>
                            <img style="object-fit: cover;" src="/images/${userObject.getImg()}"
                                 class="media-object img-circle" title="John Doe" alt="John Doe" width="30px" height="30px"
                                 onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                        <%--<img src="/resources/pics/file2.png"
                             class="img-responsive img-circle img-thumbnail" title="John Doe" alt="John Doe" width="30px" height="30px">--%>
                    </span>
                        <span class="user-name">${userObject.getFirst_name()} ${userObject.getSecond_name()}</span>
                        <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/showFriends">Мои Друзья</a></li>
                            <li><a href="/myProfile">Мой Профиль</a></li>
                            <li><a href="#">Мои Сообщения</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/EditUserProfile.jsp">Редактировать профиль</a></li>
                            <li><a href="/logout">Выйти</a></li>
                        </ul>
                    </li>
                </c:if>

                <c:if test="${loginSuccess == null || !loginSuccess}">
                    <li><a href="#" data-toggle="modal" data-target="#login">Войти</a></li>
                    <li><a href="/Registration.jsp">Зарегестрироваться</a></li>
                </c:if>

                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </li>-->
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="jumbotron jumbotron-sm" style="margin-top: 50px;">
    <div class="container">
        <div class="row">
            <h1 align="center">Займитетсь спортом</h1>
            <p align="center">SportSocial поможет тренироваться эффективнее и делиться своими
                успехами с друзьями
            </p>
            <p align="center"><a class="btn btn-primary btn-lg" href="#about" role="button">Узнать больше</a></p>
        </div>
    </div>
</div>
<div class="container">

    <!--<div class="jumbotron">
        <h1>Займитетсь спортом</h1>
        <p>SportSocial поможет тренироваться эффективнее и делиться своими
        успехами с друзьями</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Узнать больше</a></p>
    </div>-->
    <div class="row">
        <div class="col-md-4">
            <h2>Общайтесь с друзьями</h2>
            <p>Публикуйте свои результаты и
                наблюдайте за успехами друзей.</p>
        </div>
        <div class="col-md-4">
            <h2>Советуйтесь с профессионалами</h2>
            <p>Профессиональный тренер по вашей дисциплине
                проконсультирует по всем вопросам.</p>
        </div>
        <div class="col-md-4">
            <h2>Следите за своим прогрессом</h2>
            <p>Вся информация о каждой тренировке хранится на вашей странице.</p>
        </div>
    </div>
</div>

<div class="page-header" align="center">
    <h1 id="about">О проекте <small>Наши цели</small></h1>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p>SportSocial - первая в России, Украине и Белоруси спортивная социальная сеть,
                направленная на объединение всех любителей спорта и экстрима!
                Благодаря обширнейшим возможностям, теперь все спортсмены могут
                объединиться на одном проекте и легко общаться на актуальные темы.
                Блоги, видео, новости, фотогалерея, форум и многое другое теперь
                доступны всем спортсменам мира.</p>
        </div>
    </div>
</div>

<div class="page-header" align="center">
    <h1 id="tellUs">Есть что сказать? <small>Расскажите нам о своим идеях</small></h1>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="well well-sm">
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">
                                    Имя
                                </label>
                                <input type="text" class="form-control" id="name" placeholder="Введите ваше имя" required="required" />
                            </div>
                            <div class="form-group">
                                <label for="email">
                                    Email
                                </label>
                                <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                    <input type="email" class="form-control" id="email" placeholder="Введите ваш email" required="required" /></div>
                            </div>
                            <div class="form-group">
                                <label for="subject">
                                    Тема
                                </label>
                                <select id="subject" name="subject" class="form-control" required="required">
                                    <option value="na" selected="">Выберите тему:</option>
                                    <option value="service">Обслуживание клиентов</option>
                                    <option value="suggestions">Ваши предложения</option>
                                    <option value="product">Другое</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">
                                    Сообщение
                                </label>
                                <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                          placeholder="Место для ваших мыслей и пожеланий"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                                Отправить
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4" align="center">
            <form>
                <legend><span class="glyphicon glyphicon-globe"></span> Наш офис</legend>
                <address>
                    <strong>SportSocial, Inc.</strong><br>
                    Невский пр., д.44, ст.метро "Невский проспект<br>
                    Санкт-Петербург, Россия<br>
                    <abbr title="Phone">
                        Т:</abbr>
                    (4012) 456-7890
                </address>
                <address>
                    <strong>Почта для связи</strong><br>
                    <a href="mailto:#">first.last@sportsocial.com</a>
                </address>
            </form>
        </div>
    </div>
</div>

<footer class="container-fluid text-center bg-lightgray">

    <div class="copyrights" style="margin-top:25px;">
        <p>SportSocial © 2017
            <br>
            <span>All Rights Reserved</span>
        </p>
        <p>
            <a href="https://www.vk.com" target="_blank">Мы ВКонтакте</a>
        </p>
    </div>
</footer>

<div class="modal fade" tabindex="-1" role="dialog" id="login">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Войти</h3>
            </div>
            <c:if test="${loginSuccess != null && !loginSuccess}">
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert"></a>
                    <p>Неверные пароль или email! Повторите ввод!</p>
                    <!--<p>Во время обновления возникла ошибка!</p>-->
                </div>
            </c:if>
            <div class="modal-body">
                <form class="form-horizontal" action="/login" method="post">
                    <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Email" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="col-sm-2 control-label">Пароль</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="inputPassword" id="inputPassword" placeholder="Пароль" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="rememberCheck"/> Запомнить меня
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-primary">Войти</button>
                    </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.mod




<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="resources/js/bootstrap.min.js"></script>
<% if (session.getAttribute("loginSuccess") != null && !(Boolean) session.getAttribute("loginSuccess")) { %>
<script type="text/javascript">
    $("#login").modal('show');
</script>
<% } %>
</body>
</html>
