
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Мой профиль</title>

    <!-- Bootstrap -->
   <%-- <link href="resources/css/bootstrap.min.css" rel="stylesheet">--%>
    <link href="resources/css/style.css" rel="stylesheet">
    <link href="resources/css/custom2.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body style="background-color: #f2f2f2">
<c:if test="${(deletePostSuccess != null && deletePostSuccess) ||
              (deleteCommentSuccess != null && deleteCommentSuccess)}">
    <c:redirect url="/myProfile"/>
</c:if>
<fmt:setLocale value="ru_RU" scope="session"/>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a style="font-family: Lobster" class="navbar-brand" href="/myProfile">SportSocial</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/Main.jsp#about">О проекте</a></li>
                <li><a href="/Main.jsp#tellUs">Контакты</a></li>
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>-->
            </ul>
            <!--<form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>-->
            <ul class="nav navbar-nav navbar-right">
                <form class="navbar-form navbar-left" action="/searchUsers" method="get">
                    <div class="form-group" >
                        <input style="width: 80px;" type="text" class="form-control" name="inputFirstName" placeholder="Имя">
                    </div>
                    <div class="form-group">
                        <input style="width: 130px;" type="text" class="form-control" name="inputSecondName" placeholder="Фамилия">
                    </div>
                    <button type="submit" class="btn btn-default">Искать</button>
                </form>

                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                        <%--<img style="background-color: white; border: none" src="/resources/pics/default.png"--%>
                        <%--<img style="object-fit: cover;" src="/resources/pics/${userObject.getPhone_num()}.png"--%>
                            <img style="object-fit: cover;" src="/images/${userObject.getImg()}"
                                 class="media-object img-circle" title="John Doe" alt="John Doe" width="30px" height="30px"
                                 onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                        <%--<img src="/resources/pics/file2.png"
                             class="img-responsive img-circle img-thumbnail" title="John Doe" alt="John Doe" width="30px" height="30px">--%>
                    </span>
                    <span class="user-name">${userObject.getFirst_name()} ${userObject.getSecond_name()}</span>
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/showFriends">Мои Друзья</a></li>
                        <li><a href="/myProfile">Мой Профиль</a></li>
                        <li><a href="#">Мои Сообщения</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/EditUserProfile.jsp">Редактировать профиль</a></li>
                        <li><a href="/logout">Выйти</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!--
<link rel="stylesheet" href="https://bootswatch.com/cosmo/bootstrap.min.css">
-->

<div class="mainbody container-fluid col-md-10 col-md-offset-1" style="padding-top: 70px">
    <div class="row">
        <!--<div style="padding-top:50px;"> </div>-->
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div align="center">
                            <img src="/images/${userObject.getImg()}"
                                 name="aboutme" style="object-fit: cover; width: 300px; height: 300px"
                                 class="avatar img-thumbnail"
                                 onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                        </div>
                        <div class="media-body">
                            <hr>
                            <h3><strong>О себе</strong></h3>
                            <p>${userObject.getAbout()}</p>
                            <hr>
                            <h3><strong>Местоположение</strong></h3>
                            <p>${userObject.getCountry()}</p>
                            <hr>
                            <h3><strong>Пол</strong></h3>
                            <p>${userObject.getGender()}</p>
                            <hr>
                            <h3><strong>Связь со мной</strong></h3>
                            <p>телефон: ${userObject.getPhone_num()}</p>
                            <p>email: ${userObject.getEmail()}</p>
                            <hr>
                            <h3><strong>День рождения</strong></h3>
                            <p>${userObject.getBirth_date()}</p>
                            <hr>
                            <h3><strong>Любимые виды спорта</strong></h3>
                            <p>${userObject.getSports()}</p>
                            <hr>
                            <h3><strong>Антропометрия</strong></h3>
                            <p>рост: ${userObject.getHeight()}см</p>
                            <p>вес: ${userObject.getWeight()}кг</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <span>
                        <h1 class="panel-title pull-left" style="font-size:30px;">
                            ${userObject.getFirst_name()} ${userObject.getSecond_name()}
                                <small>${userObject.getEmail()}</small> <i class="fa fa-check text-success" aria-hidden="true" data-toggle="tooltip"
                                 data-placement="bottom" title="Вы находитесь на своей странице"></i>
                        </h1>
                        <%--<div class="dropdown pull-right">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Friends
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="#">Familly</a></li>
                                <li><a href="#"><i class="fa fa-fw fa-check" aria-hidden="true"></i> Friends</a></li>
                                <li><a href="#">Work</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#"><i class="fa fa-fw fa-plus" aria-hidden="true"></i> Add a new aspect</a></li>
                            </ul>
                        </div>--%>
                    </span>
                    <br><br>
                    <i class="fa fa-tags" aria-hidden="true"></i> <a href="/tags/diaspora" class="tag">#diaspora</a> <a href="/tags/hashtag" class="tag">#hashtag</a> <a href="/tags/caturday" class="tag">#caturday</a>
                    <br><br><hr>
                    <span class="pull-left">
                        <!--<a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-fw fa-files-o" aria-hidden="true"></i> Posts</a>-->
                        <!--<a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-fw fa-picture-o" aria-hidden="true"></i> Photos <span class="badge">42</span></a>-->
                        <a href="/showFriends" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-fw fa-users" aria-hidden="true"></i> Список моих друзей <span class="badge">42</span></a>
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Список моих сообщений <span class="badge">42</span></a>
                    </span>
                    <!--<span class="pull-right">
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-lg fa-at" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Mention"></i></a>
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-lg fa-envelope-o" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Message"></i></a>
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-lg fa-ban" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Ignore"></i></a>
                    </span>-->
                </div>
            </div>
            <hr>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div class="pull-left">
                            <a href="#">
                                <img style="object-fit: cover;" src="/images/${userObject.getImg()}"
                                     class="media-object img-circle" title="John Doe" alt="John Doe" width="35px" height="35px"
                                     onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                            </a>
                        </div>
                        <form action="/createPost" method="post" id="postCreate">
                            <div class="media-body">
                                <textarea form="postCreate" name="value" class="form-control" rows="3" placeholder="Расскажите о своей тренировке!"></textarea>
                            </div>
                            <input type="hidden" name="receiverId" value="${userObject.getId()}">
                            <div style="padding-left: 45px; padding-top: 5px;">
                                <button type="submit" class="btn btn-primary btn-sm">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Simple post content example. -->
            <c:if test="${gotNoPosts != null && gotNoPosts}">
                <div class="col-md-12">
                    <p align="center" style="color: #b3b3b3; font-size: large">Здесь будут отображаться ваши записи и записи ваших друзей</p>
                </div>
            </c:if>
            <c:if test="${gotNoPosts != null && gettingPostsSuccess != null && gettingPostsSuccess && !gotNoPosts}">
                <c:forEach items="${postMap}" var="pair">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="pull-left">
                                <a href="#">
                                    <c:if test="${pair.key.getAuthorId() == userObject.getId()}">
                                        <img style="object-fit: cover; margin-right:8px; margin-top:-5px;"
                                             src="/images/${userObject.getImg()}"
                                             class="media-object img-circle" title="John Doe" alt="John Doe" width="45px" height="45px"
                                             onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                                    </c:if>
                                    <c:if test="${pair.key.getAuthorId() != userObject.getId()}">
                                        <img style="object-fit: cover; margin-right:8px; margin-top:-5px;"
                                             src="/images/${pair.value.getImg()}"
                                             class="media-object img-circle" title="John Doe" alt="John Doe" width="45px" height="45px"
                                             onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                                    </c:if>

                                </a>
                            </div>

                            <h4><a href="#" style="text-decoration:none;"><strong>${pair.value.getFirst_name()} ${pair.value.getSecond_name()}</strong></a>
                                <small><small><a class="pull-right" href="#" style="text-decoration:none; color:grey; font-size: 12px"><i><i class="fa fa-clock-o" aria-hidden="true">
                                </i> <fmt:formatDate type = "both" dateStyle = "long" timeStyle = "short" value = "${pair.key.getCreationDate()}"/></i></a></small></small>
                            </h4>
                            <span>
                            <div class="navbar-right">
                                <div class="dropdown">
                                    <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <i class="fa fa-cog" aria-hidden="true"></i>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                                        <form id="form${pair.key.getId()}" action="/deletePost" method="get" style="display: none">
                                            <input type="text" value="${pair.key.getId()}" name="postId">
                                            <input type="text" value="${userObject.getId()}" name="receiverId">
                                        </form>
                                        <li><a href="javascript:;" onclick="document.getElementById('form${pair.key.getId()}').submit();">Удалить пост</a></li>
                                        <c:if test="${pair.value.getId() eq userObject.getId()}">
                                            <li role="separator" class="divider"></li>
                                            <li><a href="javascript:;" onclick="myFunction(${pair.key.getId()}, '${pair.key.getValue()}');">Редактировать пост</a></li>
                                        </c:if>
                                    </ul>
                                </div>
                            </div>
                            </span>
                            <hr>
                            <div class="post-content">
                                <p style="margin-bottom: 0px" id="postToEdit${pair.key.getId()}">${pair.key.getValue()}</p>
                                <form id="editPostForm${pair.key.getId()}" method="post" action="/editPost">
                                    <div class="media-body" style="display: none" id="divEditPostArea${pair.key.getId()}">
                                        <textarea name="editedValue" id="editPostArea${pair.key.getId()}" class="form-control" rows="3"></textarea>
                                    </div>
                                    <input style="display: none;" type="text" value="${pair.key.getId()}" name="postId">
                                    <input style="display: none" type="text" value="${userObject.getId()}" name="receiverId">
                                    <div style="padding-top: 5px;">
                                        <button id="editBtn${pair.key.getId()}" style="display: none" type="submit" class="btn btn-primary btn-sm">Отправить</button>
                                    </div>
                                </form>
                            </div>
                            <%--<hr>--%>
                            <%--<div>
                                <div class="pull-right btn-group-xs">
                                    <a class="btn btn-default btn-xs"><i class="fa fa-heart" aria-hidden="true"></i> Лайк</a>
                                    <a class="btn btn-default btn-xs"><i class="fa fa-retweet" aria-hidden="true"></i> Репост</a>
                                    <!--<a class="btn btn-default btn-xs"><i class="fa fa-comment" aria-hidden="true"></i> Comment</a>-->
                                </div>
                                <div class="pull-left">
                                    <p class="text-muted" style="margin-left:5px;"><i class="fa fa-globe" aria-hidden="true"></i> Public</p>
                                    &lt;%&ndash;<c:forEach var="comment" items="${pair.key.getComments()}">
                                        <input type="text" value="${comment.getValue()}" name="receiverId">
                                    </c:forEach>&ndash;%&gt;
                                </div>
                                <br>
                            </div>--%>

                            <c:if test="${pair.key.getComments().size() != 0}">
                                <hr>
                                <div id="commentsDiv${pair.key.getId()}">
                                    <a onclick="myFunction4(${pair.key.getId()})" class="btn btn-default btn-xs"><i class="fa fa-bars" aria-hidden="true"></i> Показать комментарии (${pair.key.getComments().size()})</a>
                                    <c:forEach var="commentPair" items="${pair.key.getComments()}">
                                        <div class="post-content" style="display: none" id="comm${commentPair.key.getId()}">
                                            <hr style="display: none;">
                                            <div class="panel-default">
                                                <div class="panel-body" style="padding-bottom: 0px; padding-top: 25px">
                                                    <div class="pull-left">
                                                        <a href="#">
                                                            <img style="object-fit: cover; margin-right:8px; margin-top:-5px;"
                                                                 src="/images/${commentPair.value.getImg()}"
                                                                 class="media-object img-circle" title="John Doe" alt="John Doe" width="35px" height="35px"
                                                                 onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                                                        </a>
                                                    </div>
                                                    <h4><a href="#" style="text-decoration:none;">
                                                        <strong>${commentPair.value.getFirst_name()} ${commentPair.value.getSecond_name()}</strong>
                                                    </a><small><small><a class="pull-right" href="#" style="text-decoration:none; color:grey; font-size: 12px"><i><i class="fa fa-clock-o" aria-hidden="true">
                                                    </i> <fmt:formatDate type = "both" dateStyle = "long" timeStyle = "short" value = "${commentPair.key.getCreationDate()}"/></i></a></small></small>
                                                    </h4>
                                                    <span>
                                                        <div class="navbar-right">
                                                            <div class="dropdown">
                                                                <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                                                    <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                                                                    <form id="form${commentPair.key.getId()}" action="/deleteComment" method="get" style="display: none">
                                                                        <input type="text" value="${commentPair.key.getId()}" name="commentId">
                                                                        <input type="text" value="${userObject.getId()}" name="receiverId">
                                                                    </form>
                                                                    <li><a href="javascript:;" onclick="document.getElementById('form${commentPair.key.getId()}').submit();">Удалить комментарий</a></li>
                                                                    <c:if test="${commentPair.value.getId() eq userObject.getId()}">
                                                                        <li role="separator" class="divider"></li>
                                                                        <li><a href="javascript:;" onclick="myFunction5(${commentPair.key.getId()}, '${commentPair.key.getValue()}');">Редактировать комментарий</a></li>
                                                                    </c:if>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </span>
                                                    <hr style="margin-bottom: 10px;">
                                                    <div class="post-content">
                                                        <p style="margin-bottom: 0px" id="commentToEdit${commentPair.key.getId()}">${commentPair.key.getValue()}</p>
                                                        <form id="editCommentForm${commentPair.key.getId()}" method="post" action="/editComment">
                                                            <div class="media-body" style="display: none" id="divEditCommentArea${commentPair.key.getId()}">
                                                                <textarea style="display: none" name="editCommentValue" id="editCommentArea${commentPair.key.getId()}" class="form-control" rows="3"></textarea>
                                                            </div>
                                                            <input style="display: none;" type="text" value="${commentPair.key.getId()}" name="commentId">
                                                            <input style="display: none" type="text" value="${userObject.getId()}" name="receiverId">
                                                            <div style="padding-top: 5px;">
                                                                <button id="editCommentBtn${commentPair.key.getId()}" style="display: none" type="submit" class="btn btn-primary btn-sm">Отправить</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                    <script>
                                        function myFunction4(id) {
                                            var div = document.getElementById('commentsDiv'.concat(id));
                                            var divs = div.getElementsByTagName('div');
                                            for (var i = 0; i < divs.length; i += 1) {
                                                divs[i].style.display="block"
                                            }
                                            /*document.getElementById("editPostArea".concat(id)).innerHTML = value
                                             document.getElementById("divEditPostArea".concat(id)).style.display="table-cell"
                                             document.getElementById("postToEdit".concat(id)).style.display="none"
                                             document.getElementById("editBtn".concat(id)).style.display="inline-block"*/
                                        }
                                    </script>
                                </div>
                            </c:if>
                            <hr>
                            <div class="media">
                                <div class="pull-left">
                                    <a href="#">
                                        <img style="object-fit: cover; margin-left:3px; margin-right:-5px;"
                                             src="/images/${userObject.getImg()}"
                                             class="media-object img-circle" title="John Doe" alt="John Doe" width="35px" height="35px"
                                             onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                                    </a>
                                </div>
                                <form action="/createComment" method="post">
                                    <div class="media-body">
                                        <textarea name="commentValue" id="ta${pair.key.getId()}" <%--onblur="myFunction3(${pair.key.getId()})" --%>onclick="myFunction2(${pair.key.getId()})"
                                                  class="form-control" rows="1" placeholder="Ваш комментарий"></textarea>
                                    </div>
                                    <input type="hidden" name="receiverId" value="${userObject.getId()}">
                                    <input type="hidden" name="postId" value="${pair.key.getId()}">
                                    <button id="btn${pair.key.getId()}" style="display: none; margin-left: 45px; margin-top: 5px" type="submit" class="btn btn-primary btn-sm">Отправить</button>
                                    <button onclick="myFunction3(${pair.key.getId()});" id="btnC${pair.key.getId()}" style="display: none;margin-top: 5px;" type="button" class="btn btn-default btn-sm">Отмена</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </c:forEach>
                <script>
                    function myFunction(id, value) {
                        document.getElementById("editPostArea".concat(id)).innerHTML = value
                        document.getElementById("divEditPostArea".concat(id)).style.display="table-cell"
                        document.getElementById("postToEdit".concat(id)).style.display="none"
                        document.getElementById("editBtn".concat(id)).style.display="inline-block"
                    }
                    function myFunction5(id, value) {
                        document.getElementById("editCommentArea".concat(id)).innerHTML = value
                        document.getElementById("editCommentArea".concat(id)).style.display = "table-cell"
                        document.getElementById("divEditCommentArea".concat(id)).style.display="table-cell"
                        document.getElementById("commentToEdit".concat(id)).style.display="none"
                        document.getElementById("editCommentBtn".concat(id)).style.display="inline-block"
                    }
                    function myFunction2(id) {
                        document.getElementById("btn".concat(id)).style.display="inline-block"
                        document.getElementById("btnC".concat(id)).style.display="inline-block"
                        document.getElementById("ta".concat(id)).rows="3"
                    }
                    function myFunction3(id) {
                        document.getElementById("btn".concat(id)).style.display="none"
                        document.getElementById("btnC".concat(id)).style.display="none"
                        document.getElementById("ta".concat(id)).rows="1"
                    }
                </script>
            </c:if>

            <!--&lt;!&ndash; Reshare Example &ndash;&gt;
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="pull-left">
                        <a href="#">
                            <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">
                        </a>
                    </div>
                    <h4><a href="#" style="text-decoration:none;"><strong>John Doe</strong></a> – <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 42 minutes ago</i></a></small></small></h4>
                    <span>
                        <div class="navbar-right">
                            <div class="dropdown">
                                <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                                    <li><a href="#"><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> Report</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-ban" aria-hidden="true"></i> Ignore</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-bell" aria-hidden="true"></i> Enable notifications for this post</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-eye-slash" aria-hidden="true"></i> Hide</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="fa fa-fw fa-trash" aria-hidden="true"></i> Delete</a></li>
                                </ul>
                            </div>
                        </div>
                    </span>
                    <hr>
                    <div class="post-content">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="pull-left">
                                    <a href="#">
                                        <img class="media-object img-circle" src="https://diaspote.org/uploads/images/thumb_large_283df6397c4db3fe0344.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">
                                    </a>
                                </div>
                                <h4><a href="#" style="text-decoration:none;"><strong>✪ SтeғOғғιcιel ✪ ツ</strong></a> – <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> about 15 hours ago</i></a></small></small></h4>
                                <hr>
                                <div class="post-content">
                                    Reshare post example.
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div>
                        <div class="pull-right btn-group-xs">
                            <a class="btn btn-default btn-xs"><i class="fa fa-heart" aria-hidden="true"></i> Like</a>
                            <a class="btn btn-default btn-xs"><i class="fa fa-retweet" aria-hidden="true"></i> Reshare</a>
                            <a class="btn btn-default btn-xs"><i class="fa fa-comment" aria-hidden="true"></i> Comment</a>
                        </div>
                        <div class="pull-left">
                            <p class="text-muted" style="margin-left:5px;"><i class="fa fa-globe" aria-hidden="true"></i> Public</p>
                        </div>
                        <br>
                    </div>
                    <hr>
                    <div class="media">
                        <div class="pull-left">
                            <a href="#">
                                <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="35px" height="35px" style="margin-left:3px; margin-right:-5px;">
                            </a>
                        </div>
                        <div class="media-body">
                            <textarea class="form-control" rows="1" placeholder="Comment"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            &lt;!&ndash; Sample post content with picture. &ndash;&gt;
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="pull-left">
                        <a href="#">
                            <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">
                        </a>
                    </div>
                    <h4><a href="#" style="text-decoration:none;"><strong>John Doe</strong></a> – <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 42 minutes ago</i></a></small></small></h4>
                    <span>
                        <div class="navbar-right">
                            <div class="dropdown">
                                <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                                    <li><a href="#"><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> Report</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-ban" aria-hidden="true"></i> Ignore</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-bell" aria-hidden="true"></i> Enable notifications for this post</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-eye-slash" aria-hidden="true"></i> Hide</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="fa fa-fw fa-trash" aria-hidden="true"></i> Delete</a></li>
                                </ul>
                            </div>
                        </div>
                    </span>
                    <hr>
                    <div class="post-content">
                        <p>Sample post content with picture.</p>
                        <img class="img-responsive" src="https://media.giphy.com/media/j1QQj6To9Pbxu/giphy.gif">
                        <p><br><a href="/tags/christmas" class="tag">#Christmas</a> <a href="/tags/caturday" class="tag">#Caturday</a></p>
                    </div>
                    <hr>
                    <div>
                        <div class="pull-right btn-group-xs">
                            <a class="btn btn-default btn-xs"><i class="fa fa-heart" aria-hidden="true"></i> Like</a>
                            <a class="btn btn-default btn-xs"><i class="fa fa-retweet" aria-hidden="true"></i> Reshare</a>
                            <a class="btn btn-default btn-xs"><i class="fa fa-comment" aria-hidden="true"></i> Comment</a>
                        </div>
                        <div class="pull-left">
                            <p class="text-muted" style="margin-left:5px;"><i class="fa fa-globe" aria-hidden="true"></i> Public <strong>via mobile</strong></p>
                        </div>
                        <br>
                    </div>
                    <hr>
                    <div class="media">
                        <div class="pull-left">
                            <a href="#">
                                <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="35px" height="35px" style="margin-left:3px; margin-right:-5px;">
                            </a>
                        </div>
                        <div class="media-body">
                            <textarea class="form-control" rows="1" placeholder="Comment"></textarea>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- Sample post content with comments. -->
            <%--<div class="panel panel-default">
                <div class="panel-body">
                    <div class="pull-left">
                        <a href="#">
                            <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">
                        </a>
                    </div>
                    <h4><a href="#" style="text-decoration:none;"><strong>John Doe</strong></a> – <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 42 minutes ago</i></a></small></small></h4>
                    <span>
                        <div class="navbar-right">
                            <div class="dropdown">
                                <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                                    <li><a href="#"><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> Report</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-ban" aria-hidden="true"></i> Ignore</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-bell" aria-hidden="true"></i> Enable notifications for this post</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-eye-slash" aria-hidden="true"></i> Hide</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="fa fa-fw fa-trash" aria-hidden="true"></i> Delete</a></li>
                                </ul>
                            </div>
                        </div>
                    </span>
                    <hr>
                    <div class="post-content">
                        <p>Sample post content with comments.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                    </div>
                    <hr>
                    <div>
                        <div class="pull-right btn-group-xs">
                            <a class="btn btn-default btn-xs"><i class="fa fa-heart" aria-hidden="true"></i> Like</a>
                            <a class="btn btn-default btn-xs"><i class="fa fa-comment" aria-hidden="true"></i> Comment</a>
                        </div>
                        <div class="pull-left">
                            <p class="text-muted" style="margin-left:5px;"><i class="fa fa-user-secret" aria-hidden="true"></i> Limited</p>
                        </div>
                        <br>
                    </div>
                    <hr>
                    <div>
                        <a class="btn btn-default btn-xs"><i class="fa fa-bars" aria-hidden="true"></i> Show 12 more comments</a>
                        <hr>
                        <div class="post-content">
                            <div class="panel-default">
                                <div class="panel-body">
                                    <div class="pull-left">
                                        <a href="#">
                                            <img class="media-object img-circle" src="https://diaspote.org/uploads/images/thumb_large_283df6397c4db3fe0344.png" width="35px" height="35px" style="margin-right:8px; margin-top:-5px;">
                                        </a>
                                    </div>
                                    <h4><a href="#" style="text-decoration:none;"><strong>✪ SтeғOғғιcιel ✪ ツ</strong></a></h4>
                                    <hr>
                                    <div class="post-content">
                                        Comment example.<br><br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at arcu sapien. Donec laoreet, nisl quis tempor hendrerit, libero augue blandit turpis, in dignissim odio mauris eu tortor. Ut hendrerit ipsum elit, a elementum nulla ultrices eu. In posuere mollis efficitur. Maecenas justo turpis, tristique sit amet ultricies quis, molestie eget ex. Nam vestibulum consequat tincidunt. Morbi vitae placerat sapien. Phasellus quis mi tincidunt sem scelerisque tincidunt. Ut viverra porttitor sagittis. Phasellus aliquam auctor purus, id sollicitudin mauris pulvinar ac. Vivamus vel erat nec orci ultricies iaculis quis sit amet augue. Vestibulum aliquam felis lorem, interdum porttitor sapien sodales ac. Maecenas id ullamcorper risus. Suspendisse id dui sed urna rutrum pharetra. Nam eu lectus et orci vestibulum bibendum. Mauris et pulvinar dui, ac facilisis leo.
                                        <br><small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 12 minutes ago</i></a></small></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="post-content">
                            <div class="panel-default">
                                <div class="panel-body">
                                    <div class="pull-left">
                                        <a href="#">
                                            <img class="media-object img-circle" src="https://lut.im/yR07xwobAA/bZpvdTZmBBTZDJDd.png" width="35px" height="35px" style="margin-right:8px; margin-top:-5px;">
                                        </a>
                                    </div>
                                    <h4><a href="#" style="text-decoration:none;"><strong>Mi Chleen</strong></a></h4>
                                    <hr>
                                    <div class="post-content">
                                        Another comment.<br><br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at arcu sapien. Donec laoreet, nisl quis tempor hendrerit, libero augue blandit turpis, in dignissim odio mauris eu tortor. Ut hendrerit ipsum elit, a elementum nulla ultrices eu. In posuere mollis efficitur. Maecenas justo turpis, tristique sit amet ultricies quis, molestie eget ex. Nam vestibulum consequat tincidunt. Morbi vitae placerat sapien. Phasellus quis mi tincidunt sem scelerisque tincidunt. Ut viverra porttitor sagittis. Phasellus aliquam auctor purus, id sollicitudin mauris pulvinar ac. Vivamus vel erat nec orci ultricies iaculis quis sit amet augue. Vestibulum aliquam felis lorem, interdum porttitor sapien sodales ac. Maecenas id ullamcorper risus. Suspendisse id dui sed urna rutrum pharetra. Nam eu lectus et orci vestibulum bibendum. Mauris et pulvinar dui, ac facilisis leo.
                                        <br><small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 9 minutes ago</i></a></small></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="post-content">
                            <div class="panel-default">
                                <div class="panel-body">
                                    <div class="pull-left">
                                        <a href="#">
                                            <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="35px" height="35px" style="margin-right:8px; margin-top:-5px;">
                                        </a>
                                    </div>
                                    <h4><a href="#" style="text-decoration:none;"><strong>John Doe</strong></a></h4>
                                    <hr>
                                    <div class="post-content">
                                        Yet another post.<br><br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at arcu sapien. Donec laoreet, nisl quis tempor hendrerit, libero augue blandit turpis, in dignissim odio mauris eu tortor. Ut hendrerit ipsum elit, a elementum nulla ultrices eu. In posuere mollis efficitur. Maecenas justo turpis, tristique sit amet ultricies quis, molestie eget ex. Nam vestibulum consequat tincidunt. Morbi vitae placerat sapien. Phasellus quis mi tincidunt sem scelerisque tincidunt. Ut viverra porttitor sagittis. Phasellus aliquam auctor purus, id sollicitudin mauris pulvinar ac. Vivamus vel erat nec orci ultricies iaculis quis sit amet augue. Vestibulum aliquam felis lorem, interdum porttitor sapien sodales ac. Maecenas id ullamcorper risus. Suspendisse id dui sed urna rutrum pharetra. Nam eu lectus et orci vestibulum bibendum. Mauris et pulvinar dui, ac facilisis leo.
                                        <br><small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 2 minutes ago</i></a></small></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="media">
                        <div class="pull-left">
                            <a href="#">
                                <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="35px" height="35px" style="margin-left:3px; margin-right:-5px;">
                            </a>
                        </div>
                        <div class="media-body">
                            <textarea class="form-control" rows="1" placeholder="Comment"></textarea>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="postsError">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Ошибка</h3>
            </div>
            <c:if test="${(gettingPostsSuccess != null && !gettingPostsSuccess) ||
            (gettingUsersFromPostsWithErr != null && gettingUsersFromPostsWithErr)}">
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert"></a>
                    <p>Во время получения ваших постов произошла ошибка!</p>
                </div>
            </c:if>
            <c:if test="${postCreationSuccess != null && !postCreationSuccess}">
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert"></a>
                    <p>Во время добавления поста произошла ошибка!</p>
                </div>
            </c:if>
            <c:if test="${deletePostSuccess != null && !deletePostSuccess}">
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert"></a>
                    <p>Во время удаления поста произошла ошибка!</p>
                </div>
            </c:if>
            <c:if test="${noPost != null && noPost}">
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert"></a>
                    <p>Во время редактирования поста произошла ошибка! Возможно, этого поста не существует!</p>
                </div>
            </c:if>
            <c:if test="${noPost!= null && !noPost}">
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert"></a>
                    <p>Во время редактирования поста произошла ошибка! Возможно, вы ввели пустое сообщение!</p>
                </div>
            </c:if>
            <c:if test="${postCreationSuccess != null && !postCreationSuccess}">
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert"></a>
                    <p>Во время создания поста произошла ошибка!</p>
                </div>
            </c:if>
            <c:if test="${commentCreationSuccess != null && !commentCreationSuccess}">
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert"></a>
                    <p>Во время создания комментария произошла ошибка!</p>
                </div>
            </c:if>
            <c:if test="${deleteCommentSuccess != null && !deleteCommentSuccess}">
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert"></a>
                    <p>Во время удаления комментария произошла ошибка!</p>
                </div>
            </c:if>
            <div class="modal-body">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="resources/js/bootstrap.min.js"></script>

<c:if test="${(gettingPostsSuccess != null && !gettingPostsSuccess) ||
              (gettingUsersFromPostsWithErr != null && gettingUsersFromPostsWithErr) ||
              (postCreationSuccess != null && !postCreationSuccess) ||
              (deletePostSuccess != null && !deletePostSuccess) ||
              (editingPostSuccess != null && !editingPostSuccess) ||
              (postCreationSuccess != null && !postCreationSuccess) ||
              (commentCreationSuccess != null && !commentCreationSuccess) ||
              (deleteCommentSuccess != null && !deleteCommentSuccess)}">
    <script type="text/javascript">
        $("#postsError").modal('show');
    </script>
</c:if>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    $(function () {
        $('[data-toggle="popover"]').popover()
    })
</script>
</body>

</html>

