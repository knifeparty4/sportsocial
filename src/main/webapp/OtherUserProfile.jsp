<%@ page import="util.enums.FriendshipStatus" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%--
<%@ page import="util.enums.FriendshipStatus" %>
<jsp:useBean id="StatusBean" scope="request" class="util.enums.FriendshipStatus">
    <c:set var="a1" value="${StatusBean.FRIEND}"/>
    <c:set var="a2" value="${StatusBean.NOT_FRIEND}"/>
    <c:set var="a3" value="${StatusBean.REQUESTED_TO_USER}"/>
    <c:set var="a4" value="${StatusBean.REQUETED_BY_USER}"/>
</jsp:useBean>--%>
<c:set var="FRIEND" value="<%=FriendshipStatus.FRIEND%>"/>
<c:set var="NOT_FRIEND" value="<%=FriendshipStatus.NOT_FRIEND%>"/>
<c:set var="TO" value="<%=FriendshipStatus.REQUESTED_TO_USER%>"/>
<c:set var="BY" value="<%=FriendshipStatus.REQUETED_BY_USER%>"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Профиль пользователя ${userObject.getFirst_name()} ${userObject.getSecond_name()}</title>

    <!-- Bootstrap -->
    <%--<link href="resources/css/bootstrap.min.css" rel="stylesheet">--%>
    <link href="resources/css/style.css" rel="stylesheet">
    <link href="resources/css/custom2.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body style="background-color: #f2f2f2">
<c:if test="${(requestSendSuccess != null && requestSendSuccess) ||
              (successAccept != null && successAccept) ||
              (successFriendDelete != null && successFriendDelete) ||
              (deletePostSuccess != null && deletePostSuccess) ||
              (editingPostSuccess != null && editingPostSuccess) ||
              (deleteCommentSuccess != null && deleteCommentSuccess)}">
    <c:redirect url="/otherUser?id=${userToShow.getId()}"/>
</c:if>

<fmt:setLocale value="ru_RU" scope="session"/>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a style="font-family: Lobster" class="navbar-brand" href="/myProfile">SportSocial</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/Main.jsp#about">О проекте</a></li>
                <li><a href="/Main.jsp#tellUs">Контакты</a></li>
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>-->
            </ul>
            <!--<form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>-->
            <ul class="nav navbar-nav navbar-right">
                <form class="navbar-form navbar-left" action="/searchUsers" method="get">
                    <div class="form-group" >
                        <input style="width: 80px;" type="text" class="form-control" name="inputFirstName" placeholder="Имя">
                    </div>
                    <div class="form-group">
                        <input style="width: 130px;" type="text" class="form-control" name="inputSecondName" placeholder="Фамилия">
                    </div>
                    <button type="submit" class="btn btn-default">Искать</button>
                </form>

                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                        <%--<img style="background-color: white; border: none" src="/resources/pics/default.png"--%>
                        <%--<img style="object-fit: cover;" src="/resources/pics/${userObject.getPhone_num()}.png"--%>
                            <img style="object-fit: cover;" src="/images/${userObject.getImg()}"
                                 class="media-object img-circle" title="John Doe" alt="John Doe" width="30px" height="30px"
                                 onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                        <%--<img src="/resources/pics/file2.png"
                             class="img-responsive img-circle img-thumbnail" title="John Doe" alt="John Doe" width="30px" height="30px">--%>
                    </span>
                    <span class="user-name">${userObject.getFirst_name()} ${userObject.getSecond_name()}</span>
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/showFriends">Мои Друзья</a></li>
                        <li><a href="/myProfile">Мой Профиль</a></li>
                        <li><a href="#">Мои Сообщения</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/EditUserProfile.jsp">Редактировать профиль</a></li>
                        <li><a href="/logout">Выйти</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!--
<link rel="stylesheet" href="https://bootswatch.com/cosmo/bootstrap.min.css">
-->

<div class="mainbody container-fluid col-md-10 col-md-offset-1" style="padding-top: 70px">
    <div class="row">
        <!--<div style="padding-top:50px;"> </div>-->
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div align="center">
                            <img src="/images/${userToShow.getImg()}"
                                 name="aboutme" style="object-fit: cover; width: 300px; height: 300px"
                                 class="avatar img-thumbnail"
                                 onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                        </div>
                        <div class="media-body">
                            <hr>
                            <h3><strong>О себе</strong></h3>
                            <p>${userToShow.getAbout()}</p>
                            <hr>
                            <h3><strong>Местоположение</strong></h3>
                            <p>${userToShow.getCountry()}</p>
                            <hr>
                            <h3><strong>Пол</strong></h3>
                            <p>${userToShow.getGender()}</p>
                            <hr>
                            <h3><strong>Связь со мной</strong></h3>
                            <p>телефон: ${userToShow.getPhone_num()}</p>
                            <p>email: ${userToShow.getEmail()}</p>
                            <hr>
                            <h3><strong>День рождения</strong></h3>
                            <p>${userToShow.getBirth_date()}</p>
                            <hr>
                            <h3><strong>Любимые виды спорта</strong></h3>
                            <p>${userObject.getSports()}</p>
                            <hr>
                            <h3><strong>Антропометрия</strong></h3>
                            <p>рост: ${userObject.getHeight()}см</p>
                            <p>вес: ${userObject.getWeight()}кг</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <span>
                        <h1 class="panel-title pull-left" style="font-size:30px;">
                            ${userToShow.getFirst_name()} ${userToShow.getSecond_name()}
                                <small>${userToShow.getEmail()}</small>
                        </h1>
                        <c:if test="${friendshipStatus == FRIEND}">
                            <div class="dropdown pull-right">
                                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" style="background-color: #bdccdb; border: hidden">
                                    Вы дружите
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="/deleteFriendship?id=${userToShow.getId()}">Удалить из друзей</a></li>
                                </ul>
                            </div>
                        </c:if>
                        <c:if test="${friendshipStatus == BY}">
                            <div class="dropdown pull-right">
                                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" style="background-color: #bdccdb; border: hidden">
                                    Вы уже отправили заявку
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <li><a href="/deleteFriendship?id=${userToShow.getId()}">Отменить заявку</a></li>
                                </ul>
                            </div>
                        </c:if>
                        <c:if test="${friendshipStatus == NOT_FRIEND}">
                            <div class="pull-right">
                                <form action="/sendFriendship" method="get">
                                    <input type="hidden" name='id' id='id1' value=${userToShow.getId()} />
                                    <button type="submit" class="btn btn-success">
                                        Добавить в друзья
                                    </button>
                                </form>
                            </div>
                        </c:if>
                        <c:if test="${friendshipStatus == TO}">
                            <div class="dropdown pull-right">
                                <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" style="background-color: #bdccdb; border: hidden">
                                    Пользователь хочет с вами дружить
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu3">
                                    <li><a href="/acceptFriendship?id=${userToShow.getId()}">Подтвердить заявку</a></li>
                                    <li><a href="/deleteFriendship?id=${userToShow.getId()}">Отказаться</a></li>
                                </ul>
                            </div>
                            <%--<div class="pull-right">
                                <form action="/acceptFriendship" method="get">
                                    <input type="hidden" name='id' id='id2' value=${userToShow.getId()} />
                                    <button type="submit" class="btn btn-success">
                                        Подтвердить заявку
                                    </button>
                                </form>
                            </div>--%>
                        </c:if>
                        <%--<div class="pull-right">
                            <form action="">
                                 <button style="background-color: #bdccdb; border: hidden" type="submit" class="btn btn-success">
                                     <c:choose>
                                         <c:when test="${friendshipStatus == null}">
                                             Невозможно получить данные о статусе дружбы
                                         </c:when>
                                         <c:when test="${friendshipStatus == FRIEND}">
                                             Вы в друзьях
                                         </c:when>
                                         <c:when test="${friendshipStatus == BY}">

                                         </c:when>
                                         <c:when test="${friendshipStatus == FROM}">

                                         </c:when>
                                         <c:when test="${friendshipStatus == NOT_FRIEND}">
                                             Вы не дружите
                                         </c:when>
                                     </c:choose>
                                 </button>
                            </form>
                        </div>--%>
                        <%--<div class="dropdown pull-right">
                            <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Friends
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="#">Familly</a></li>
                                <li><a href="#"><i class="fa fa-fw fa-check" aria-hidden="true"></i> Friends</a></li>
                                <li><a href="#">Work</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#"><i class="fa fa-fw fa-plus" aria-hidden="true"></i> Add a new aspect</a></li>
                            </ul>
                        </div>--%>
                    </span>
                    <br><br>
                    <i class="fa fa-tags" aria-hidden="true"></i> <a href="/tags/diaspora" class="tag">#diaspora</a> <a href="/tags/hashtag" class="tag">#hashtag</a> <a href="/tags/caturday" class="tag">#caturday</a>
                    <br><br><hr>
                    <span class="pull-left">
                        <!--<a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-fw fa-files-o" aria-hidden="true"></i> Posts</a>-->
                        <!--<a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-fw fa-picture-o" aria-hidden="true"></i> Photos <span class="badge">42</span></a>-->
                        <a href="/showUserFriends?id=${userToShow.getId()}" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-fw fa-users" aria-hidden="true"></i> Список друзей пользователя <span class="badge">42</span></a>
                        <%--<a href="#" class="btn btn-link" style="text-decoration:none;"><span class="glyphicon glyphicon-send" aria-hidden="true"></span> Список моих сообщений <span class="badge">42</span></a>--%>
                    </span>
                    <div class="pull-right">
                        <a href="#" class="btn btn-success" role="button" data-toggle="modal" data-target="#msg">Сообщения</a>
                    </div>
                    <!--<span class="pull-right">
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-lg fa-at" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Mention"></i></a>
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-lg fa-envelope-o" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Message"></i></a>
                        <a href="#" class="btn btn-link" style="text-decoration:none;"><i class="fa fa-lg fa-ban" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Ignore"></i></a>
                    </span>-->
                </div>
            </div>
            <hr>
            <c:if test="${friendshipStatus == FRIEND}">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <div class="pull-left">
                                <a href="#">
                                    <img style="object-fit: cover;" src="/images/${userObject.getImg()}"
                                         class="media-object img-circle" title="John Doe" alt="John Doe" width="35px" height="35px"
                                         onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                                </a>
                            </div>
                            <form action="/createPost" method="post" id="postCreate">
                                <div class="media-body">
                                    <textarea form="postCreate" name="value" class="form-control" rows="3" placeholder="Напишите своему другу!"></textarea>
                                </div>
                                <input type="hidden" name="receiverId" value="${userToShow.getId()}">
                                <div style="padding-left: 45px; padding-top: 5px;">
                                    <button type="submit" class="btn btn-primary btn-sm">Отправить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </c:if>
            <!-- Simple post content example. -->
            <c:if test="${gotNoPosts != null && gotNoPosts}">
                <div class="col-md-12">
                    <p align="center" style="color: #b3b3b3; font-size: large">Здесь будут отображаться записи этого пользователя</p>
                </div>
            </c:if>
            <c:if test="${gotNoPosts != null && gettingPostsSuccess != null && gettingPostsSuccess && !gotNoPosts}">
                <c:forEach items="${postMap}" var="pair">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="pull-left">
                                <a href="#">
                                    <c:if test="${pair.key.getAuthorId() == userObject.getId()}">
                                        <img style="object-fit: cover; margin-right:8px; margin-top:-5px;"
                                             src="/images/${userObject.getImg()}"
                                             class="media-object img-circle" title="John Doe" alt="John Doe" width="50px" height="50px"
                                             onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                                    </c:if>
                                    <c:if test="${pair.key.getAuthorId() != userObject.getId()}">
                                        <img style="object-fit: cover; margin-right:8px; margin-top:-5px;"
                                             src="/images/${pair.value.getImg()}"
                                             class="media-object img-circle" title="John Doe" alt="John Doe" width="50px" height="50px"
                                             onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                                    </c:if>
                                </a>
                            </div>
                            <h4><a href="#" style="text-decoration:none;"><strong>${pair.value.getFirst_name()} ${pair.value.getSecond_name()}</strong></a>
                                <small><small><a class="pull-right" href="#" style="text-decoration:none; font-size: 12px; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> <fmt:formatDate type = "both" dateStyle = "long" timeStyle = "short" value = "${pair.key.getCreationDate()}"/></i></a></small></small>
                            </h4>
                            <span>
                                <c:if test="${userObject.getId() eq pair.value.getId()}">
                                    <div class="navbar-right">
                                        <div class="dropdown">
                                            <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <i class="fa fa-cog" aria-hidden="true"></i>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                                                <form id="form1" action="/deletePost" method="get" style="display: none">
                                                    <input type="text" value="${pair.key.getId()}" name="postId">
                                                    <input type="text" value="${userToShow.getId()}" name="receiverId">
                                                </form>
                                                <li><a href="javascript:;" onclick="document.getElementById('form1').submit();">Удалить пост</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="javascript:;" onclick="myFunction(${pair.key.getId()}, '${pair.key.getValue()}');">Редактировать пост</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </c:if>
                            </span>
                            <hr>
                            <div class="post-content">
                                <p style="margin-bottom: 0px" id="postToEdit${pair.key.getId()}">${pair.key.getValue()}</p>
                                <form id="editPostForm${pair.key.getId()}" method="post" action="/editPost">
                                    <div class="media-body" style="display: none" id="divEditPostArea${pair.key.getId()}">
                                        <textarea name="editedValue" id="editPostArea${pair.key.getId()}" class="form-control" rows="3"></textarea>
                                    </div>
                                    <input style="display: none;" type="text" value="${pair.key.getId()}" name="postId">
                                    <input style="display: none" type="text" value="${userToShow.getId()}" name="receiverId">
                                    <div style="padding-top: 5px;">
                                        <button id="editBtn${pair.key.getId()}" style="display: none" type="submit" class="btn btn-primary btn-sm">Отправить</button>
                                    </div>
                                </form>
                            </div>
                            <%--<hr>
                            <div>
                                <div class="pull-right btn-group-xs">
                                    <a class="btn btn-default btn-xs"><i class="fa fa-heart" aria-hidden="true"></i> Лайк</a>
                                    <a class="btn btn-default btn-xs"><i class="fa fa-retweet" aria-hidden="true"></i> Репост</a>
                                    <!--<a class="btn btn-default btn-xs"><i class="fa fa-comment" aria-hidden="true"></i> Comment</a>-->
                                </div>
                                <div class="pull-left">
                                    <p class="text-muted" style="margin-left:5px;"><i class="fa fa-globe" aria-hidden="true"></i> Public</p>
                                </div>
                                <br>
                            </div>--%>
                            <c:if test="${pair.key.getComments().size() != 0}">
                                <hr>
                                <div id="commentsDiv${pair.key.getId()}">
                                    <a onclick="myFunction4(${pair.key.getId()})" class="btn btn-default btn-xs"><i class="fa fa-bars" aria-hidden="true"></i> Показать комментарии (${pair.key.getComments().size()})</a>
                                    <c:forEach var="commentPair" items="${pair.key.getComments()}">
                                        <div class="post-content" style="display: none" id="comm${commentPair.key.getId()}">
                                            <hr style="display: none;">
                                            <div class="panel-default">
                                                <div class="panel-body" style="padding-bottom: 0px; padding-top: 25px">
                                                    <div class="pull-left">
                                                        <a href="#">
                                                            <img style="object-fit: cover; margin-right:8px; margin-top:-5px;"
                                                                 src="/images/${commentPair.value.getImg()}"
                                                                 class="media-object img-circle" title="John Doe" alt="John Doe" width="35px" height="35px"
                                                                 onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                                                        </a>
                                                    </div>
                                                    <h4><a href="#" style="text-decoration:none;">
                                                        <strong>${commentPair.value.getFirst_name()} ${commentPair.value.getSecond_name()}</strong>
                                                    </a><small><small><a class="pull-right" href="#" style="text-decoration:none; color:grey; font-size: 12px"><i><i class="fa fa-clock-o" aria-hidden="true"></i> <fmt:formatDate type = "both" dateStyle = "long" timeStyle = "short" value = "${commentPair.key.getCreationDate()}"/></i></a></small></small>
                                                    </h4>
                                                    <c:if test="${commentPair.value.getId() eq userObject.getId()}">
                                                        <span>
                                                            <div class="navbar-right">
                                                                <div class="dropdown">
                                                                    <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                        <i class="fa fa-cog" aria-hidden="true"></i>
                                                                        <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                                                                        <form id="form${commentPair.key.getId()}" action="/deleteComment" method="get" style="display: none">
                                                                            <input type="text" value="${commentPair.key.getId()}" name="commentId">
                                                                            <input type="text" value="${userObject.getId()}" name="receiverId">
                                                                        </form>
                                                                        <li><a href="javascript:;" onclick="document.getElementById('form${commentPair.key.getId()}').submit();">Удалить комментарий</a></li>
                                                                            <li role="separator" class="divider"></li>
                                                                            <li><a href="javascript:;" onclick="myFunction5(${commentPair.key.getId()}, '${commentPair.key.getValue()}');">Редактировать комментарий</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </c:if>
                                                    <hr style="margin-bottom: 10px;">
                                                    <div class="post-content">
                                                        <p style="margin-bottom: 0px" id="commentToEdit${commentPair.key.getId()}">${commentPair.key.getValue()}</p>
                                                        <form id="editCommentForm${commentPair.key.getId()}" method="post" action="/editComment">
                                                            <div class="media-body" style="display: none" id="divEditCommentArea${commentPair.key.getId()}">
                                                                <textarea style="display: none" name="editCommentValue" id="editCommentArea${commentPair.key.getId()}" class="form-control" rows="3"></textarea>
                                                            </div>
                                                            <input style="display: none;" type="text" value="${commentPair.key.getId()}" name="commentId">
                                                            <input style="display: none" type="text" value="${userToShow.getId()}" name="receiverId">
                                                            <div style="padding-top: 5px;">
                                                                <button id="editCommentBtn${commentPair.key.getId()}" style="display: none" type="submit" class="btn btn-primary btn-sm">Отправить</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                    <script>
                                        function myFunction4(id) {
                                            var div = document.getElementById('commentsDiv'.concat(id));
                                            var divs = div.getElementsByTagName('div');
                                            for (var i = 0; i < divs.length; i += 1) {
                                                divs[i].style.display="block"
                                            }
                                            /*document.getElementById("editPostArea".concat(id)).innerHTML = value
                                             document.getElementById("divEditPostArea".concat(id)).style.display="table-cell"
                                             document.getElementById("postToEdit".concat(id)).style.display="none"
                                             document.getElementById("editBtn".concat(id)).style.display="inline-block"*/
                                        }
                                    </script>
                                </div>
                            </c:if>
                            <c:if test="${friendshipStatus == FRIEND}">
                                <hr>
                                <div class="media">
                                    <div class="pull-left">
                                        <a href="#">
                                            <img style="object-fit: cover; margin-left:3px; margin-right:-5px;"
                                                 src="/images/${userObject.getImg()}"
                                                 class="media-object img-circle" title="John Doe" alt="John Doe" width="35px" height="35px"
                                                 onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                                        </a>
                                    </div>
                                    <form action="/createComment" method="post">
                                        <div class="media-body">
                                            <textarea id="ta${pair.key.getId()}" onclick="myFunction2(${pair.key.getId()})"
                                                      name="commentValue" class="form-control" rows="1" placeholder="Ваш комментарий"></textarea>
                                        </div>
                                        <input type="hidden" name="receiverId" value="${userToShow.getId()}">
                                        <input type="hidden" name="postId" value="${pair.key.getId()}">
                                        <%--<div style="padding-left: 45px; padding-top: 5px;">
                                            <button id="btn${pair.key.getId()}" style="display: none" type="submit" class="btn btn-primary btn-sm">Отправить</button>
                                        </div>--%>
                                        <button id="btn${pair.key.getId()}" style="display: none; margin-left: 45px; margin-top: 5px" type="submit" class="btn btn-primary btn-sm">Отправить</button>
                                        <button onclick="myFunction3(${pair.key.getId()});" id="btnC${pair.key.getId()}" style="display: none;margin-top: 5px;" type="button" class="btn btn-default btn-sm">Отмена</button>
                                    </form>
                                </div>
                                <script>
                                    function myFunction(id, value) {
                                        document.getElementById("editPostArea".concat(id)).innerHTML = value
                                        document.getElementById("divEditPostArea".concat(id)).style.display="table-cell"
                                        document.getElementById("postToEdit".concat(id)).style.display="none"
                                        document.getElementById("editBtn".concat(id)).style.display="inline-block"
                                    }
                                    function myFunction5(id, value) {
                                        document.getElementById("editCommentArea".concat(id)).innerHTML = value
                                        document.getElementById("editCommentArea".concat(id)).style.display = "table-cell"
                                        document.getElementById("divEditCommentArea".concat(id)).style.display="table-cell"
                                        document.getElementById("commentToEdit".concat(id)).style.display="none"
                                        document.getElementById("editCommentBtn".concat(id)).style.display="inline-block"
                                    }
                                    function myFunction2(id) {
                                        document.getElementById("btn".concat(id)).style.display="inline-block"
                                        document.getElementById("btnC".concat(id)).style.display="inline-block"
                                        document.getElementById("ta".concat(id)).rows="3"
                                    }
                                    function myFunction3(id) {
                                        document.getElementById("btn".concat(id)).style.display="none"
                                        document.getElementById("btnC".concat(id)).style.display="none"
                                        document.getElementById("ta".concat(id)).rows="1"
                                    }
                                </script>
                            </c:if>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
            <!--&lt;!&ndash; Reshare Example &ndash;&gt;
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="pull-left">
                        <a href="#">
                            <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">
                        </a>
                    </div>
                    <h4><a href="#" style="text-decoration:none;"><strong>John Doe</strong></a> – <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 42 minutes ago</i></a></small></small></h4>
                    <span>
                        <div class="navbar-right">
                            <div class="dropdown">
                                <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                                    <li><a href="#"><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> Report</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-ban" aria-hidden="true"></i> Ignore</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-bell" aria-hidden="true"></i> Enable notifications for this post</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-eye-slash" aria-hidden="true"></i> Hide</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="fa fa-fw fa-trash" aria-hidden="true"></i> Delete</a></li>
                                </ul>
                            </div>
                        </div>
                    </span>
                    <hr>
                    <div class="post-content">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="pull-left">
                                    <a href="#">
                                        <img class="media-object img-circle" src="https://diaspote.org/uploads/images/thumb_large_283df6397c4db3fe0344.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">
                                    </a>
                                </div>
                                <h4><a href="#" style="text-decoration:none;"><strong>✪ SтeғOғғιcιel ✪ ツ</strong></a> – <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> about 15 hours ago</i></a></small></small></h4>
                                <hr>
                                <div class="post-content">
                                    Reshare post example.
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div>
                        <div class="pull-right btn-group-xs">
                            <a class="btn btn-default btn-xs"><i class="fa fa-heart" aria-hidden="true"></i> Like</a>
                            <a class="btn btn-default btn-xs"><i class="fa fa-retweet" aria-hidden="true"></i> Reshare</a>
                            <a class="btn btn-default btn-xs"><i class="fa fa-comment" aria-hidden="true"></i> Comment</a>
                        </div>
                        <div class="pull-left">
                            <p class="text-muted" style="margin-left:5px;"><i class="fa fa-globe" aria-hidden="true"></i> Public</p>
                        </div>
                        <br>
                    </div>
                    <hr>
                    <div class="media">
                        <div class="pull-left">
                            <a href="#">
                                <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="35px" height="35px" style="margin-left:3px; margin-right:-5px;">
                            </a>
                        </div>
                        <div class="media-body">
                            <textarea class="form-control" rows="1" placeholder="Comment"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            &lt;!&ndash; Sample post content with picture. &ndash;&gt;
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="pull-left">
                        <a href="#">
                            <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">
                        </a>
                    </div>
                    <h4><a href="#" style="text-decoration:none;"><strong>John Doe</strong></a> – <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 42 minutes ago</i></a></small></small></h4>
                    <span>
                        <div class="navbar-right">
                            <div class="dropdown">
                                <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                                    <li><a href="#"><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> Report</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-ban" aria-hidden="true"></i> Ignore</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-bell" aria-hidden="true"></i> Enable notifications for this post</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-eye-slash" aria-hidden="true"></i> Hide</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="fa fa-fw fa-trash" aria-hidden="true"></i> Delete</a></li>
                                </ul>
                            </div>
                        </div>
                    </span>
                    <hr>
                    <div class="post-content">
                        <p>Sample post content with picture.</p>
                        <img class="img-responsive" src="https://media.giphy.com/media/j1QQj6To9Pbxu/giphy.gif">
                        <p><br><a href="/tags/christmas" class="tag">#Christmas</a> <a href="/tags/caturday" class="tag">#Caturday</a></p>
                    </div>
                    <hr>
                    <div>
                        <div class="pull-right btn-group-xs">
                            <a class="btn btn-default btn-xs"><i class="fa fa-heart" aria-hidden="true"></i> Like</a>
                            <a class="btn btn-default btn-xs"><i class="fa fa-retweet" aria-hidden="true"></i> Reshare</a>
                            <a class="btn btn-default btn-xs"><i class="fa fa-comment" aria-hidden="true"></i> Comment</a>
                        </div>
                        <div class="pull-left">
                            <p class="text-muted" style="margin-left:5px;"><i class="fa fa-globe" aria-hidden="true"></i> Public <strong>via mobile</strong></p>
                        </div>
                        <br>
                    </div>
                    <hr>
                    <div class="media">
                        <div class="pull-left">
                            <a href="#">
                                <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="35px" height="35px" style="margin-left:3px; margin-right:-5px;">
                            </a>
                        </div>
                        <div class="media-body">
                            <textarea class="form-control" rows="1" placeholder="Comment"></textarea>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- Sample post content with comments. -->
            <%--<div class="panel panel-default">
                <div class="panel-body">
                    <div class="pull-left">
                        <a href="#">
                            <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="50px" height="50px" style="margin-right:8px; margin-top:-5px;">
                        </a>
                    </div>
                    <h4><a href="#" style="text-decoration:none;"><strong>John Doe</strong></a> – <small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 42 minutes ago</i></a></small></small></h4>
                    <span>
                        <div class="navbar-right">
                            <div class="dropdown">
                                <button class="btn btn-link btn-xs dropdown-toggle" type="button" id="dd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="fa fa-cog" aria-hidden="true"></i>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dd1" style="float: right;">
                                    <li><a href="#"><i class="fa fa-fw fa-exclamation-triangle" aria-hidden="true"></i> Report</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-ban" aria-hidden="true"></i> Ignore</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-bell" aria-hidden="true"></i> Enable notifications for this post</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-eye-slash" aria-hidden="true"></i> Hide</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="fa fa-fw fa-trash" aria-hidden="true"></i> Delete</a></li>
                                </ul>
                            </div>
                        </div>
                    </span>
                    <hr>
                    <div class="post-content">
                        <p>Sample post content with comments.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vel gravida metus, non ultrices sapien. Morbi odio metus, dapibus non nibh id amet.</p>
                    </div>
                    <hr>
                    <div>
                        <div class="pull-right btn-group-xs">
                            <a class="btn btn-default btn-xs"><i class="fa fa-heart" aria-hidden="true"></i> Like</a>
                            <a class="btn btn-default btn-xs"><i class="fa fa-comment" aria-hidden="true"></i> Comment</a>
                        </div>
                        <div class="pull-left">
                            <p class="text-muted" style="margin-left:5px;"><i class="fa fa-user-secret" aria-hidden="true"></i> Limited</p>
                        </div>
                        <br>
                    </div>
                    <hr>
                    <div>
                        <a class="btn btn-default btn-xs"><i class="fa fa-bars" aria-hidden="true"></i> Show 12 more comments</a>
                        <hr>
                        <div class="post-content">
                            <div class="panel-default">
                                <div class="panel-body">
                                    <div class="pull-left">
                                        <a href="#">
                                            <img class="media-object img-circle" src="https://diaspote.org/uploads/images/thumb_large_283df6397c4db3fe0344.png" width="35px" height="35px" style="margin-right:8px; margin-top:-5px;">
                                        </a>
                                    </div>
                                    <h4><a href="#" style="text-decoration:none;"><strong>✪ SтeғOғғιcιel ✪ ツ</strong></a></h4>
                                    <hr>
                                    <div class="post-content">
                                        Comment example.<br><br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at arcu sapien. Donec laoreet, nisl quis tempor hendrerit, libero augue blandit turpis, in dignissim odio mauris eu tortor. Ut hendrerit ipsum elit, a elementum nulla ultrices eu. In posuere mollis efficitur. Maecenas justo turpis, tristique sit amet ultricies quis, molestie eget ex. Nam vestibulum consequat tincidunt. Morbi vitae placerat sapien. Phasellus quis mi tincidunt sem scelerisque tincidunt. Ut viverra porttitor sagittis. Phasellus aliquam auctor purus, id sollicitudin mauris pulvinar ac. Vivamus vel erat nec orci ultricies iaculis quis sit amet augue. Vestibulum aliquam felis lorem, interdum porttitor sapien sodales ac. Maecenas id ullamcorper risus. Suspendisse id dui sed urna rutrum pharetra. Nam eu lectus et orci vestibulum bibendum. Mauris et pulvinar dui, ac facilisis leo.
                                        <br><small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 12 minutes ago</i></a></small></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="post-content">
                            <div class="panel-default">
                                <div class="panel-body">
                                    <div class="pull-left">
                                        <a href="#">
                                            <img class="media-object img-circle" src="https://lut.im/yR07xwobAA/bZpvdTZmBBTZDJDd.png" width="35px" height="35px" style="margin-right:8px; margin-top:-5px;">
                                        </a>
                                    </div>
                                    <h4><a href="#" style="text-decoration:none;"><strong>Mi Chleen</strong></a></h4>
                                    <hr>
                                    <div class="post-content">
                                        Another comment.<br><br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at arcu sapien. Donec laoreet, nisl quis tempor hendrerit, libero augue blandit turpis, in dignissim odio mauris eu tortor. Ut hendrerit ipsum elit, a elementum nulla ultrices eu. In posuere mollis efficitur. Maecenas justo turpis, tristique sit amet ultricies quis, molestie eget ex. Nam vestibulum consequat tincidunt. Morbi vitae placerat sapien. Phasellus quis mi tincidunt sem scelerisque tincidunt. Ut viverra porttitor sagittis. Phasellus aliquam auctor purus, id sollicitudin mauris pulvinar ac. Vivamus vel erat nec orci ultricies iaculis quis sit amet augue. Vestibulum aliquam felis lorem, interdum porttitor sapien sodales ac. Maecenas id ullamcorper risus. Suspendisse id dui sed urna rutrum pharetra. Nam eu lectus et orci vestibulum bibendum. Mauris et pulvinar dui, ac facilisis leo.
                                        <br><small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 9 minutes ago</i></a></small></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="post-content">
                            <div class="panel-default">
                                <div class="panel-body">
                                    <div class="pull-left">
                                        <a href="#">
                                            <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="35px" height="35px" style="margin-right:8px; margin-top:-5px;">
                                        </a>
                                    </div>
                                    <h4><a href="#" style="text-decoration:none;"><strong>John Doe</strong></a></h4>
                                    <hr>
                                    <div class="post-content">
                                        Yet another post.<br><br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at arcu sapien. Donec laoreet, nisl quis tempor hendrerit, libero augue blandit turpis, in dignissim odio mauris eu tortor. Ut hendrerit ipsum elit, a elementum nulla ultrices eu. In posuere mollis efficitur. Maecenas justo turpis, tristique sit amet ultricies quis, molestie eget ex. Nam vestibulum consequat tincidunt. Morbi vitae placerat sapien. Phasellus quis mi tincidunt sem scelerisque tincidunt. Ut viverra porttitor sagittis. Phasellus aliquam auctor purus, id sollicitudin mauris pulvinar ac. Vivamus vel erat nec orci ultricies iaculis quis sit amet augue. Vestibulum aliquam felis lorem, interdum porttitor sapien sodales ac. Maecenas id ullamcorper risus. Suspendisse id dui sed urna rutrum pharetra. Nam eu lectus et orci vestibulum bibendum. Mauris et pulvinar dui, ac facilisis leo.
                                        <br><small><small><a href="#" style="text-decoration:none; color:grey;"><i><i class="fa fa-clock-o" aria-hidden="true"></i> 2 minutes ago</i></a></small></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="media">
                        <div class="pull-left">
                            <a href="#">
                                <img class="media-object img-circle" src="https://lut.im/7JCpw12uUT/mY0Mb78SvSIcjvkf.png" width="35px" height="35px" style="margin-left:3px; margin-right:-5px;">
                            </a>
                        </div>
                        <div class="media-body">
                            <textarea class="form-control" rows="1" placeholder="Comment"></textarea>
                        </div>
                    </div>
                </div>
            </div>--%>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    $(function () {
        $('[data-toggle="popover"]').popover()
    })
</script>

<div class="modal fade" tabindex="-1" role="dialog" id="msg">
    <div class="modal-dialog" role="document">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="panel panel-default">
                        <div class="panel-heading" id="accordion">
                            <form action="/sendMessage" method="post">
                                <div class="input-group">
                                    <input id="btn-input1" name="messageValue" type="text" class="form-control input-sm" placeholder="Введите текст сообщения..." />
                                    <input id="btn-input2" name="senderId" type="hidden" value="${userObject.getId()}"/>
                                    <input id="btn-input3" name="receiverId" type="hidden" value="${userToShow.getId()}"/>
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default btn-sm" id="btn-chat">
                                            <i class="glyphicon glyphicon-send" aria-hidden="true"></i>
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                        <c:if test="${successMessages != null && noMessages != null && successMessages && !noMessages}">
                            <div class="panel-collapse collapse in" id="collapseOne">
                                <div class="panel-body">
                                    <ul class="chat">
                                        <c:forEach items="${messages}" var="message">
                                            <c:if test="${message.getSenderId() == userToShow.getId()}">
<%--
                                                <a href="main.html"><small class="pull-left text-muted"><span class="glyphicon glyphicon-remove"></span></small></a>
--%>
                                                <li class="right clearfix">
                                                    <span class="chat-img pull-right">
                                                        <%--<img style="object-fit: cover; margin-right:8px; margin-top:-5px;"
                                                             src="/images/${commentPair.value.getPhone_num()}.png"
                                                             class="media-object img-circle" title="John Doe" alt="John Doe" width="35px" height="35px"
                                                             onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">--%>
                                                        <img style="object-fit: cover" src="/images/${userToShow.getImg()}" class="img-circle media-object"
                                                             title="John Doe" alt="John Doe" width="45px" height="45px"
                                                             onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                                                    </span>
                                                    <div class="chat-body clearfix">
                                                        <div class="header">
                                                            <small class=" text-muted"><span class="glyphicon glyphicon-time"></span> <fmt:formatDate type = "both" dateStyle = "long" timeStyle = "short" value = "${message.getSentDate()}"/></small>
                                                            <strong style="padding-right: 10px" class="pull-right primary-font">${userToShow.getFirst_name()} ${userToShow.getSecond_name()}</strong>
                                                        </div>
                                                        <p style="margin-top: 10px; margin-bottom: 10px">${message.getValue()}</p>
                                                    </div>
                                                </li>
                                            </c:if>
                                            <c:if test="${message.getSenderId() == userObject.getId()}">
<%--
                                                <a href="main.html"><small class="pull-right text-muted"><span class="glyphicon glyphicon-remove"></span></small></a>
--%>
                                                <style>
                                                    .chat li.right .chat-body {
                                                        margin-left: 60px;
                                                    }
                                                </style>
                                                <li class="right clearfix">
                                                    <span class="chat-img pull-left">
                                                        <img style="object-fit: cover" src="/images/${userObject.getImg()}" alt="User Avatar" class="img-circle media-object"
                                                             title="John Doe" width="45px" height="45px"
                                                             onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                                                    </span>
                                                    <div class="chat-body clearfix">
                                                        <div class="header">
                                                            <small class="pull-right text-muted"><span class="glyphicon glyphicon-time"></span> <fmt:formatDate type = "both" dateStyle = "long" timeStyle = "short" value = "${message.getSentDate()}"/></small>
                                                            <strong class="pull-left primary-font">${userObject.getFirst_name()} ${userObject.getSecond_name()}</strong>
                                                            <br>
                                                        </div>
                                                        <p style="margin-top: 10px; margin-bottom: 10px">${message.getValue()}</p>
                                                    </div>
                                                </li>
                                            </c:if>

                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${successMessages != null && noMessages != null && successMessages && noMessages}">
                            <p style="margin-top: 10px;" align="center">Здесь появится история вашей переписки с этим пользователем</p>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="friendsError">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <c:if test="${(successMessageSent != null && !successMessageSent) || successMessageSent == null}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Ошибка</h3>
                </div>
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert"></a>
                    <c:if test="${duplicateFriends != null && duplicateFriends}">
                        <p>Не удалось отправить запрос! Вы и пользователь уже являетесь друзьями (или заявка была отправлена)!</p>
                    </c:if>
                    <c:if test="${duplicateFriends != null && !duplicateFriends}">
                        <p>Не удалось отправить запрос! Возникла ошибка, повторите запрос позже.</p>
                    </c:if>
                    <c:if test="${noSuchFriendshipRequest != null && noSuchFriendshipRequest}">
                        <p>Не удалось подтвердить запрос в друзья! Возможно, заявка вам не была отправлена
                            или вы уже дружите!</p>
                    </c:if>
                    <c:if test="${noSuchFriendshipRequest != null && !noSuchFriendshipRequest}">
                        <p>Не удалось подтвердить запрос в друзья! Возникла ошибка, повторите запрос позже.</p>
                    </c:if>
                    <c:if test="${noFriendshipFound != null && noFriendshipFound}">
                        <p>Не удалось отменить заявку или удалить из друзей! Возможно, вы уже и так не дружите!</p>
                    </c:if>
                    <c:if test="${noFriendshipFound != null && !noFriendshipFound}">
                        <p>Не удалось отменить заявку или удалить из друзей! Возникла ошибка, повторите запрос позже.</p>
                    </c:if>
                    <c:if test="${deletePostSuccess != null && !deletePostSuccess}">
                        <p>Не удалось удалить пост! Возникла ошибка, повторите запрос позже (возможно поста не существует.</p>
                    </c:if>
                    <c:if test="${noPost != null && noPost}">
                        <p>Во время редактирования поста произошла ошибка! Возможно, этого поста не существует!</p>
                    </c:if>
                    <c:if test="${noPost!= null && !noPost}">
                        <p>Во время редактирования поста произошла ошибка! Возможно, вы ввели пустое сообщение!</p>
                    </c:if>
                    <c:if test="${postCreationSuccess!= null && !postCreationSuccess}">
                        <p>Во время создания поста произошла ошибка!</p>
                    </c:if>
                    <c:if test="${commentCreationSuccess!= null && !commentCreationSuccess}">
                        <p>Во время создания комментария произошла ошибка!</p>
                    </c:if>
                    <c:if test="${deleteCommentSuccess!= null && !deleteCommentSuccess}">
                        <p>Во время удаления комментария произошла ошибка!</p>
                    </c:if>
                    <c:if test="${successMessageSent!= null && !successMessageSent}">
                        <p>Во время отправления сообщения произошла ошибка!</p>
                    </c:if>
                </div>
            </c:if>

            <c:if test="${successMessageSent != null && successMessageSent}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Успех</h3>
                </div>
                <div class="alert alert-success alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert">×</a>
                    <c:if test="${successMessageSent != null && successMessageSent}">
                        <p>Сообщение успешно отправлено</p>
                    </c:if>
                </div>
            </c:if>

            <div class="modal-body">

            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="resources/js/bootstrap.min.js"></script>
<c:if test="${(requestSendSuccess != null && !requestSendSuccess) ||
              (successAccept != null && !successAccept) ||
              (successFriendDelete!= null && !successFriendDelete) ||
              (deletePostSuccess != null && !deletePostSuccess) ||
              (editingPostSuccess != null && !editingPostSuccess) ||
              (postCreationSuccess != null && !postCreationSuccess) ||
              (commentCreationSuccess != null && !commentCreationSuccess) ||
              (deleteCommentSuccess != null && !deleteCommentSuccess) ||
              (successMessageSent != null)}">
    <script type="text/javascript">
        $("#friendsError").modal('show');
    </script>
</c:if>

</body>
</html>

