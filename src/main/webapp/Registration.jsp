<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Регистрация</title>

    <!-- Bootstrap -->
    <%--<link href="resources/css/bootstrap.min.css" rel="stylesheet">--%>
    <link href="resources/css/style.css" rel="stylesheet">
    <link href="resources/css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a style="font-family: Lobster" class="navbar-brand" href="/Main.jsp">SportSocial</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/Main.jsp#about">О проекте</a></li>
                <li><a href="/Main.jsp#tellUs">Контакты</a></li>

                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>-->
            </ul>
            <!--<form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>-->
            <ul class="nav navbar-nav navbar-right">
                <%--<% if (session.getAttribute("loginSuccess") != null && (Boolean) session.getAttribute("loginSuccess")) { %>
                <li><a href="#">Здравствуйте, ${userObject.getFirst_name()}</a></li>
                <% } else { %>--%>
                <li><a href="#" data-toggle="modal" data-target="#login">Войти</a></li>
                <li><a href="/Registration.jsp">Зарегестрироваться</a></li>
                <%--<% } %>--%>

                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </li>-->
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container" style="padding-top: 50px">
    <h1 class="page-header" align="center">Регистрация <small>начните пользоваться SportSocial</small></h1>
<%--<div class="page-header">
        <h1>Регистрация <small>начните пользоваться SportSocial</small></h1>
        <% if (session.getAttribute("registrationSuccess") != null && !(Boolean) session.getAttribute("registrationSuccess")) { %>
        <h4 style="color: darkred">Неверные данные или пользователь с таким email уже существует!</h4>
        <% } %>
    </div>--%>


    <div class="col-md-8 col-sm-6 col-xs-12 col-md-offset-2">
        <c:if test="${registrationSuccess != null && !registrationSuccess}">
            <div class="alert alert-danger alert-dismissable" align="center">
                <a class="panel-close close" data-dismiss="alert">×</a>
                <i class="fa fa-coffee"></i>
                <p>Неверные данные или пользователь с таким email уже существует!</p>
            </div>
        </c:if>
        <form class="form-horizontal" action="/registration" method="post" id="regForm" accept-charset="UTF-8" enctype="multipart/form-data">
            <div class="form-group">
                <label for="inputEmail" class="col-md-3 col-sm-2 control-label">Email</label>
                <div class="col-md-8 col-sm-10">
                    <input type="email" value="${param.inputEmail}" class="form-control" name="inputEmail" id="inputEmail" placeholder="Email" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-md-3 col-sm-2 control-label">Пароль</label>
                <div class="col-md-8 col-sm-10">
                    <input type="password" value="${param.inputPassword}" class="form-control" name="inputPassword" id="inputPassword" placeholder="Пароль" required>
                    <span id="helpBlock" class="help-block">Пароль должен быть не мнее 8 символов</span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputFirstName" class="col-md-3 col-sm-2 control-label">Имя</label>
                <div class="col-md-8 col-sm-10">
                    <input type="text" value="${param.inputFirstName}" class="form-control" name="inputFirstName" id="inputFirstName" placeholder="Ваше имя" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputSecondName" class="col-md-3 col-sm-2 control-label">Фамилия</label>
                <div class="col-md-8 col-sm-10">
                    <input type="text" value="${param.inputSecondName}" class="form-control" name="inputSecondName" id="inputSecondName" placeholder="Ваша фамилия" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputDate" class="col-md-3 col-sm-2 control-label">Дата рождения</label>
                <div class="col-md-8 col-sm-10">
                    <input type="date" value="${param.inputDate}" class="form-control" name="inputDate" id="inputDate" placeholder="Дата вашего рождения" required>
                </div>
            </div>
            <!--<div class="form-group">
                <label for="inputCountry" class="col-sm-2 control-label">Страна</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="inputCountry" id="inputCountry" placeholder="Страна, где вы находитесь" required>
                </div>
            </div>-->
            <div class="form-group">
                <label for="inputCountry" class="col-md-3 col-sm-2 control-label">Страна</label>
                <div class="col-md-8 col-sm-10">
                    <select class="form-control" name="countrySelected" id="inputCountry">
                        <option>Россия</option>
                        <option>Беларусь</option>
                        <option>Украина</option>
                        <option>Казахстан</option>
                        <option selected>${param.countrySelected}</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPhone" class="col-md-3 col-sm-2 control-label">Номер телефона</label>
                <div class="col-md-8 col-sm-10">
                    <input value="${param.inputPhone}" type="number" class="form-control" name="inputPhone" id="inputPhone" placeholder="Ваш номер телефона" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 col-sm-2 control-label">Пол</label>
                <div class="col-md-8">
                    <div class="radio">
                        <label>
                            <input type="radio" name="optionsGender" id="optionsRadios1" value="Мужской" required>
                            Мужской
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="optionsGender" id="optionsRadios2" value="Женский" required>
                            Женский
                        </label>
                    </div>
                </div>
            </div>
            <h3>Дополнительные поля (необязательные для заполнения)</h3>
            <div class="form-group" align="center">
                <label for="inputAbout" class="col-sm-2 col-md-3 control-label">О себе</label>
                <div class="col-sm-10 col-md-8">
                    <textarea form="regForm" name="inputAbout" id="inputAbout" class="form-control"
                              rows="3" placeholder="Вкратце расскажите о себе: характер, интересы помимо спортивных">
                        ${param.inputAbout}
                    </textarea>
                    <%--<input type="text" class="form-control" name="inputAbout" id="inputAbout"
                           placeholder="Вкратце расскажите о себе: характер, интересы помимо спортивных"required>--%>
                </div>
            </div>
            <div class="form-group" align="center">
                <label for="inputSports" class="col-sm-2 col-md-3 control-label">Ваши любимые виды спорта</label>
                <div class="col-sm-10 col-md-8">
                    <input value="${param.inputSports}" type="text" class="form-control" name="inputSports" id="inputSports"
                           placeholder="Виды спорта, перечисленные через ','" >
                </div>
            </div>
            <div class="form-group" align="left">
                <label for="inputWeight" class="col-sm-2 col-md-3 control-label">Вес</label>
                <div class="col-sm-10 col-md-8">
                    <input value="${param.inputWeight}" type="number" class="form-control" name="inputWeight" id="inputWeight"
                           placeholder="Ваш вес (примерный, если не помните точное значение)" >
                    <span id="helpBlockH" class="help-block">Значение в килограммах</span>
                </div>
            </div>
            <div class="form-group" align="left">
                <label for="inputHeight" class="col-sm-2 col-md-3 control-label">Рост</label>
                <div class="col-sm-10 col-md-8">
                    <input value="${param.inputHeight}" type="number" class="form-control" name="inputHeight" id="inputHeight"
                           placeholder="Ваш рост (примерный, если не помните точное значение)" >
                    <span id="helpBlockW" class="help-block">Значение в сантиметрах</span>
                </div>
            </div>
            <div class="form-group" align="left">
                <label for="inputHeight" class="col-sm-2 col-md-3 control-label">Фото</label>
                <div class="col-sm-10 col-md-8" >
                    <input accept="image/png" onchange="showPath(this)" style="display: none" type="file" id="fileInput" name="inputPic" class="text-center center-block well well-sm"><i ></i>
                    <button type="button" class="btn btn-sm" onclick="chooseFile();">Выберите фото</button>
                    <!--<p id="path"></p>-->
                    <img class="col-md-5" style="display: none" id="blah" src="#" />
                    <script>
                        function chooseFile() {
                            $("#fileInput").click();
                        }
                        function showPath(input) {
                            /*document.getElementById("path").innerHTML = "Файл загружен"*/
                            if (input.files && input.files[0]) {
                                var reader = new FileReader();
                                reader.onload = function (e) {
                                    $('#blah')
                                        .attr('src', e.target.result)
                                        .width(70)
                                        .height(70);
                                };
                                reader.readAsDataURL(input.files[0]);
                                document.getElementById("blah").style.display="block"
                            }
                        }
                    </script>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-8 col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-md">Подтвердить регистрацию</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div style="padding-top: 20px"></div>

<div class="modal fade" tabindex="-1" role="dialog" id="login">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Войти</h3>
            </div>
            <c:if test="${loginSuccess != null && !loginSuccess}">
                <div class="alert alert-danger alert-dismissable" align="center">
                    <a class="panel-close close" data-dismiss="alert"></a>
                    <p>Неверные пароль или email! Повторите ввод!</p>
                    <!--<p>Во время обновления возникла ошибка!</p>-->
                </div>
            </c:if>
            <div class="modal-body">
                <form class="form-horizontal" action="/login" method="post">
                    <div class="form-group">
                        <label for="inputEmail2" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="inputEmail" id="inputEmail2" placeholder="Email" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword2" class="col-sm-2 control-label">Пароль</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="inputPassword" id="inputPassword2" placeholder="Пароль" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="rememberCheck"/> Запомнить меня
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                        <button type="submit" class="btn btn-primary">Войти</button>
                    </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.mod


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="resources/js/bootstrap.min.js"></script>
</body>
</html>

