<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: yuriy
  Date: 18.08.17
  Time: 0:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Результаты поиска</title>

    <!-- Bootstrap -->
    <%--<link href="resources/css/bootstrap.min.css" rel="stylesheet">--%>
    <link href="resources/css/style.css" rel="stylesheet">
    <link href="resources/css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a style="font-family: Lobster" class="navbar-brand" href="/myProfile">SportSocial</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/Main.jsp#about">О проекте</a></li>
                <li><a href="/Main.jsp#tellUs">Контакты</a></li>
                <!--<li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>-->
            </ul>
            <!--<form class="navbar-form navbar-left">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>-->
            <ul class="nav navbar-nav navbar-right">
                <form class="navbar-form navbar-left" action="/searchUsers" method="get">
                    <div class="form-group" >
                        <input style="width: 80px;" type="text" class="form-control" name="inputFirstName" placeholder="Имя">
                    </div>
                    <div class="form-group">
                        <input style="width: 130px;" type="text" class="form-control" name="inputSecondName" placeholder="Фамилия">
                    </div>
                    <button type="submit" class="btn btn-default">Искать</button>
                </form>

                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                        <img style="object-fit: cover;" src="/images/${userObject.getImg()}"
                                 class="media-object img-circle" title="John Doe" alt="John Doe" width="30px" height="30px"
                                 onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                    </span>
                    <span class="user-name">${userObject.getFirst_name()} ${userObject.getSecond_name()}</span>
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/showFriends">Мои Друзья</a></li>
                        <li><a href="/myProfile">Мой Профиль</a></li>
                        <li><a href="#">Мои Сообщения</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/EditUserProfile.jsp">Редактировать профиль</a></li>
                        <li><a href="/logout">Выйти</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container" style="padding-top: 50px;" >
    <div class="row">
            <h1 class="page-header" style="margin-bottom: 0px;" align="center">Результаты поиска
                <small>Запрос: "${param.get("inputFirstName")} ${param.get("inputSecondName")}"</small></h1>
            <c:if test="${gotUsersSuccess != null && !gotUsersSuccess}">
                <h4>По вашему запросу ничего не найдено :(</h4>
            </c:if>
            <c:if test="${invalidParsToCompleteSearch!= null && invalidParsToCompleteSearch}">
                <h4 style="color: darkred">Проверьте правильность ввода параметров поиска!</h4>
            </c:if>
            <c:if test="${gotUsersSuccess != null && gotUsersSuccess && userList != null}">
                <div class="container col-md-offset-1">
                    <c:forEach var="user" items="${userList}">
                        <div class="span3 well col-md-3" style="margin: 5px;">
                            <center>
                                <a href="/otherUser?id=${user.getId()}">
                                    <img src="/images/${user.getImg()}"
                                         name="aboutme" style="object-fit: cover; width: 140px; height: 140px"
                                         class="avatar img-circle img-thumbnail"
                                         onerror="if (this.src != '/images/default.png') this.src = '/images/default.png';">
                                </a>
                                <a href="/otherUser?id=${user.getId()}"><h3>${user.getFirst_name()} ${user.getSecond_name()}</h3></a>

                                <p> <i class="glyphicon glyphicon-envelope"></i> ${user.getEmail()}
                                    <br/><i class="glyphicon glyphicon-globe"></i> ${user.getCountry()}
                                    <br/> <i class="glyphicon glyphicon-gift"></i> ${user.getBirth_date()}
                                    <br/> <i class="glyphicon glyphicon-user"></i> ${user.getGender()}
                                </p>
                            </center>
                        </div>
                    </c:forEach>
                </div>
                <%--<div class="panel-body col-md-7 col-md-offset-2" style="border: hidden; padding-top: 0px;">
                    <div class="table-container" style="border: hidden">
                        <table class="table-users table" border="0" style="border: hidden;">
                            <tbody>
                            <c:forEach var="user" items="${userList}">
                                <tr &lt;%&ndash;onclick="window.location.href = 'otherUser?id=${user.getId()}';" style="cursor: pointer"&ndash;%&gt;>
                                    <td width="10">
                                        <img class="pull-left img-circle nav-user-photo" width="25" src="https://www.shareicon.net/download/2015/11/15/168096_delete_512x512.png" />  
                                    </td>
                                    <td style="font-size: 14px;">
                                        ${user.getFirst_name()} ${user.getSecond_name()}<br>
                                    </td>
                                    <td style="font-size: 14px;">
                                        ${user.getEmail()}
                                    </td>
                                    <td style="font-size: 14px;">
                                        ${user.getCountry()}
                                    </td>
                                    <td style="font-size: 14px;">
                                        ${user.getBirth_date()}
                                    </td>
                                    <td style="font-size: 14px;">
                                        ${user.getGender()}
                                    </td>
                                    <td align="center">
                                        <span class="glyphicon glyphicon-send" style="font-size: 25px;" aria-hidden="true"></span>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>--%>
            </c:if>
    </div>
</div>
<div style="padding-top: 50px"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="resources/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $("#login").modal('show');
</script>
</body>
</html>
