package dao;

import entities.Comment;
import entities.Post;
import entities.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.DBConnector;
import util.exceptions.comments.GotNoCommentException;
import util.exceptions.users.UserCreateException;

import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by yuriy on 29.08.17.
 */
public class CommentsDAOTest {
    private User testUser1;
    private User testUser2;
    private User testUser3;
    private User testUser4;
    private User testUser5;
    private User testUser6;
    private User testUser7;

    private Post testPost1;
    private Post testPost2;
    private Post testPost3;

    private Comment testComment1;
    private Comment testComment2;
    private Comment testComment3;

    @Before
    public void setUp() throws Exception {
        testUser1 = new User(
                "lala@gmail.com",
                "12345678",
                "Иван",
                "Иванов",
                "01-11-1995",
                "Россия",
                "8900123456",
                "Мужской",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser2 = new User(
                "eoneguy@gmail.com",
                "12345678",
                "Ивангай",
                "Стрельцов",
                "01-11-1995",
                "Россия",
                "89112340123",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser3 = new User(
                "eoneguy1@gmail.com",
                "12345678",
                "Ивангай",
                "Пикачуев",
                "01-11-1995",
                "Россия",
                "89112340124",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser4 = new User(
                "petro@gmail.com",
                "12345678",
                "Зенит",
                "Петровский",
                "01-11-1995",
                "Россия",
                "89112340125",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser5 = new User(
                "petro2@gmail.com",
                "12345678",
                "Зенит",
                "Петровский",
                "01-11-1995",
                "Россия",
                "89112340126",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser6 = new User(
                "petro3@gmail.com",
                "12345678",
                "Спартак",
                "Петровский",
                "01-11-1995",
                "Россия",
                "89112340127",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser7 = new User(
                "prpr@gmail.com",
                "12345678",
                "Иван",
                "Иванов",
                "01-11-1995",
                "Россия",
                "89112340128",
                "Мужской",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
    }
    @After
    public void tearDown() throws Exception {
        deleteTestUsers(new UserDAO());
    }

    @Test
    public void createComment() throws Exception {
        CommentsDAO commentsDAO = new CommentsDAO();
        UserDAO userDAO = new UserDAO();
        PostsDAO postsDAO = new PostsDAO();
        addTestUsersAndPosts(userDAO, postsDAO);
        testComment1 = commentsDAO.createComment(testComment1);
        testComment2 = commentsDAO.createComment(testComment2);
        assertEquals(testComment1, commentsDAO.getCommentById(testComment1.getId()));
        assertEquals(testComment2, commentsDAO.getCommentById(testComment2.getId()));


    }

    @Test
    public void editComment() throws Exception {
        CommentsDAO commentsDAO = new CommentsDAO();
        UserDAO userDAO = new UserDAO();
        PostsDAO postsDAO = new PostsDAO();
        addTestUsersAndPosts(userDAO, postsDAO);
        testComment1 = commentsDAO.createComment(testComment1);
        testComment2 = commentsDAO.createComment(testComment2);
        testComment1 = commentsDAO.editComment("123", testComment1.getId());
        assertEquals(testComment1.getValue(), "123");

    }

    @Test(expected = GotNoCommentException.class)
    public void deleteComment() throws Exception {
        CommentsDAO commentsDAO = new CommentsDAO();
        UserDAO userDAO = new UserDAO();
        PostsDAO postsDAO = new PostsDAO();
        addTestUsersAndPosts(userDAO, postsDAO);
        testComment1 = commentsDAO.createComment(testComment1);
        commentsDAO.deleteComment(testComment1.getId());
        commentsDAO.getCommentById(testComment1.getId());

    }

    @Test
    public void getPostComments() throws Exception {
        CommentsDAO commentsDAO = new CommentsDAO();
        UserDAO userDAO = new UserDAO();
        PostsDAO postsDAO = new PostsDAO();
        addTestUsersAndPosts(userDAO, postsDAO);
        testComment1 = commentsDAO.createComment(testComment1);
        testComment2 = commentsDAO.createComment(testComment2);
        ArrayList<Comment> testList = new ArrayList<>();
        testList.add(testComment1);
        testList.add(testComment2);
        assertEquals(commentsDAO.getPostComments(testPost1.getId()), testList);

    }

    private void addTestUsersAndPosts(UserDAO userDAO, PostsDAO postsDAO) throws Exception {
        testUser1 = userDAO.createUser(testUser1);
        testUser2 = userDAO.createUser(testUser2);
        testUser3 = userDAO.createUser(testUser3);
        testUser4 = userDAO.createUser(testUser4);
        testUser5 = userDAO.createUser(testUser5);
        testUser6 = userDAO.createUser(testUser6);
        testUser7 = userDAO.createUser(testUser7);

        testPost1 = postsDAO.createPost(new Post(
                testUser4.getId(),
                testUser4.getId(),
                "Fury прошерстил позабытые фильмы этого лета и обнаружил единственный в своем роде глянцевый пост-апокалипсис – " +
                        "«Плохая партия». А вот почему же она все-таки плохая – смотрим, разбираемся, встаем в позу." +
                       /* "\n" +*/
                        "Нет-нет-нет, никто ни в коем случае не станет спорить с тем, что Ана Лили ",
                new Date()
        ));
        testPost2 = postsDAO.createPost(new Post(
                testUser4.getId(),
                testUser5.getId(),
                "Fury прошерстил позабытые фильмы этого лета и обнаружил единственный в своем роде глянцевый пост-апокалипсис – " +
                        "«Плохая партия». А вот почему же она все-таки плохая – смотрим, разбираемся, встаем в позу." +
                        /*"\n" +*/
                        "Нет-нет-нет, никто ни в коем случае не станет спорить с тем, что Ана Лили ",
                new Date()
        ));
        testPost3 = postsDAO.createPost(new Post(
                testUser5.getId(),
                testUser4.getId(),
                "Fury прошерстил позабытые фильмы этого лета и обнаружил единственный в своем роде глянцевый пост-апокалипсис – " +
                        "«Плохая партия». А вот почему же она все-таки плохая – смотрим, разбираемся, встаем в позу." +
                        /*"\n" +*/
                        "Нет-нет-нет, никто ни в коем случае не станет спорить с тем, что Ана Лили ",
                new Date()
        ));

        testComment1 = new Comment(
                testPost1.getId(),
                testUser1.getId(),
                "Comment1",
                new Date()
        );

        testComment2 = new Comment(
                testPost1.getId(),
                testUser2.getId(),
                "Comment1",
                new Date()
        );

        testComment3 = new Comment(
                testPost3.getId(),
                testUser1.getId(),
                "Comment1",
                new Date()
        );

        /*rel1 = new FriendshipRelation(testUser4.getId(), testUser7.getId(), "false");
        rel2 = new FriendshipRelation(testUser4.getId(), testUser1.getId(), "false");
        rel3 = new FriendshipRelation(testUser2.getId(), testUser4.getId(), "false");
        rel4 = new FriendshipRelation(testUser4.getId(), testUser5.getId(), "true");
        rel5 = new FriendshipRelation(testUser4.getId(), testUser6.getId(), "true");
        rel6 = new FriendshipRelation(testUser3.getId(), testUser4.getId(), "true");*/
    }

    private void deleteTestUsers(UserDAO userDAO) throws Exception {
        userDAO.deleteUserByEmail(testUser1.getEmail());
        userDAO.deleteUserByEmail(testUser2.getEmail());
        userDAO.deleteUserByEmail(testUser3.getEmail());
        userDAO.deleteUserByEmail(testUser4.getEmail());
        userDAO.deleteUserByEmail(testUser5.getEmail());
        userDAO.deleteUserByEmail(testUser6.getEmail());
        userDAO.deleteUserByEmail(testUser7.getEmail());

    }

}