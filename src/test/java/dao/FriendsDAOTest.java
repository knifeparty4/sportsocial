package dao;

import entities.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.DBConnector;
import util.dto.FriendshipRelation;
import util.enums.FriendsRequests;
import util.enums.FriendshipStatus;
import util.exceptions.users.UserCreateException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by yuriy on 19.08.17.
 */
public class FriendsDAOTest {

    private User testUser1;
    private User testUser2;
    private User testUser3;
    private User testUser4;
    private User testUser5;
    private User testUser6;
    private User testUser7;
    private FriendshipRelation rel1;
    private FriendshipRelation rel2;
    private FriendshipRelation rel3;
    private FriendshipRelation rel4;
    private FriendshipRelation rel5;
    private FriendshipRelation rel6;

    @Before
    public void setUp() throws Exception {
        testUser1 = new User(
                "lala@gmail.com",
                "12345678",
                "Иван",
                "Иванов",
                "01-11-1995",
                "Россия",
                "8900123456",
                "Мужской",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser2 = new User(
                "eoneguy@gmail.com",
                "12345678",
                "Ивангай",
                "Стрельцов",
                "01-11-1995",
                "Россия",
                "89112340123",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser3 = new User(
                "eoneguy1@gmail.com",
                "12345678",
                "Ивангай",
                "Пикачуев",
                "01-11-1995",
                "Россия",
                "89112340124",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser4 = new User(
                "petro@gmail.com",
                "12345678",
                "Зенит",
                "Петровский",
                "01-11-1995",
                "Россия",
                "89112340125",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser5 = new User(
                "petro2@gmail.com",
                "12345678",
                "Зенит",
                "Петровский",
                "01-11-1995",
                "Россия",
                "89112340126",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser6 = new User(
                "petro3@gmail.com",
                "12345678",
                "Спартак",
                "Петровский",
                "01-11-1995",
                "Россия",
                "89112340127",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser7 = new User(
                "prpr@gmail.com",
                "12345678",
                "Иван",
                "Иванов",
                "01-11-1995",
                "Россия",
                "89112340128",
                "Мужской",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );

    }

    @After
    public void tearDown() throws Exception {
        /*deleteTestUsers(new UserDAO(DBConnector.getMysqlConnection()));*/
    }

    @Test
    public void sendFriendRequest() throws Exception {
        UserDAO userDAO = new UserDAO();
        FriendsDAO friendsDAO = new FriendsDAO();
        testUser1 = userDAO.createUser(testUser1);
        testUser2 = userDAO.createUser(testUser2);
        friendsDAO.sendFriendRequest(
                testUser1.getId(),
                testUser2.getId()
        );
/*        friendsDAO.sendFriendRequest(
                testUser2.getId(),
                testUser1.getId()
        );*/
        userDAO.deleteUserByEmail(testUser1.getEmail());
        userDAO.deleteUserByEmail(testUser2.getEmail());

        friendsDAO.deleteFriendship(testUser1.getId(), testUser2.getId());


    }

    @Test
    public void getUserFriendsById() throws Exception {
        UserDAO userDAO = new UserDAO();
        FriendsDAO friendsDAO = new FriendsDAO();
        addTestUsers(userDAO);
        addTestFriendship(friendsDAO);
        List<User> list = friendsDAO.getUserFriendsById(testUser4.getId());
        List<User> testList = new ArrayList<>();
        testList.add(testUser5);
        testList.add(testUser6);
        testList.add(testUser3);
        assertEquals(testList, list);
        deleteTestUsers(new UserDAO());
        deleteTestFriendship(friendsDAO);
    }

    @Test
    public void getUserFriendRequestsById() throws Exception {
        UserDAO userDAO = new UserDAO();
        FriendsDAO friendsDAO = new FriendsDAO();
        addTestUsers(userDAO);
        addTestFriendship(friendsDAO);
        List<User> list = friendsDAO.getUserFriendRequestsById(testUser4.getId(), FriendsRequests.REQUESTS_FROM_USER);
        List<User> testList = new ArrayList<>();
        testList.add(testUser7);
        testList.add(testUser1);
        assertEquals(testList, list);

        list = friendsDAO.getUserFriendRequestsById(testUser4.getId(), FriendsRequests.REQUESTS_TO_USER);
        testList.clear();
        testList.add(testUser2);
        assertEquals(testList, list);
        deleteTestFriendship(friendsDAO);
        deleteTestUsers(userDAO);

    }

    @Test
    public void getUserFriendshipStatus() throws Exception {
        UserDAO userDAO = new UserDAO();
        FriendsDAO friendsDAO = new FriendsDAO();
        addTestUsers(userDAO);
        addTestFriendship(friendsDAO);
        assertEquals(friendsDAO.getUserFriendshipStatus(testUser4.getId(), testUser7.getId()), FriendshipStatus.REQUETED_BY_USER);
        assertEquals(friendsDAO.getUserFriendshipStatus(testUser4.getId(), testUser5.getId()), FriendshipStatus.FRIEND);
        assertEquals(friendsDAO.getUserFriendshipStatus(testUser4.getId(), 10), FriendshipStatus.NOT_FRIEND);
        assertEquals(friendsDAO.getUserFriendshipStatus(testUser4.getId(), testUser2.getId()), FriendshipStatus.REQUESTED_TO_USER);


        deleteTestUsers(userDAO);
        deleteTestFriendship(friendsDAO);

    }

    @Test
    public void deleteFriendship() throws Exception {
        UserDAO userDAO = new UserDAO();
        FriendsDAO friendsDAO = new FriendsDAO();
        addTestUsers(userDAO);
        addTestFriendship(friendsDAO);
        friendsDAO.deleteFriendship(testUser4.getId(), testUser7.getId());
        assertEquals(friendsDAO.getUserFriendshipStatus(testUser4.getId(), testUser7.getId()), FriendshipStatus.NOT_FRIEND);
        friendsDAO.deleteFriendship(rel2.getUserId1(), rel2.getUserId2());
        friendsDAO.deleteFriendship(rel3.getUserId1(), rel3.getUserId2());
        friendsDAO.deleteFriendship(rel4.getUserId1(), rel4.getUserId2());
        friendsDAO.deleteFriendship(rel5.getUserId1(), rel5.getUserId2());
        friendsDAO.deleteFriendship(rel6.getUserId1(), rel6.getUserId2());
        deleteTestUsers(userDAO);

    }

    @Test
    public void acceptFriend() throws Exception {
        UserDAO userDAO = new UserDAO();
        addTestUsers(userDAO);
        FriendsDAO friendsDAO = new FriendsDAO();
        friendsDAO.sendFriendRequest(rel1.getUserId1(), rel1.getUserId2());
        friendsDAO.acceptFriend(rel1.getUserId1(), rel1.getUserId2());
        List<User> list = friendsDAO.getUserFriendsById(rel1.getUserId1());
        assertEquals(userDAO.getUserById(rel1.getUserId2()), list.get(0));
        friendsDAO.deleteFriendship(rel1.getUserId1(), rel1.getUserId2());
        deleteTestUsers(userDAO);



    }

    private void addTestUsers(UserDAO userDAO) throws UserCreateException {
        testUser1 = userDAO.createUser(testUser1);
        testUser2 = userDAO.createUser(testUser2);
        testUser3 = userDAO.createUser(testUser3);
        testUser4 = userDAO.createUser(testUser4);
        testUser5 = userDAO.createUser(testUser5);
        testUser6 = userDAO.createUser(testUser6);
        testUser7 = userDAO.createUser(testUser7);

        rel1 = new FriendshipRelation(testUser4.getId(), testUser7.getId(), "false");
        rel2 = new FriendshipRelation(testUser4.getId(), testUser1.getId(), "false");
        rel3 = new FriendshipRelation(testUser2.getId(), testUser4.getId(), "false");
        rel4 = new FriendshipRelation(testUser4.getId(), testUser5.getId(), "true");
        rel5 = new FriendshipRelation(testUser4.getId(), testUser6.getId(), "true");
        rel6 = new FriendshipRelation(testUser3.getId(), testUser4.getId(), "true");
    }

    private void deleteTestUsers(UserDAO userDAO) throws Exception {
        userDAO.deleteUserByEmail(testUser1.getEmail());
        userDAO.deleteUserByEmail(testUser2.getEmail());
        userDAO.deleteUserByEmail(testUser3.getEmail());
        userDAO.deleteUserByEmail(testUser4.getEmail());
        userDAO.deleteUserByEmail(testUser5.getEmail());
        userDAO.deleteUserByEmail(testUser6.getEmail());
        userDAO.deleteUserByEmail(testUser7.getEmail());
    }

    private void addTestFriendship(FriendsDAO friendsDAO) throws Exception {
        friendsDAO.sendFriendRequest(rel1.getUserId1(), rel1.getUserId2());
        friendsDAO.sendFriendRequest(rel2.getUserId1(), rel2.getUserId2());
        friendsDAO.sendFriendRequest(rel3.getUserId1(), rel3.getUserId2());
        friendsDAO.sendFriendRequest(rel4.getUserId1(), rel4.getUserId2());
        friendsDAO.acceptFriend(rel4.getUserId1(), rel4.getUserId2());
        friendsDAO.sendFriendRequest(rel5.getUserId1(), rel5.getUserId2());
        friendsDAO.acceptFriend(rel5.getUserId1(), rel5.getUserId2());
        friendsDAO.sendFriendRequest(rel6.getUserId1(), rel6.getUserId2());
        friendsDAO.acceptFriend(rel6.getUserId1(), rel6.getUserId2());

    }

    private void deleteTestFriendship(FriendsDAO friendsDAO) throws Exception{
        friendsDAO.deleteFriendship(rel1.getUserId1(), rel1.getUserId2());
        friendsDAO.deleteFriendship(rel2.getUserId1(), rel2.getUserId2());
        friendsDAO.deleteFriendship(rel3.getUserId1(), rel3.getUserId2());
        friendsDAO.deleteFriendship(rel4.getUserId1(), rel4.getUserId2());
        friendsDAO.deleteFriendship(rel5.getUserId1(), rel5.getUserId2());
        friendsDAO.deleteFriendship(rel6.getUserId1(), rel6.getUserId2());

    }


}