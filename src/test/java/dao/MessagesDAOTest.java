package dao;

import entities.Message;
import entities.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.DBConnector;
import util.exceptions.messages.GotNoMessageException;
import util.exceptions.users.UserCreateException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by yuriy on 04.09.17.
 */
public class MessagesDAOTest {
    private User testUser1;
    private User testUser2;
    private User testUser3;

    private Message testMessage1;
    private Message testMessage2;
    private Message testMessage3;

    @Before
    public void setUp() throws Exception {
        testUser1 = new User(
                "lala@gmail.com",
                "12345678",
                "Иван",
                "Иванов",
                "01-11-1995",
                "Россия",
                "8900123456",
                "Мужской",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser2 = new User(
                "eoneguy@gmail.com",
                "12345678",
                "Ивангай",
                "Стрельцов",
                "01-11-1995",
                "Россия",
                "89112340123",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser3 = new User(
                "eoneguy1@gmail.com",
                "12345678",
                "Ивангай",
                "Пикачуев",
                "01-11-1995",
                "Россия",
                "89112340124",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );


    }

    @Test
    public void getMessageById() throws Exception {
        MessagesDAO messagesDAO = new MessagesDAO();
        addTestUsers(new UserDAO());
        testMessage1 = messagesDAO.createMessage(testMessage1);
        assertEquals(testMessage1, messagesDAO.getMessageById(testMessage1.getId()));
        messagesDAO.deleteMessage(testMessage1.getId());

    }

    @Test
    public void createMessage() throws Exception {
        MessagesDAO messagesDAO = new MessagesDAO();
        addTestUsers(new UserDAO());
        testMessage1 = messagesDAO.createMessage(testMessage1);
        testMessage2 = messagesDAO.createMessage(testMessage2);
        testMessage3 = messagesDAO.createMessage(testMessage3);
        assertEquals(testMessage1, messagesDAO.getMessageById(testMessage1.getId()));
        assertEquals(testMessage2, messagesDAO.getMessageById(testMessage2.getId()));
        assertEquals(testMessage3, messagesDAO.getMessageById(testMessage3.getId()));
        messagesDAO.deleteMessage(testMessage1.getId());
        messagesDAO.deleteMessage(testMessage2.getId());
        messagesDAO.deleteMessage(testMessage3.getId());
    }

    @Test(expected = GotNoMessageException.class)
    public void deleteMessage() throws Exception {
        MessagesDAO messagesDAO = new MessagesDAO();
        addTestUsers(new UserDAO());
        testMessage1 = messagesDAO.createMessage(testMessage1);
        messagesDAO.deleteMessage(testMessage1.getId());
        messagesDAO.getMessageById(testMessage1.getId());

    }

    //порядок заполнения??
    @Test
    public void getUserMessages() throws Exception {
        MessagesDAO messagesDAO = new MessagesDAO();
        addTestUsers(new UserDAO());
        testMessage1 = messagesDAO.createMessage(testMessage1);
        testMessage2 = messagesDAO.createMessage(testMessage2);
        testMessage3 = messagesDAO.createMessage(testMessage3);
        List<Message> testList = new ArrayList<>();
        testList.add(testMessage1);
        testList.add(testMessage2);
        assertEquals(testList, messagesDAO.getUserMessages(testUser1.getId(), testUser2.getId()));
        messagesDAO.deleteMessage(testMessage1.getId());
        messagesDAO.deleteMessage(testMessage2.getId());
        messagesDAO.deleteMessage(testMessage3.getId());
    }

    @After
    public void tearDown() throws Exception {
        deleteTestUsers(new UserDAO());
    }

    private void addTestUsers(UserDAO userDAO) throws UserCreateException {
        testUser1 = userDAO.createUser(testUser1);
        testUser2 = userDAO.createUser(testUser2);
        testUser3 = userDAO.createUser(testUser3);

        testMessage1 = new Message(
                testUser1.getId(),
                testUser2.getId(),
                "msg1",
                new Date()
        );

        testMessage2 = new Message(
                testUser2.getId(),
                testUser1.getId(),
                "msg2",
                new Date()
        );

        testMessage3 = new Message(
                testUser2.getId(),
                testUser3.getId(),
                "msg1",
                new Date()
        );

    }

    private void deleteTestUsers(UserDAO userDAO) throws Exception {
        userDAO.deleteUserByEmail(testUser1.getEmail());
        userDAO.deleteUserByEmail(testUser2.getEmail());
        userDAO.deleteUserByEmail(testUser3.getEmail());
    }

}