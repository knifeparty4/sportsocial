package dao;

import entities.Post;
import entities.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.DBConnector;
import util.dto.FriendshipRelation;
import util.exceptions.posts.GotNoPostException;
import util.exceptions.users.UserCreateException;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by yuriy on 23.08.17.
 */
public class PostsDAOTest {
    private User testUser1;
    private User testUser2;
    private User testUser3;
    private User testUser4;
    private User testUser5;
    private User testUser6;
    private User testUser7;

    private Post testPost1;
    private Post testPost2;
    private Post testPost3;

    @Before
    public void setUp() throws Exception {
        testUser1 = new User(
                "lala@gmail.com",
                "12345678",
                "Иван",
                "Иванов",
                "01-11-1995",
                "Россия",
                "8900123456",
                "Мужской",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser2 = new User(
                "eoneguy@gmail.com",
                "12345678",
                "Ивангай",
                "Стрельцов",
                "01-11-1995",
                "Россия",
                "89112340123",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser3 = new User(
                "eoneguy1@gmail.com",
                "12345678",
                "Ивангай",
                "Пикачуев",
                "01-11-1995",
                "Россия",
                "89112340124",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser4 = new User(
                "petro@gmail.com",
                "12345678",
                "Зенит",
                "Петровский",
                "01-11-1995",
                "Россия",
                "89112340125",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser5 = new User(
                "petro2@gmail.com",
                "12345678",
                "Зенит",
                "Петровский",
                "01-11-1995",
                "Россия",
                "89112340126",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser6 = new User(
                "petro3@gmail.com",
                "12345678",
                "Спартак",
                "Петровский",
                "01-11-1995",
                "Россия",
                "89112340127",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser7 = new User(
                "prpr@gmail.com",
                "12345678",
                "Иван",
                "Иванов",
                "01-11-1995",
                "Россия",
                "89112340128",
                "Мужской",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );


        /*testPost4 = new Post(
                testUser4.getId(),
                testUser4.getId(),
                "Fury прошерстил позабытые фильмы этого лета и обнаружил единственный в своем роде глянцевый пост-апокалипсис – " +
                        "«Плохая партия». А вот почему же она все-таки плохая – смотрим, разбираемся, встаем в позу.\n" +
                        "\n" +
                        "Нет-нет-нет, никто ни в коем случае не станет спорить с тем, что Ана Лили ",
                new Date()
        );
        testPost5 = new Post(
                testUser4.getId(),
                testUser4.getId(),
                "Fury прошерстил позабытые фильмы этого лета и обнаружил единственный в своем роде глянцевый пост-апокалипсис – " +
                        "«Плохая партия». А вот почему же она все-таки плохая – смотрим, разбираемся, встаем в позу.\n" +
                        "\n" +
                        "Нет-нет-нет, никто ни в коем случае не станет спорить с тем, что Ана Лили ",
                new Date()
        );*/

    }

    @After
    public void tearDown() throws Exception {
        deleteTestUsers(new UserDAO());
    }

    @Test
    public void createPost() throws Exception {
        PostsDAO postsDAO = new PostsDAO();
        UserDAO userDAO = new UserDAO();
        addTestUsers(userDAO);
        Post post1 = postsDAO.createPost(testPost1);
        Post post2 = postsDAO.createPost(testPost2);
        Post post3 = postsDAO.createPost(testPost3);
        assertEquals(post1.getAuthorId(), testPost1.getAuthorId());
        assertEquals(post2.getAuthorId(), testPost2.getAuthorId());
        assertEquals(post3.getAuthorId(), testPost3.getAuthorId());
        assertEquals(post1.getValue(), testPost1.getValue());
        assertEquals(post2.getValue(), testPost2.getValue());
        assertEquals(post3.getValue(), testPost3.getValue());


    }

    /*@Test
    public void editPost() throws Exception {

    }*/

    @Test(expected = GotNoPostException.class)
    public void deletePost() throws Exception {
        PostsDAO postsDAO = new PostsDAO();
        UserDAO userDAO = new UserDAO();
        addTestUsers(userDAO);
        testPost1 = postsDAO.createPost(testPost1);
        testPost2 = postsDAO.createPost(testPost2);
        testPost3 = postsDAO.createPost(testPost3);
        postsDAO.deletePost(testPost1.getId());
        postsDAO.getPostById(testPost1.getId());

    }

    @Test
    public void getUserPosts() throws Exception {
        PostsDAO postsDAO = new PostsDAO();
        UserDAO userDAO = new UserDAO();
        addTestUsers(userDAO);
        testPost1 = postsDAO.createPost(testPost1);
        testPost2 = postsDAO.createPost(testPost2);
        testPost3 = postsDAO.createPost(testPost3);
        List<Post> list = postsDAO.getUserPosts(testUser4.getId());
        List<Post> testList = new ArrayList<>();
        testList.add(testPost1);
        testList.add(testPost3);
        assertEquals(testList, list);


    }

    private void addTestUsers(UserDAO userDAO) throws UserCreateException {
        testUser1 = userDAO.createUser(testUser1);
        testUser2 = userDAO.createUser(testUser2);
        testUser3 = userDAO.createUser(testUser3);
        testUser4 = userDAO.createUser(testUser4);
        testUser5 = userDAO.createUser(testUser5);
        testUser6 = userDAO.createUser(testUser6);
        testUser7 = userDAO.createUser(testUser7);

        testPost1 = new Post(
                testUser4.getId(),
                testUser4.getId(),
                "Fury прошерстил позабытые фильмы этого лета и обнаружил единственный в своем роде глянцевый пост-апокалипсис – " +
                        "«Плохая партия». А вот почему же она все-таки плохая – смотрим, разбираемся, встаем в позу." +
                       /* "\n" +*/
                        "Нет-нет-нет, никто ни в коем случае не станет спорить с тем, что Ана Лили ",
                new Date()
        );
        testPost2 = new Post(
                testUser4.getId(),
                testUser5.getId(),
                "Fury прошерстил позабытые фильмы этого лета и обнаружил единственный в своем роде глянцевый пост-апокалипсис – " +
                        "«Плохая партия». А вот почему же она все-таки плохая – смотрим, разбираемся, встаем в позу." +
                        /*"\n" +*/
                        "Нет-нет-нет, никто ни в коем случае не станет спорить с тем, что Ана Лили ",
                new Date()
        );
        testPost3 = new Post(
                testUser5.getId(),
                testUser4.getId(),
                "Fury прошерстил позабытые фильмы этого лета и обнаружил единственный в своем роде глянцевый пост-апокалипсис – " +
                        "«Плохая партия». А вот почему же она все-таки плохая – смотрим, разбираемся, встаем в позу." +
                        /*"\n" +*/
                        "Нет-нет-нет, никто ни в коем случае не станет спорить с тем, что Ана Лили ",
                new Date()
        );

        /*rel1 = new FriendshipRelation(testUser4.getId(), testUser7.getId(), "false");
        rel2 = new FriendshipRelation(testUser4.getId(), testUser1.getId(), "false");
        rel3 = new FriendshipRelation(testUser2.getId(), testUser4.getId(), "false");
        rel4 = new FriendshipRelation(testUser4.getId(), testUser5.getId(), "true");
        rel5 = new FriendshipRelation(testUser4.getId(), testUser6.getId(), "true");
        rel6 = new FriendshipRelation(testUser3.getId(), testUser4.getId(), "true");*/
    }

    private void deleteTestUsers(UserDAO userDAO) throws Exception {
        userDAO.deleteUserByEmail(testUser1.getEmail());
        userDAO.deleteUserByEmail(testUser2.getEmail());
        userDAO.deleteUserByEmail(testUser3.getEmail());
        userDAO.deleteUserByEmail(testUser4.getEmail());
        userDAO.deleteUserByEmail(testUser5.getEmail());
        userDAO.deleteUserByEmail(testUser6.getEmail());
        userDAO.deleteUserByEmail(testUser7.getEmail());
    }

    /*private void addTestFriendship(FriendsDAO friendsDAO) throws Exception {
        friendsDAO.sendFriendRequest(rel1.getUserId1(), rel1.getUserId2());
        friendsDAO.sendFriendRequest(rel2.getUserId1(), rel2.getUserId2());
        friendsDAO.sendFriendRequest(rel3.getUserId1(), rel3.getUserId2());
        friendsDAO.sendFriendRequest(rel4.getUserId1(), rel4.getUserId2());
        friendsDAO.acceptFriend(rel4.getUserId1(), rel4.getUserId2());
        friendsDAO.sendFriendRequest(rel5.getUserId1(), rel5.getUserId2());
        friendsDAO.acceptFriend(rel5.getUserId1(), rel5.getUserId2());
        friendsDAO.sendFriendRequest(rel6.getUserId1(), rel6.getUserId2());
        friendsDAO.acceptFriend(rel6.getUserId1(), rel6.getUserId2());

    }

    private void deleteTestFriendship(FriendsDAO friendsDAO) throws Exception{
        friendsDAO.deleteFriendship(rel1.getUserId1(), rel1.getUserId2());
        friendsDAO.deleteFriendship(rel2.getUserId1(), rel2.getUserId2());
        friendsDAO.deleteFriendship(rel3.getUserId1(), rel3.getUserId2());
        friendsDAO.deleteFriendship(rel4.getUserId1(), rel4.getUserId2());
        friendsDAO.deleteFriendship(rel5.getUserId1(), rel5.getUserId2());
        friendsDAO.deleteFriendship(rel6.getUserId1(), rel6.getUserId2());

    }*/

}