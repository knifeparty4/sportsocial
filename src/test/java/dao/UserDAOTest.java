package dao;

import entities.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import util.DBConnector;
import util.dto.Name;
import util.exceptions.users.NoUserException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserDAOTest {
    private User testUser;
    private User testUser2;
    private User testUser3;
    private User testUser4;
    private User testUser5;
    private User testUser6;
    private static final String testEmail = "test2333@gmail.com";
    private static final String testPhone = "89212341673";

    @Before
    public void setUp() throws Exception {
        testUser = new User(
                testEmail,
                "12345678",
                "Иван",
                "Иванов",
                "01-11-1995",
                "Россия",
                testPhone,
                "Мужской",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser2 = new User(
                "eoneguy@gmail.com",
                "12345678",
                "Ивангай",
                "Стрельцов",
                "01-11-1995",
                "Россия",
                "89112340123",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser3 = new User(
                "eoneguy1@gmail.com",
                "12345678",
                "Ивангай",
                "Пикачуев",
                "01-11-1995",
                "Россия",
                "89112340124",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser4 = new User(
                "petro@gmail.com",
                "12345678",
                "Зенит",
                "Петровский",
                "01-11-1995",
                "Россия",
                "89112340125",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser5 = new User(
                "petro2@gmail.com",
                "12345678",
                "Зенит",
                "Петровский",
                "01-11-1995",
                "Россия",
                "89112340126",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
        testUser6 = new User(
                "petro3@gmail.com",
                "12345678",
                "Спартак",
                "Петровский",
                "01-11-1995",
                "Россия",
                "89112340127",
                "male",
                "Я отличный человек",
                "Футбол, Баскетбол",
                "",
                "",
                ""
        );
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getUserById() throws Exception {
        UserDAO userDAO = new UserDAO();
        userDAO.createUser(testUser);
        User user = userDAO.getUserById(userDAO.getUserByEmail(testEmail).getId());
        user.setId(null);
        assertEquals(testUser, user);
        userDAO.deleteUserByEmail(testEmail);

    }

    @Test
    public void getUserByEmail() throws Exception {
        UserDAO userDAO = new UserDAO();
        userDAO.createUser(testUser);
        User user = userDAO.getUserByEmail(testEmail);
        user.setId(null);
        assertEquals(testUser, user);
        userDAO.deleteUserByEmail(testEmail);

    }

    @Test
    public void createUser() throws Exception {
        UserDAO userDAO = new UserDAO();
        User createdUser = userDAO.createUser(testUser);
        assertEquals(createdUser, userDAO.getUserByEmail(testEmail));
        userDAO.deleteUserByEmail(testEmail);

    }

    @Test(expected = NoUserException.class)
    public void deleteUserById() throws Exception {
        UserDAO userDAO = new UserDAO();
        userDAO.createUser(testUser);
        userDAO.deleteUserByEmail(testEmail);
        userDAO.getUserByEmail(testEmail);

    }

    @Test(expected = NoUserException.class)
    public void deleteUserByEmail() throws Exception {
        UserDAO userDAO = new UserDAO();
        User createdUser = userDAO.createUser(testUser);
        userDAO.deleteUserById(createdUser.getId());
        userDAO.getUserByEmail(testEmail);

    }

    @Test
    public void updateUser() throws Exception {
        UserDAO userDAO = new UserDAO();
        User createdUser = userDAO.createUser(testUser);
        createdUser.setEmail("newemail@gmail.com");
        createdUser.setPhone_num("89114533484");
        User updatedUser = userDAO.updateUser(createdUser, createdUser.getId());
        assertEquals(createdUser, updatedUser);
        userDAO.deleteUserById(createdUser.getId());
    }

    @Test
    public void getUsers() throws Exception {
        UserDAO userDAO = new UserDAO();
        List<User> test = new ArrayList<>();
       /* userDAO.createUser(testUser2);
        userDAO.createUser(testUser3);*/
        test.add(userDAO.createUser(testUser2));
        test.add(userDAO.createUser(testUser3));
        List<User> list = userDAO.getUsersByName(new Name(testUser2.getFirst_name(), "Noname"));
        assertEquals(list, test);

        test.clear();
        test.add(userDAO.createUser(testUser4));
        test.add(userDAO.createUser(testUser5));
        test.add(userDAO.createUser(testUser6));
        list = userDAO.getUsersByName(new Name("Noname", testUser4.getSecond_name()));
        assertEquals(list, test);

        test.clear();
        test.add(userDAO.getUserByEmail(testUser4.getEmail()));
        test.add(userDAO.getUserByEmail(testUser5.getEmail()));
        list = userDAO.getUsersByName(new Name(testUser4.getFirst_name(), testUser4.getSecond_name()));
        assertEquals(list, test);

        userDAO.deleteUserByEmail(testUser2.getEmail());
        userDAO.deleteUserByEmail(testUser3.getEmail());
        userDAO.deleteUserByEmail(testUser4.getEmail());
        userDAO.deleteUserByEmail(testUser5.getEmail());
        userDAO.deleteUserByEmail(testUser6.getEmail());


        /*test.add(userDAO.getUserById(3));
        test.add(userDAO.getUserById(15));
        assertEquals(list, test);*/
    }
}